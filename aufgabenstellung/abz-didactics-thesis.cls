\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{abz-didactics-thesis}[2023/02/03 Giovanni Serafini]

\LoadClass[paper=A4,numbers=enddot,parskip=half]{scrartcl}
\RequirePackage[left=2.0cm, right=2.0cm, top=2.5cm, bottom=2.5cm]{geometry}
\setlength{\parindent}{0pt}
\RequirePackage{setspace}
\setstretch{1.1}

%\RequirePackage{pgfkeys}
\RequirePackage{pgfopts}

\RequirePackage{graphicx} 
\graphicspath{{pictures/}}

\pgfkeys{
    student/.store in=\student, % to store the value into a macro
    student={Roger Federer}, % initial value
    studentnr/.store in=\studentnr,
    studentnr=00-000-000,
    studentshort/.store in=\studentshort,
    studentshort={R. Federer}, % initial value
}

\newcommand{\studentsetup}[3]{%
    \pgfkeys{student=#1,studentshort=#2,studentnr=#3}
}


\pgfkeys{
    professor/.store in=\professor, % to store the value into a macro
    professor={Prof. Dr. Juraj Hromkovi\v{c}}, % initial value
    professorshort/.store in=\professorshort,
    professorshort={Prof. Dr. J. Hromkovi\v{c}}, % initial value
}

\pgfkeys{
    cosupervisor/.store in=\cosupervisor, % to store the value into a macro
    cosupervisor={Giovanni Serafini}, % initial value
    cosupervisorshort/.store in=\cosupervisorshort,
    cosupervisorshort={G. Serafini}, % initial value
    cosupervisorfunction/.store in=\cosupervisorfunction,
    cosupervisorfunction={Lecturer in Didactics of Computer Science}
}

\pgfkeys{
    cocosupervisor/.store in=\cocosupervisor, % to store the value into a macro
    cocosupervisor={Roger Federer}, % initial value
    cocosupervisorshort/.store in=\cocosupervisorshort,
    cocosupervisorshort={R. Federer}, % initial value
    cocosupervisorfunction/.store in=\cocosupervisorfunction,
    cocosupervisorfunction={Lecturer in didactics of computer science at}
}


\pgfkeys{/abz-didactics-thesis/.cd,% to set the path
    thesis/.store in=\thesistype, % to store the value into a macro
    thesis={}, % initial value
    programme/.store in = \ethprogramme,
    programme = {},
    unit/.store in=\unitnr,
    unit=0,
    credits/.store in = \credits,
    credits = 0,
    basecolor/.store in=\basecolor,
    basecolor=black,
    thesisfontsize/.store in = \thesisfontsize,
    thesisfontsize = {13pt}
}



\pgfkeys{
    /abz-didactics-thesis/.cd,
        bachelor/.style = {
            /abz-didactics-thesis/.cd,
            thesis = Bachelor Thesis, 
            programme = Bachelor Programme,
            unit = 252-0500-00, 
            credits = 10, 
            basecolor = ETHBronzeDark},
        master/.style = {
            /abz-didactics-thesis/.cd,
            thesis = Master Thesis, 
            programme = Master Programme,
            unit =  263-0800-00, 
            credits = 30, 
            basecolor = ETHBlueDark}
}

\ProcessPgfOptions{/abz-didactics-thesis}

\KOMAoption{fontsize}{\thesisfontsize}


\RedeclareSectionCommand[%
  afterskip=3pt
]{section}

\RedeclareSectionCommand[%
  beforeskip=1ex,
  afterskip=2pt
]{subsection}


%%%% General packages
\RequirePackage[T1]{fontenc}
\RequirePackage[utf8]{inputenc}
\RequirePackage[main=english]{babel}
\RequirePackage[english]{varioref}
\RequirePackage{microtype}
%\RequirePackage[%autostyle,german=guillemets
%]{csquotes}
\RequirePackage[]{csquotes}
\setquotestyle[italianguillemets]{latin}
\RequirePackage{enumitem}
\RequirePackage{lastpage}
\RequirePackage[]{scrlayer-scrpage}
\RequirePackage[markup=nocolor,deletedmarkup=xout]{changes}



%%%% ETH colors & new ETH colors
% https://ethz.ch/services/de/service/kommunikation/corporate-design/farbe.html
% https://contrastchecker.com/
% https://www.w3schools.com/colors/colors_picker.asp
\usepackage{xcolor}
\definecolor{ETH1}{HTML}{1F407A}
\definecolor{ETH2}{HTML}{3C5A0F}
\definecolor{ETH3}{HTML}{0069B4}
\definecolor{ETH4}{HTML}{72791C}
\definecolor{ETH5}{HTML}{91056A}
\definecolor{ETH6}{HTML}{6F6F6E}
\definecolor{ETH7}{HTML}{A8322D}
\definecolor{ETH8}{HTML}{007A92}
\definecolor{ETH9}{HTML}{956013}
\definecolor{ETH10}{HTML}{82BE1E}

\definecolor{ETHBlue}{HTML}{215CAF}
\definecolor{ETHPetrol}{HTML}{007894}
\definecolor{ETHGreen}{HTML}{627313}
\definecolor{ETHBronze}{HTML}{8E6713}
\definecolor{ETHRed}{HTML}{B7352D}
\definecolor{ETHPurple}{HTML}{A30774}
\definecolor{ETHGrey}{HTML}{6F6F6F}

\definecolor{ETHBlueDark}{HTML}{08407E}
\definecolor{ETHPetrolDark}{HTML}{00596D}
\definecolor{ETHGreenDark}{HTML}{365213}
\definecolor{ETHBronzeDark}{HTML}{956013}
\definecolor{ETHRedDark}{HTML}{96272D}
\definecolor{ETHPurpleDark}{HTML}{8C0A59}
\definecolor{ETHGreyDark}{HTML}{575757}

% My additional colors
\definecolor{GSChocolate}{RGB}{123,63,0}
\definecolor{Coffee}{RGB}{111,78,55}
\definecolor{GymInf}{RGB}{200,34,44}
%%%%

\RequirePackage[colorlinks=true,citecolor=\basecolor, linkcolor=, urlcolor=\basecolor]{hyperref}
\urlstyle{sf}

\RequirePackage[
    backend = biber,
    style = numeric-comp,
    natbib =  true,
    sorting = none,
%    sortcites = true,
    doi = false,
    autolang=other
]{biblatex}


%%%% tikz and libraries
\RequirePackage{tikz}
\usetikzlibrary{positioning}
\RequirePackage{tikzpagenodes}
\usetikzlibrary{calc}
%\RequirePackage{showframe}
%Theres the tikzpagenodes package which offers current page text area anchors:
%https://tex.stackexchange.com/questions/229934/anchor-tikz-at-the-left-margin
%%%%


\newcommand{\thesistitle}{%
    \vspace{1.6\baselineskip}
    \begin{center}
        \LARGE\sffamily\textcolor{\basecolor}{\textbf{\thesistype}}

        \footnotesize\sffamily\textcolor{\basecolor}{Course Unit: \unitnr}\\
        \footnotesize\sffamily\textcolor{\basecolor}{(\credits~ECTS credits)}

        \vspace{\baselineskip}
%	    \Large\sffamily\textbf{\textcolor{\basecolor}{Project Description}}
	    \Large\sffamily\textcolor{\basecolor}{Project Description}
    \end{center}
}
    
     
\newcommand{\ethz}{ETH Zurich}
\newcommand{\ABZ}{Center for Informatics Education of ETH Zurich (ABZ)}
\newcommand{\ITE}{Chair of Information Technology and Education}
%\newcommand{\professor}{Prof. Dr. Juraj Hromkovi\v{c}}
%\newcommand{\cosupervisor}{Giovanni Serafini}
%\newcommand{\cosupervisorfunction}{Lecturer in didactics of computer science}
%\newcommand{\cocosupervisor}{Roger Federer}
%\newcommand{\cocosupervisorfunction}{PhD student}

\RequirePackage[sfdefault]{ClearSans} %% option 'sfdefault' activates Clear Sans as the default text font
\RequirePackage[euler-digits,euler-hat-accent]{eulervm}
%\usepackage{cochineal}
%\RequirePackage[sfdefault]{biolinum}

\setkomafont{section}{\sffamily\color{\basecolor}}
\setkomafont{subsection}{\sffamily\color{\basecolor}}
\setkomafont{subtitle}{\sffamily\color{\basecolor}}

\setkomafont{title}{\sffamily\color{\basecolor}}
\addtokomafont{title}{\let\huge\Large}

\setkomafont{subject}{\sffamily\color{\basecolor}}
\setkomafont{date}{\sffamily\color{\basecolor}}
\newcommand{\alert}[1]{\textcolor{\basecolor}{#1}}

\setkomafont{pagenumber}{\normalfont\sffamily\small\color{\basecolor}}
\renewcommand*{\pagemark}{%
    \usekomafont{pagenumber}{\thepage\slash\pageref{LastPage}}
}

\let\oldbibitem\bibitem
\renewcommand{\bibitem}[1]{\color{\basecolor}\oldbibitem{#1}\color{black}}


\newcommand{\logos}{
\noindent
\begin{tikzpicture}[remember picture,overlay]
\coordinate[yshift= 0cm] (nw) at (current page header area.north west);
\coordinate[yshift=-2.5cm] (sw) at (current page header area.north west);
\coordinate[yshift= 0cm] (ne) at (current page header area.north east);
\coordinate[yshift=-2.5cm] (se) at (current page header area.north east);

\coordinate (mw) at ($(nw)!0.5!(sw)$);
\coordinate (me) at ($(ne)!0.5!(se)$);

\draw[fill=\basecolor] (nw) rectangle (se);

\node[anchor=west,xshift=0.0cm,yshift=0.0cm](eth) at (mw) {\includegraphics[height=2.5cm]{eth_logo_kurz_neg}};

\node[anchor=east,xshift=0.0cm,yshift=0.0cm](dinfk) at (me) {\includegraphics[height=2.5cm]{eth_dinfk_logo_neg}};

\end{tikzpicture}
}

\RequirePackage{scrlayer-scrpage}% sets pagestyle scrheadings automatically
\clearpairofpagestyles
\cfoot*{\pagemark}

\renewcommand{\pagemark}{%
\begin{tikzpicture}
    \coordinate (L) at (0:0);
    \coordinate (R) at (0:\linewidth);
    \coordinate (M) at ($(L)!0.5!(R)$);
    \node[rectangle,fill=\basecolor, minimum width = 2em, minimum height=1.5em, align=center,yshift=1pt,inner sep = 3pt, outer sep=3pt](pn) at (M) {\usekomafont{pagenumber}\textcolor{white}{\thepage\textbar\pageref*{LastPage}}};
\end{tikzpicture}
}


\newcommand{\signatures}{
    \begin{tikzpicture}%
        \draw [line width=0.5pt, cap=rect,color=\basecolor] (0,0) -- ({0.30\linewidth},0);
        \node[] () at ({0.15\linewidth},-1em) {\studentshort};
    
        \draw [line width=0.5pt, cap=rect,color=\basecolor] ({0.35\linewidth},0) -- ({0.65\linewidth},0);
        \node[] () at ({0.5\linewidth},-1em) {\cosupervisorshort};
    
        \draw [line width=0.5pt, cap=rect,color=\basecolor] ({0.70\linewidth},0) -- ({\linewidth},0);
        \node[] () at ({0.850\linewidth},-1em) {\professorshort};
    \end{tikzpicture}
}



\makeatletter
\renewcommand{\sectionlinesformat}[4]{%
\ifstr{#1}{section}{%
    \parbox[t]{\linewidth}{%
      \raggedsection\@hangfrom{\hskip #2#3}{#4}\par%
      \kern-.75\ht\strutbox\rule{\linewidth}{.8pt}%
    }%
  }{%
    \@hangfrom{\hskip #2#3}{#4}}% 
}
\makeatother

\deffootnote[1em]{1em}{1em}{\textsuperscript{\textcolor{\basecolor}{\thefootnotemark}}}
\renewcommand\thefootnote{\textcolor{\basecolor}{\arabic{footnote}}}

\let\oldfootnoterule\footnoterule
\renewcommand{\footnoterule}{\textcolor{\basecolor}{\oldfootnoterule}}

\setlist[itemize]{topsep=0pt, partopsep=0pt,itemsep=2pt,parsep=2pt}

