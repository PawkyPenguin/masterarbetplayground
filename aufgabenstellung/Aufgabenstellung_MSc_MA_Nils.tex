%\documentclass[bachelor]{abz-didactics-thesis}
\documentclass[master,thesisfontsize={12pt}]{abz-didactics-thesis}

\usepackage{draftwatermark}

\newif\ifusewatermark
%\usewatermarktrue

\ifusewatermark
    \SetWatermarkLightness{0.9}
    \SetWatermarkText{DRAFT}
    \SetWatermarkScale{1}
    \SetWatermarkColor{ETHRed!10}
\else
    \SetWatermarkText{}
\fi


%http://mirror.kumi.systems/ctan/biblio/bibtex/utils/bibtool/bibtool.pdf
\addbibresource[]{Aufgabenstellung.bib}

\studentsetup{Nils Leuzinger}{N. Leuzinger}{14-939-896}


%\pagestyle{empty}

\begin{document}

\logos\thesistitle


\section*{Topic}
Design and implementation of an environment for efficient creation of digital, dynamic, and interactive learning content for schools


\section*{Student}
\student{} (\studentnr)\\
Computer Science~\ethprogramme~at \ethz

\section*{Supervision \& Grading}
Lead supervisor: \professor \\
%Supervisor: \cosupervisor, \cocosupervisor \\
Project supervisor: \cosupervisor, \cosupervisorfunction\\

\section*{Time-Frame}
Assignment date: Monday, August 14, 2023 \\
Submission due: Wednesday, February 14,  2024

\newpage

\section*{Background, Motivation, and general Requirements}
Up to date, it is difficult for non-programmers to create learning material that effectively utilizes the power of a digital environment for more than simple multimedia content. In a typical Swiss school setting, a teacher writes digital learning material in a WYSIWYG-rich text editor, Latex, or a presentation program. The resulting document, while digital, is largely static except for a few non-interactive visualizations, animations, and embedded multimedia content. This sells short the power of a digital environment. 

This thesis aims to empower teachers with the ability to create digital interactive learning material (further called \enquote{dynamic documents}) with digital elements that go beyond simple multimedia content. Teachers should be equipped with tools for creating dynamic and interactive learning material, like a programmer might do. Unfortunately, creating learning content by programming it yourself, while extremely flexible, is also time intensive and requires technical expertise. Platforms such as the so-called \enquote{Data-Driven Documents} \citep{DDD} or Jupyter Books \citep{jupyter_book} are good illustrations of the dilemma.

The idea at the root of this thesis consists in implementing a general framework for creating dynamic documents, and in looking, hand in hand, for specific application areas where digital features could enrich existing learning material. These features should then be progressively added as basic components to the application and would therefore become standard building blocks for dynamic documents. That is, the thesis should make it possible for teachers to enrich learning material in specific predefined ways, with modest technical skills, and with relatively little time investment.

A major task is thus to research contemporary learning material that profits most from added digital elements (we call these \enquote{dynamic components}). We then choose and implement specific dynamic components in our document format. We must prioritize components by ease of use, benefit to student learning, and breadth of applicability (across multiple school subjects, or within a single subject). Breadth of application is useful because the target audience of our environment are non-programmers, and so they will be limited by its available features. 

A further requirement is the implementation of the environment and dynamic document format in a modular way, such that later work can expand it to increase the available dynamic components. The environment and document format must be easy enough for non-programmers to use that they can create dynamic learning material efficiently. Furthermore, we must consider that teachers need to share this material with their students, and ensure that students can work with it in a user-friendly manner. Many teachers and students like to print their learning material, so dynamic documents should \enquote{fail gracefully} when printed on paper and they should not lose their static content, such as textual prose.
\newpage
\section*{Goals of the Thesis}
The objective of the thesis consists in designing, implementing, testing, and evaluating an environment and a document format for school teachers to create dynamic documents for their students. Teachers without programming experience should be able to create dynamic documents without significant additional time investment when compared to static documents. The thesis' focus lies on the Swiss school setting at Gymnasiums.

The outcome of the thesis should be a well-documented, stable and reliable environment prototype that enables teachers with no programming experience to create dynamic documents, and enables students to profit from this new style of learning material in regular Swiss Gymnasium school classes. The environment should run on popular modern browsers. The thesis should investigate dynamic documents in different school subjects and discuss where they might have merit, and which steps (if any) can be taken to improve value.


\newpage

\section*{Tasks}
As part of the~\thesistype, the student is expected to perform the three following main tasks and their subtasks:

\begin{enumerate}[leftmargin=*]
	\item \alert{Concept of the application}
	\begin{enumerate}
		\item Study the textbooks and the relevant publications of the \ITE{} in didactics of computer science. Consider further literature in the field and reflect on possible contributions of computer science to school and to general education.
		\item Investigate learning material currently used in school across a spectrum of subjects to find elements that might benefit from interactive content. Study their documentation and the underlying technologies.
	\end{enumerate}
	\item \alert{Implementation and test}
	\begin{enumerate}
		\item Evaluate and analyze requirements and features for the application.
		\item Propose the most suitable state-of-the-art technologies for this project.
		\item Produce a design specification of the software that meets the requirements. 
		\item Implement the application in accordance with requirements and analysis.
		\item Test the application and report on the outcomes with respect to the requirements.
		\item Test the usability of the platform in appropriate settings at the \ABZ{} or at school.			
	\end{enumerate}
	\item \alert{Presentation of the results}
	\begin{enumerate}
		\item Elaborate on the work in a well-documented report.
		\item Demonstrate the use and functionality of the application.
		\item Give an informal mid-project talk as well as a final, graded presentation.
	\end{enumerate}
\end{enumerate}

\noindent The student is expected to autonomously manage the project, to comply with rigorous scientific standards, and to apply sound software engineering procedures in order to successfully design, develop and test the platform. We expect full and detailed documentation of all steps of the project. The student has to provide both a high-level documentation as well as a low-level documentation in the form of suitable comments in the source code.

\section*{Grading Criteria}
The Master Thesis will be graded according to the \enquote{Memo internal Registration for Master’s Thesis in Computer Science, Regulation of Studies 2009} \citep{IR}. 

Major emphasis will be placed on the quality of \textit{objectives and scope}, \textit{scientific approach}, \textit{design}, \textit{implementation}, \textit{reflection}, \textit{autonomy}, \textit{learning aptitude}, \textit{creativity}, \textit{documentation},  \textit{presentation}, and \textit{time management}.

%\newpage

\printbibliography



\vspace{1cm}
\noindent Zurich, \today 

\vspace{2.0cm}
\signatures

\end{document}
