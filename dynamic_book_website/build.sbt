import scala.sys.process._
import org.scalajs.linker.interface.ModuleInitializer

val ScalatraVersion = "3.0.0-M4"

ThisBuild / scalaVersion := "3.3.0"
ThisBuild / organization := "com.nils"
Global / scalaJSStage := FastOptStage

lazy val shared = crossProject(JSPlatform, JVMPlatform)
    .crossType(CrossType.Pure)
    .in(file("nilsdoc/shared"))
    .settings(
      libraryDependencies ++= Seq("com.lihaoyi" %%% "scalatags" % "0.12.0", "com.lihaoyi" %%% "upickle" % "3.1.3",
      )
    )
    .jsSettings(
      scalaJSLinkerConfig ~= { _.withModuleKind(ModuleKind.ESModule) },
      scalaJSUseMainModuleInitializer := true,
    )

lazy val client = (project
    .in(file("nilsdoc/client")))
    .settings(
      libraryDependencies ++= Seq(
        "org.scala-js" %%% "scalajs-dom" % "2.6.0",
        "com.lihaoyi" %% "requests" % "0.8.0",
      ),
      externalNpm := {
          val rootDir = (ThisBuild / baseDirectory).value / "nilsdoc"
          Process("npm install", rootDir).!
          rootDir
      },
      //Compile / npmDevDependencies ++= Seq(
      //  "source-map-loader"-> "4.0.1",
      //  "webpack"-> "5.88.2",
      //  "concat-with-sourcemaps"-> "1.0.7",
      //  "webpack-cli"-> "5.1.4",
      //  "webpack-dev-server"-> "4.15.1"
      //),
      //Compile / npmDependencies ++= Seq(
      //  "webpack" -> "~5.88.2",
      //  "webpack-cli" -> "~5.1.4",
      //  "webpack-dev-server" -> "~4.15.1",
      //  "bootstrap" -> "~5.2.0",
      //  "@types/bootstrap" -> "~5.2.0",
      //  "quill" -> "~1.3.0",
      //  "@types/quill" -> "~1.3.0",
      //  "konva" -> "~9.2.0",
      //  "prosemirror-example-setup" -> "~1.2.2",
      //  "prosemirror-model" -> "~1.19.3",
      //  "prosemirror-schema-basic" -> "~1.2.2",
      //  "prosemirror-state" -> "~1.4.3",
      //  "prosemirror-view" -> "~1.31.8"
      //),
      //stIgnore ++= List("pagedjs"),
      scalaJSLinkerConfig ~= { _.withModuleKind(ModuleKind.CommonJSModule) },
      Compile / scalaJSModuleInitializers ++= Seq(
        ModuleInitializer.mainMethod("nilsdocWorkstation.NilsdocWorkstation", "main").withModuleID("nilsdocWorkstation"),
        ModuleInitializer.mainMethod("nilsdocWorkstation.PublishView", "main").withModuleID("publishView"),
        ModuleInitializer.mainMethod("nilsdocWorkstation.NilsdocStudentView", "main").withModuleID("studentView"),
        ModuleInitializer.mainMethod("personalNotes.PersonalNotesViewer", "main").withModuleID("personalNotes"),
        ModuleInitializer.mainMethod("visualEditor.VisualEditorTest", "main").withModuleID("visualEditor"),
      )
    )
    .dependsOn(shared.js)
    .enablePlugins(ScalaJSPlugin /*, ScalablyTypedConverterExternalNpmPlugin*/ /*, ScalaJSBundlerPlugin*/)

lazy val server = (project
    .in(file("nilsdoc/jvm")))
    .settings(
      // sourcesInBase := false,
      // packageOptions += Package.MainClass("JettyLauncher"),
      Jetty / containerPort := {
        sys.env.getOrElse("MODE", "development") match {
          case "production" => 80
          case _ => 8081
        }
      },
      name := "My Scalatra Web App",
      version := "0.1.0-SNAPSHOT",
      scalacOptions := Seq("-deprecation"),
      libraryDependencies ++= Seq(
        "com.lihaoyi" %% "os-lib" % "0.9.1",
        "org.scalatra" %% "scalatra" % ScalatraVersion,
        "org.scalatra" %% "scalatra-scalatest" % ScalatraVersion % "test",
        "ch.qos.logback" % "logback-classic" % "1.4.11",
        "org.eclipse.jetty" % "jetty-webapp" % "9.4.43.v20210629" % "container",
        "javax.servlet" % "javax.servlet-api" % "3.1.0" % "provided",
        "com.typesafe.slick" %% "slick" % "3.5.0-M4",
        "org.xerial" % "sqlite-jdbc" % "3.43.0.0",
        "com.mchange" % "c3p0" % "0.9.5.2"
      )
    )
    .dependsOn(shared.jvm)
    .enablePlugins(SbtTwirl)
    .enablePlugins(JettyPlugin)

lazy val copyResourceFiles = taskKey[Unit]("Copy scalajs file from client to server")
copyResourceFiles := {
    val serverWebappSrc = (server / Compile / target).value / "webapp" / "src"
    IO.createDirectory(serverWebappSrc)
    val jsDir = (client / Compile / fastLinkJS / crossTarget).value / "client-fastopt"
    val jsFiles = jsDir.glob("*.js") +++ jsDir.glob("*.js.map")
    val destDir = serverWebappSrc
    IO.copy(jsFiles.pair(Path.rebase(jsDir, destDir)))

    val nodeModulesBase = baseDirectory.value / "nilsdoc"
    Seq("node_modules", "vendor").map(sourceDir => {
        (nodeModulesBase / sourceDir)
            .glob("*")
            .filter(_.isDirectory)
            .pair(Path.rebase(nodeModulesBase / sourceDir, serverWebappSrc))
            .foreach(srcAndTarget => {
                IO.createDirectory(srcAndTarget._2)
                IO.copyDirectory(srcAndTarget._1, srcAndTarget._2)
            })
    })
}

lazy val bundleWebpack = taskKey[Unit]("Bundle webpack")
bundleWebpack := {
  Process("npx webpack", (server / Compile / target).value / "webapp").!
}

lazy val killOldBuildProcess = taskKey[Unit]("Kills still running build processes. Intended to be run on every recompile because VSCode when too much is going on.")
killOldBuildProcess := {
  "./kill_running_build_processes.sh" !
}

ThisBuild / fork := true

addCommandAlias("compileAll", "killOldBuildProcess; client/Compile/fastLinkJS; copyResourceFiles; bundleWebpack; server/Jetty/start")
addCommandAlias("buildAll", "killOldBuildProcess; client/Compile/fastLinkJS; copyResourceFiles; bundleWebpack")

//server / Compile / compile := (server / Compile / compile).dependsOn(copyResourceFiles).value
//lazy val startBoth = taskKey[Unit]("Compile all and run server")
//startBoth := {
//  (server / Compile / compile).value
//  (server/Jetty/start).toTask.evaluated
//}.enablePlugins(JettyPlugin).dependsOn(server)
