package ast

import scala.collection.mutable.ArrayBuffer
import lineparse.LineParser
import inlineparser.InlineParser
import scala.CanEqual.derived
import scala.util.control.Breaks._
import blockparse.BlockParser
import blockparse.CodeBlock
import blockparse.LatexBlock
import blockparse.NamedBlock
import blockparse.Root
import blockparse.ImportantBlock
import blockparse.FormattedTextBlock
import blockparse.UnformattedTextBlock
import blockparse.HeaderBlock
import ast.AST.TextSegment

object ASTBuilder {
    def blockToAST(block: blockparse.Block, interpretSubtreeAsPlaintext: Boolean)(using file: Vector[String]): AST.Block = {
        val nextRecursion = blockToAST(_, block.contentIsPlaintext) // already fill in the argument of next recursion step for brevity.

        block match {
            case b@blockparse.Root(subblocks) => AST.Document(subblocks.map(nextRecursion(_)))(using b.b)
            case b@blockparse.CodeBlock(content, level) => AST.CodeBlock(nextRecursion(content).asInstanceOf[AST.UnformattedTextBlock], level)(using b.b)
            case b@blockparse.LatexBlock(content, level) => AST.LatexBlock(nextRecursion(content).asInstanceOf[AST.UnformattedTextBlock], level)(using b.b)
            case b@blockparse.ImportantBlock(subBlocks, level) => AST.ImportantBlock(subBlocks.map(nextRecursion(_)), level)(using b.b)
            case b@HeaderBlock(subblocks, level) => AST.HeaderBlock(subblocks.map(nextRecursion(_)), level)(using b.b)
            case b@blockparse.UnformattedTextBlock(startLine: Int, endLine: Int) =>
                val lines = file.slice(startLine, endLine).zipWithIndex.map((line, i) =>
                    AST.PlaintextLine(line)(using AST.TextSegment(startLine + i, startLine + i + 1), AST.PositionInfo(0, line.length, 0))
                )
                AST.UnformattedTextBlock(lines)(using b.b)
            case b@blockparse.FormattedTextBlock(startLine: Int, endLine: Int) =>
                val linesToParse: Vector[String] = file.slice(startLine, endLine)
                val parsedLineFormatters: Vector[AST.FormattedLine] = lineparse.LineParser.parse(file, startLine, endLine)
                AST.FormattedTextBlock(parsedLineFormatters)(using b.b)
        }
    }

    def buildAst(file: Vector[String]): AST.Document = {
        val root: blockparse.Root = BlockParser.parseBlocks(file)
        println(root.subblocks.mkString("\n"))
        blockToAST(root, false)(using file).asInstanceOf[AST.Document]
    }

    def buildIntervalTree(root: blockparse.Block): LineRange = {
        // If case distinction here gets annoying, consider upgrading the Block-AST to include types that immediately tell me about subblocks.
        def recurse(currentNode: blockparse.Block): LineRange = currentNode match
            case Root(subblocks) =>
                val lineRanges = subblocks.map(recurse)
                LineRange(lineRanges.head.start, lineRanges.last.end, lineRanges)
            case ImportantBlock(subblocks, level) => 
                val lineRanges = subblocks.map(recurse)
                LineRange(lineRanges.head.start, lineRanges.last.end, lineRanges)
            case HeaderBlock(subblocks, level) => 
                val lineRanges = subblocks.map(recurse)
                LineRange(lineRanges.head.start, lineRanges.last.end, lineRanges)
            case CodeBlock(content, level) => 
                val child = recurse(content)
                LineRange(child.start, child.end, Vector(child))
            case LatexBlock(content, level) =>
                val child = recurse(content)
                LineRange(child.start, child.end, Vector(child))
            case FormattedTextBlock(startLine, endLine) => LineRange(startLine, endLine, Vector())
            case UnformattedTextBlock(startLine, endLine) => LineRange(startLine, endLine, Vector())
        recurse(root)
    }

    // Tree containing file intervals
    final case class LineRange(start: Int, end: Int, children: Vector[LineRange])

}