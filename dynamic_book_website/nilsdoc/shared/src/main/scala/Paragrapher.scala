package ast

import blockparse.UnformattedTextBlock
import blockparse.FormattedTextBlock
import ASTBuilder.LineRange
import blockparse.Root
import blockparse.CodeBlock
import blockparse.LatexBlock
import blockparse.ImportantBlock

// TODO for paragraph splitting:
// [x] build separate interval tree with line start/ends by processing the tree we already have (clone structure)
// [x] find lines where paragraphs would get split (when > 1 empty line, add the trailing linebreaks to the previous paragraph)
// [ ] sliding-window style: go over list of paragraph lines, while querying current node of interval tree. Recurse by passing child nodes.
object Paragrapher {
    /* TODO
    * discontinued implementation, because multiple problems
    * - <br> statements still float around outside paragraphs (there's no elegant solution for empty paragraphs between 2 named blocks)
    * - splitting into paragraphs leaves document with awkward <br>/<p> ambivalence, causing the styling to become more complex
    * - in the first place, Word uses, I think, a kind of "pseudoparagraph" for newlines, so it's good to orient our design like this
    */
    //def applyParagraphing(root: blockparse.Root, lines: Vector[String]) = {
    //    val intervalTree = ASTBuilder.buildIntervalTree(root)
    //    val splits = findParagraphSplits(lines)
    //    splitBlocksOnParagraphs(root, root, intervalTree, intervalTree, splits, 0)
    //}

    //def splitBlocksOnParagraphs(root: blockparse.Root, currentBlock: blockparse.Block, intervalTreeRoot: LineRange, currentIntervalNode: LineRange, splits: Vector[Int], splitId: Int): blockparse.Block = {
    //    // tree recursion, sliding window style
    //    val currentSplit = splits(splitId)
    //    currentBlock match
    //        case Root(subblocks) =>
    //            blockparse.Root(subblocks.map(splitBlocksOnParagraphs(root, _, intervalTreeRoot, currentIntervalNode, splits, splitId)))
    //        case CodeBlock(content) =>
    //            blockparse.CodeBlock(splitBlocksOnParagraphs(root, content, intervalTreeRoot, currentIntervalNode, splits, splitId).asInstanceOf[UnformattedTextBlock])
    //        case LatexBlock(content) =>
    //            blockparse.LatexBlock(splitBlocksOnParagraphs(root, content, intervalTreeRoot, currentIntervalNode, splits, splitId).asInstanceOf[UnformattedTextBlock])
    //        case ImportantBlock(subblocks) =>
    //            blockparse.ImportantBlock(subblocks.map(splitBlocksOnParagraphs(root, _, intervalTreeRoot, currentIntervalNode, splits, splitId)))
    //        case b@FormattedTextBlock(startLine, endLine) =>
    //            if (startLine == currentSplit) {
    //                // the first line of the block is an empty line.
    //                // Don't split here, because at least 1 browser (the Firefox I tested this on) does not show an empty paragraph as a newline or any kind of vertical spacing.
    //                // We simply continue the recursion 1 step further.
    //                splitBlocksOnParagraphs(root, currentBlock, intervalTreeRoot, currentIntervalNode, splits, splitId + 1)
    //            } else if (startLine < splits(splitId) && splits(splitId) < endLine) {
    //                val paragraph1 = ???


    //            }
    //        case UnformattedTextBlock(startLine, endLine) =>
    //        case _ => ???
    //    
    //    ???
    //}

    //def findParagraphSplits(lines: Vector[String]): Vector[Int] = {
    //    var splitPoints = collection.mutable.ArrayBuffer[Int]()
    //    var i = 0
    //    var isOnEmptyLines = false
    //    while (i < lines.length) {
    //        val line = lines(i)
    //        if (line.isEmpty) {
    //            isOnEmptyLines = true
    //        } else if (isOnEmptyLines) {
    //            // first line that isn't empty after a bunch of empty lines => replace empty line before it with a paragraph end.
    //            splitPoints += i - 1 // note that i >= 1 here
    //            isOnEmptyLines = false
    //        }
    //        i += 1
    //    }
    //    splitPoints.toVector
    //}

}
