package lineparse

import scala.collection.mutable.ArrayBuffer
import scala.reflect.ClassTag
import scala.util.control.Breaks._
import ast.AST
import inlineparser.InlineParser
import ast.AST.TextSegment
import ast.AST.PositionInfo
import ast.AST.FormattedLine
import ast.AST.ListItem
import ast.AST.UnparagraphedLine
import ast.AST.ParagraphedLine

object LineParser {
  enum LineContextGroups {
    case ALL, NONE
  }
  trait LineContextObj {
    val delimiter: String
    val permittedSubcontexts: LineContextGroups

    type Args
    def isFormatterAboveCompatible(formatterAbove: Option[LineContext], myNestingLevel: Int): Boolean
    def isFormatterBeforeCompatible(formatterBefore: LineContext): Boolean
    def apply(a: Args): LineContext

  }
  object Title extends LineContextObj {
    type Args = NestedContextArg
    val delimiter: String = "#"
    val permittedSubcontexts: LineContextGroups = LineContextGroups.NONE
    def isFormatterAboveCompatible(formatterAbove: Option[LineContext], myNestingLevel: Int): Boolean = true

    def isFormatterBeforeCompatible(formatterBefore: LineContext): Boolean = !formatterBefore.isInstanceOf[Title]
      && formatterBefore.ifTypeEnsureProperty[Continuation](!_.continuedContext.isInstanceOf[Title])
    
  }
  case class NestedContextArg(nestingLevel: Int, startCol: Int)
  case class Title(a: NestedContextArg) extends LineContext(a.startCol, a.startCol + a.nestingLevel)
  sealed abstract class LineparserList(val a: NestedContextArg) extends LineContext(a. startCol, a.startCol + a.nestingLevel) {
    protected def getAstConstructor: (items: Vector[ListItem], nestingLevel: Int, textSegment: AST.TextSegment) => (AST.OrderedList | AST.UnorderedList)
    def makeAstNode(listItems: Vector[ListItem], lineNum: Int, lineOffset: Int) = {
      var itemCount = 0
      for (item <- listItems) {
        itemCount += item.parts.length
      }
      getAstConstructor(listItems.toVector, a.nestingLevel, AST.TextSegment(lineNum + lineOffset, lineNum + lineOffset + itemCount))
    }
  }
  case class OrderedList(override val a: NestedContextArg) extends LineparserList(a) {
    override protected def getAstConstructor = AST.OrderedList(_, _)(using _)
  }
  case class UnorderedList(override val a: NestedContextArg) extends LineparserList(a) {
    override protected def getAstConstructor = AST.UnorderedList(_, _)(using _)
  }
  case class Continuation(continuedContext: LineContext) extends LineContext(continuedContext.formatterPos, continuedContext.continuantPos) {}

  def areSublistLevelsCorrect(parent: LineparserList, child: LineparserList) = parent.getClass == child.getClass && parent.a.nestingLevel + 1 == child.a.nestingLevel
  def canBeChildList(parent: LineparserList, child: LineparserList) =
    parent.ensureTypeAndProperty[OrderedList](f => child.ensureTypeAndProperty[OrderedList](s => areSublistLevelsCorrect(f, s))) || parent.ensureTypeAndProperty[UnorderedList](f => child.ensureTypeAndProperty[UnorderedList](s => areSublistLevelsCorrect(f, s)))

  object OrderedList extends LineContextObj {
    type Args = NestedContextArg
    val delimiter: String = "+"
    val permittedSubcontexts: LineContextGroups = LineContextGroups.ALL

    def isFormatterAboveCompatible(formatterAbove: Option[LineContext], myNestingLevel: Int): Boolean =
      myNestingLevel == 0 ||
        (formatterAbove.nonEmpty && 
         (formatterAbove.get.ensureTypeAndProperty[OrderedList](o => o.a.nestingLevel + 1 >= myNestingLevel) ||
          formatterAbove.get.ensureTypeAndProperty[Continuation](c => c.continuedContext.ensureTypeAndProperty[OrderedList](o => o.a.nestingLevel + 1 >= myNestingLevel))))

    def isFormatterBeforeCompatible(formatterBefore: LineContext): Boolean = !formatterBefore.isInstanceOf[Title] && !formatterBefore.isInstanceOf[OrderedList]
      && formatterBefore.ifTypeEnsureProperty[Continuation](c => !c.continuedContext.isInstanceOf[OrderedList] && !c.continuedContext.isInstanceOf[Title])
  }

  def areSameListLevel(first: LineparserList, second: LineparserList) = {
    first.getClass == second.getClass && first.a.nestingLevel == second.a.nestingLevel
  }

  object UnorderedList extends LineContextObj {
    type Args = NestedContextArg
    val delimiter: String = "*"
    val permittedSubcontexts: LineContextGroups = LineContextGroups.ALL

    def isFormatterAboveCompatible(formatterAbove: Option[LineContext], myNestingLevel: Int): Boolean =
      myNestingLevel == 0 ||
        (formatterAbove.nonEmpty && 
           (formatterAbove.get.ensureTypeAndProperty[UnorderedList](o => o.a.nestingLevel + 1 >= myNestingLevel) ||
            formatterAbove.get.ensureTypeAndProperty[Continuation](c => c.continuedContext.ensureTypeAndProperty[UnorderedList](o => o.a.nestingLevel + 1 >= myNestingLevel))))

    def isFormatterBeforeCompatible(formatterBefore: LineContext): Boolean =
      !formatterBefore.isInstanceOf[Title] && !formatterBefore.isInstanceOf[UnorderedList]
        && formatterBefore.ifTypeEnsureProperty[Continuation](c => !c.continuedContext.isInstanceOf[UnorderedList] && !c.continuedContext.isInstanceOf[Title])
  }

  abstract class LineContext(val formatterPos: Int, val continuantPos: Int) {
    // precondition: formatterPos < line.length && continuantPost < line.length, where line is whereever this context came from.

    def positionAfterFormatter = continuantPos + 2

    def ensureTypeAndProperty[Sub <: LineContext: ClassTag](property: Sub => Boolean) = this match {
      case s: Sub => property(s)
      case _      => false
    }

    def ifTypeEnsureProperty[Sub <: LineContext: ClassTag](property: Sub => Boolean) = this match {
      case s: Sub => property(s)
      case _      => true
    }
  }

  def findListItems(currentContext: LineparserList, lineNum: Int, contextPos: Int, isVisited: ArrayBuffer[ArrayBuffer[Boolean]])(using allContexts: ArrayBuffer[ArrayBuffer[LineContext]], wholeFile: Vector[String], lineOffset: Int): ArrayBuffer[ListItem] = {
    var i = lineNum
    val listItems = ArrayBuffer[ArrayBuffer[FormattedLine]]() // these will become the matched OrderedList's s items
    while (i < allContexts.length && contextPos < allContexts(i).length) {
      //println(s"find list items $i, $contextPos")
      val potentialListItem: LineContext = allContexts(i)(contextPos)
      potentialListItem match {
        case Continuation(continuedContext) => {
          //println("found continuation")
          val isAnotherContinuationToTheRight = contextPos + 1 < allContexts(i).length && allContexts(i)(contextPos + 1).isInstanceOf[Continuation]
          isVisited(i)(contextPos) = true
          if (!isAnotherContinuationToTheRight) {
            /* We're looking at a continuation of the `currentContext`'s most recent list item (`listItems.last`).
             * There is no continuation to the right, so the continuation really does belong to this list (and not a child of it)
             * Continuations remain part of the same HTML <li>-element, so recurse into it and add it to `listItems.last`.
             */
            staircaseRecurse(i, contextPos + 1, isVisited).foreach(
              listItems.last += _
            )
          }
        }
        case l: LineparserList if areSublistLevelsCorrect(currentContext, l) => {
          //println("found sublist")
          /* We found a sublist. This should become a new <ol>-element, so first add a new item (<li>)
           * and then recurse into the new list context.
           */

          // mark isvisited?
          staircaseRecurse(i, contextPos, isVisited).foreach(item =>
            listItems += ArrayBuffer[FormattedLine](item)
          )
        }
        case l: LineparserList if areSameListLevel(currentContext, l) => {
          //println("found list item")
          /* we found a new list item that will become a new <li>-element, so add a new item.
           * However, it does not become a new <ol>, so recurse only to find the item's contents (which is one to the right)
           */
          isVisited(i)(contextPos) = true // remember to mark list as visited (though not strictly necessary)
          staircaseRecurse(i, contextPos + 1, isVisited).foreach(item =>
            listItems += ArrayBuffer[FormattedLine](item)
          )
        }
        case _ => i = allContexts.length // break
      }
      i += 1
    }
    var l = lineNum
    listItems.map(itemContent =>
      val listItem = ListItem(itemContent.toVector)(using AST.TextSegment(l + lineOffset, l + lineOffset + itemContent.length))
      l += itemContent.length
      listItem
    )
  }

  def staircaseRecurse(lineNum: Int, contextPos: Int, isVisited: ArrayBuffer[ArrayBuffer[Boolean]])(using allContexts: ArrayBuffer[ArrayBuffer[LineContext]], wholeFile: Vector[String], lineOffset: Int): Option[FormattedLine] = {
    if (isVisited(lineNum)(contextPos)) {
      //println(s"staircase recurse (visited) $lineNum, $contextPos")
      None
    } else if (contextPos >= allContexts(lineNum).length) {
      //println(s"staircase recurse (OOB) $lineNum, $contextPos")
      isVisited(lineNum)(contextPos) = true
      val line = wholeFile(lineOffset + lineNum)
      val inlineContentStart = allContexts(lineNum).lastOption.map(_.positionAfterFormatter).getOrElse(0)
      val inlineContentEnd = line.length // may require modifcation once tables come into play!
      Some(InlineParser.parseInlineContent(lineOffset + lineNum, inlineContentStart, inlineContentEnd))
    } else {
      isVisited(lineNum)(contextPos) = true
      val currentContext = allContexts(lineNum)(contextPos)
      // TODO: if visited, we shouldn't into this method! It gets very annoying to pass back an option.
      //println(s"staircase recurse $lineNum, $contextPos")
      currentContext match {
        case _: Continuation =>
          throw new IllegalArgumentException("Method was called when on continuation!")
        case l: LineparserList =>
          val listItems = findListItems(l, lineNum, contextPos, isVisited)
          Some(l.makeAstNode(listItems.toVector, lineNum, lineOffset))
        case Title(a) =>
          val titleItems = ArrayBuffer[AST.UnparagraphedLine]()
          staircaseRecurse(lineNum, contextPos + 1, isVisited).foreach(l =>
            val parsedLine = l.asInstanceOf[ParagraphedLine]
            titleItems += UnparagraphedLine(parsedLine.line)(using parsedLine.l, parsedLine.i)
          )
          var i = lineNum + 1
          while (i < allContexts.length && contextPos < allContexts(i).length && allContexts(i)(contextPos).isInstanceOf[Continuation]) {
            isVisited(i)(contextPos) = true
            staircaseRecurse(i, contextPos + 1, isVisited).foreach(l =>
              val parsedLine = l.asInstanceOf[ParagraphedLine]
              titleItems += UnparagraphedLine(parsedLine.line)(using parsedLine.l, parsedLine.i)
            )
            i += 1
          }
          Some(AST.Title(titleItems.toVector, a.nestingLevel)(using AST.TextSegment(lineOffset + lineNum, lineOffset + lineNum + titleItems.length)))
      }
    }
  }

  val continuant = '|'
  val formatterSeparator = " "
  val allFormatters = Seq(OrderedList, UnorderedList, Title)

  def tryToStartFormatter(line: String, col: Int, contextAbove: Option[LineContext], previousContextOnLine: Option[LineContext]): Option[(LineContext, Int)] = {
    if (previousContextOnLine.exists(_.formatterPos != col)) {
      // if a formatter is above us, we can only start a new formatter at the exact same column. This is the easiest to check, so for speed reasons we check this first.
      None
    }
    var foundContext: Option[(LineContext, Int)] = None
    var j = 0
    while (j < allFormatters.length && foundContext.isEmpty) {
      val formatter = allFormatters(j)
      val matches = util.ParseUtils.countRepeatedMatchAndThen(line, col, formatter.delimiter, formatterSeparator)
      val nestingLevel = matches - 1
      // TODO what are these conditions man. Double check everything here, I found three mistakes already. Also better to rewrite it to whitelist.
      val isValidContext = matches > 0
        && (previousContextOnLine.isEmpty || formatter.isFormatterBeforeCompatible(previousContextOnLine.get))
        && (formatter.isFormatterAboveCompatible(contextAbove, nestingLevel))
      if (isValidContext) {
        foundContext = Some(formatter.apply(NestedContextArg(nestingLevel, col).asInstanceOf[formatter.Args]), col + matches * formatter.delimiter.length + formatterSeparator.length)
      }
      j += 1
    }
    foundContext
  }

  def parseLine(line: String, contextsAbove: Vector[LineContext]): (ArrayBuffer[LineContext], Int) = {
    val contextsOnThisLine = ArrayBuffer[LineContext]()
    var addedAtLeastOneContinuant = false
    var allContinuantsWereImplicit = true
    var i = 0
    var col = 0
    breakable {
      while (col < line.length) {
        val contextAboveCurrentColumn = contextsAbove.lift(i)
        //println(s"col=$col, i=$i, line=$line, context above=$contextAboveCurrentColumn, all above=$contextsAbove")
        if ((line(col) == ' ' || line(col) == continuant) && col+1 < line.length && line(col+1) == ' ' && contextAboveCurrentColumn.nonEmpty && col == contextAboveCurrentColumn.get.continuantPos) {
          // On a space or continuant char, we continue whatever we found above us.
          val contextAbove = contextAboveCurrentColumn.get
          val newContinuation = contextAbove match {
            case continuationAbove: Continuation => Continuation(continuationAbove.continuedContext)
            case _                               => Continuation(contextAbove)
          }
          contextsOnThisLine += newContinuation
          addedAtLeastOneContinuant = true
          if (line(col) == continuant) {
            // the user wrote an explicit continuation character
            allContinuantsWereImplicit = false
          }
          col = contextAbove.positionAfterFormatter // uh oh, double check if this logic holds.
        } else if (line(col) == ' ' && contextsAbove.nonEmpty && col < contextsAbove.last.continuantPos) {
          col += 1
        } else {
          val newContext = tryToStartFormatter(line, col, contextAboveCurrentColumn, contextsOnThisLine.lastOption)
          if (newContext.nonEmpty) {
            // we found a new context. In all cases, if a new formatter is found, the context from the line above is thrown away. Also adjust cursor position.
            col = newContext.get._2
            contextsOnThisLine += newContext.get._1
            i = contextsAbove.length
          } else {
            // we aren't on a space, haven't matched a continuant, and haven't matched a formatter. Therefore, lineparsing is over.
            break
          }
        }

        while (contextsAbove.lift(i).nonEmpty && col > contextsAbove(i).continuantPos) {
          i += 1
        }
      }
    }

    if (addedAtLeastOneContinuant && allContinuantsWereImplicit) {
        // the user didn't provide any continuants right till the end. Therefore, no formatters are continued.
        return (ArrayBuffer(), 0)
    } else {
        return (contextsOnThisLine, col)
    }
  }

  def parse(wholeFile: Vector[String], startLine: Int, endLine: Int): Vector[FormattedLine] = {
    var contextsAbove: Vector[LineContext] = Vector()
    var allContexts: ArrayBuffer[ArrayBuffer[LineContext]] = ArrayBuffer()
    var i = startLine
    while (i < endLine) {
      val (newContexts, lineformatterEndPos) = parseLine(wholeFile(i), contextsAbove)
      contextsAbove = newContexts.toVector
      allContexts.append(newContexts)
      i += 1
    }
    println("Lineparser:________________")
    println(allContexts.zipWithIndex.map((s, i) => s"$i $s").mkString("\n"))
    println("________________")
    // todo: put in a step that extracts table information

    val isVisited = ArrayBuffer[ArrayBuffer[Boolean]]()
    allContexts.foreach(contextRow => isVisited += ArrayBuffer.fill[Boolean](contextRow.length + 1)(false))
    0.until(endLine-startLine).flatMap(i =>
      staircaseRecurse(i, 0, isVisited)(using allContexts, wholeFile, startLine)
    ).toVector
  }


}
