package api

object Routes {
  opaque type UrlEncodedString = String



  def urlEncode(unencoded: String): UrlEncodedString = java.net.URLEncoder.encode(unencoded, "UTF-8")

  def urlDecode(encoded: UrlEncodedString): String = java.net.URLDecoder.decode(encoded, "UTF-8")

  def nilsdocPutUrl(): String = "/document"

  def nilsObjectPostOrDeleteUrl(): String = "/nilsdocObject"

  def makeNilsdocGetUrl(documentTitle: String, userId: String): String = 
    val unencodedUrl = s"/users/$userId/documents/$documentTitle"
    val encodedUrl = urlEncode(unencodedUrl)
    encodedUrl

  def emptyDoc() = "/users/user/newDocument/empty"

  def makePublishUrl(userId: String, documentTitle: String) =
    s"/users/${userId}/documents/${documentTitle}/publish"
}
