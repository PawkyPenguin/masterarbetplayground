package api
import globals.Config
import globals.Config.DynamicComponentHtmlClasses.NilsobjType
import globals.Config.DynamicComponentHtmlClasses
import upickle.default.ReadWriter
import collection.mutable.ArrayBuffer
import globals.Config
import globals.Config.DynamicComponentHtmlClasses
import upickle.default._
import api.Routes

object Nilsobjects {

  sealed abstract class NilsobjectPayload(val name: String) derives ReadWriter {
    //def pickle = upickle.default.write(this)
    def objectType: NilsobjType
    def isEmpty: Boolean
  }

  trait Emptiable[A <: NilsobjectPayload] {
    def emptyContent(name: String): A
  }

  case class NilsdocLocation(y: Int, x: Option[Int]) derives ReadWriter
  case class Note(override val name: String, location: NilsdocLocation, note: String) extends NilsobjectPayload(name) {
    override def isEmpty = false
    override def objectType: NilsobjType = DynamicComponentHtmlClasses.note
  }

  case class Tree(override val name: String, root: TreeNode) extends NilsobjectPayload(name) {
    def objectType: NilsobjType = Config.DynamicComponentHtmlClasses.tree
    def isEmpty = root.children.isEmpty && (root.text == TreeNode.defaultNodeText || root.text.isBlank)
  }

  given Emptiable[Tree] with {
  def emptyContent(name: String): Tree = Tree(name, TreeNode.empty)
  }
  object TreeNode {
  val defaultNodeText = "text"
  def empty = TreeNode(ArrayBuffer(), defaultNodeText)
  }
  case class TreeNode(children: ArrayBuffer[TreeNode], var text: String) derives ReadWriter

  case class Audio(override val name: String, var data: Array[Byte]) extends NilsobjectPayload(name) {
    def objectType: NilsobjType = Config.DynamicComponentHtmlClasses.audio
    def isEmpty: Boolean = data.isEmpty
  }
  given Emptiable[Audio] with {
    def emptyContent(name: String): Audio = Audio(name, Array())
  }


  given Emptiable[Summary] with {
    def emptyContent(name: String): Summary = Summary(name, "")
  }

  case class Summary(override val name: String, writtenSummary: String) extends NilsobjectPayload(name) {
      def objectType = Config.DynamicComponentHtmlClasses.summary
      def isEmpty = writtenSummary.isBlank()
  }

}
