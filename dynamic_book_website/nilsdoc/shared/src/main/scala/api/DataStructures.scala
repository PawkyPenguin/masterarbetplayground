package api

import upickle.default.ReadWriter
import api.Routes.UrlEncodedString
import scalatags.Text.all._
import globals.Config
import globals.Config.DynamicComponentHtmlClasses
import globals.Config.DynamicComponentHtmlClasses.NilsobjType
import scalatags.Text.TypedTag
import api.Nilsobjects._

object DataStructures {
  import upickle.default._
  given ReadWriter[Routes.UrlEncodedString] = readwriter[String].bimap(Routes.urlDecode(_), Routes.urlEncode(_))

  case class Nilsobject(userId: Int, documentAuthor: Int, documentTitle: String, name: String, pickledContent: String, objectType: String) derives ReadWriter {
    def unpickle[A : ReadWriter] = upickle.default.read[A](pickledContent)
  }

  object Nilsobject{
    def fromPayload[A <: Nilsobjects.NilsobjectPayload : ReadWriter](userId: Int, documentAuthor: Int, documentTitle: String, payload: A) =
      Nilsobject(userId, documentAuthor, documentTitle, payload.name, upickle.default.write(payload), payload.objectType.asString)
  }

  def getPublishForReaderButtonMessage(checkedUserCount: Int) = s"Publish for $checkedUserCount " + (if (checkedUserCount == 1) "person" else "people")
  case class NewReadPermissions(readerIds: Seq[Int]) derives ReadWriter

  case class Document(id: Option[Int], authorId: Int, versionNumber: Long, title: String, content: String) derives ReadWriter
  case class User(id: Option[Int], username: String, password: String) derives ReadWriter

  // Create the Html for nilsobjects that live in the document
  def makeNilsobjDocumentDiv(name: String, objClass: NilsobjType, additionalClasses: Seq[String] = Seq(), additionalData: Seq[(String, String)] = Seq()): scalatags.Text.TypedTag[?] =
    div(
      id := Config.Ref.makeRefTargetId(name),
      `class` := (objClass +: additionalClasses).mkString(" "),
      additionalData.map(
        data(_) := _
      ),
      data(Config.DynamicComponentHtmlClasses.nilsobjTypeData) := objClass.asString,
      data(Config.DynamicComponentHtmlClasses.nilsobjNameData) := name
    )

  def makeNilsobjDocumentSpan(name: String, objClass: NilsobjType, additionalClasses: Seq[String] = Seq(), additionalData: Seq[(String, String)] = Seq()): scalatags.Text.TypedTag[?] =
    span(
      id := Config.Ref.makeRefTargetId(name),
      `class` := (objClass +: additionalClasses).mkString(" "),
      additionalData.map(
        data(_) := _
      ),
      data(Config.DynamicComponentHtmlClasses.nilsobjTypeData) := objClass.asString,
      data(Config.DynamicComponentHtmlClasses.nilsobjNameData) := name
    )


  def getNilsobjStorageClassFromType(objClass: String) = objClass + "-storage" // used inside storage elements to get, for example, a class="nd-tree-storage"

  class SharedHtmlTemplates[Builder, Output <: FragT, FragT](val bundle: scalatags.generic.Bundle[Builder, Output, FragT]){
    import bundle.all._
    def makeStorageHtmlForNilsobj(name: String, payload: NilsobjectPayload): bundle.Tag  = // same implementation as other method of same name, but for clientside calls.
      script(
        id := DynamicComponentHtmlClasses.makeNilsobjStorageId(name),
        `type` := "application/json",
        `class` := Seq(DynamicComponentHtmlClasses.ndNilsobjStorage, getNilsobjStorageClassFromType(payload.objectType.asString)).mkString(" "),
        data(Config.DynamicComponentHtmlClasses.nilsobjTypeData) := payload.objectType.asString,
        data(Config.DynamicComponentHtmlClasses.nilsobjNameData) := name,
        raw(upickle.default.write(payload))
      )
  }

}
