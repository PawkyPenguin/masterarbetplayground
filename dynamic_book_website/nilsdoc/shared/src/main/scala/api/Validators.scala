package api

object Validator {
  case class Condition[A](pred: A => Boolean, errorMessage: String)

  def getErrors[A](toValidate: A, validators: Seq[Condition[A]]): Seq[String] = validators.collect {
    case v if !v.pred(toValidate) => v.errorMessage
  }

  def validateOrPrintError[A, Error](toValidate: A, validators: Seq[Condition[A]], errorPrinter: Seq[String] => Error): Either[A, Error] = 
    getErrors(toValidate, validators) match {
      case Seq() => Left(toValidate)
      case Seq(a:_*) => Right(errorPrinter(a))
    }
}

abstract class Validator[A] {
  val validators: Seq[Validator.Condition[A]]
  def validateOrPrintError[Error](s: A, errorPrinter: Seq[String] => Error) = Validator.validateOrPrintError(s, validators, errorPrinter)
  def isValid(a: A) = validators.forall(_.pred(a))
}

object TitleValidator extends Validator[String] {
  val maxTitleLength = 40
  val Condition = Validator.Condition[String]
  val hasValidTitle = Condition(_.matches("[a-zA-Z0-9 _-]*"), """The title must only contain alphanumeric characters, spaces, and "_" or "-"""")
  val isNonEmpty = Condition(!_.isBlank, "The title must contain text")
  val isShortEnough = Condition(_.length < maxTitleLength, s"The title must be shorter than $maxTitleLength")
  
  override val validators = Seq(hasValidTitle, isNonEmpty, isShortEnough)
}
