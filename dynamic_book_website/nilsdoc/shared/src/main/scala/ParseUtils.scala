package util
object ParseUtils {
    def matchExactly(line: String, startOffset: Int, toMatch: String): Int = {
        var i = 0
        var matches = 0
        while (line.startsWith(toMatch, startOffset + i)) {
            i += toMatch.length
            matches += 1
        }
        if (startOffset + i == line.length) {
            matches
        } else {
            0
        }
    }


    def countRepeatedMatch(line: String, startOffset: Int, toMatch: String): Int = {
        var matches = 0
        var i = 0
        while (line.startsWith(toMatch, startOffset + i)) {
            i += toMatch.length
            matches += 1
        }
        matches
    }

    def countRepeatedMatchAndThen(line: String, startOffset: Int, toMatch: String, followText: String): Int = {
        val matches = countRepeatedMatch(line, startOffset, toMatch)
        if (matches > 0) {
            if (line.startsWith(followText, startOffset + matches)) {
                matches
            } else {
                0
            }
        } else {
            0
        }
    }
}
