package inlineparser

import scala.collection.mutable.ArrayBuffer
import lineparse.LineParser
import scala.CanEqual.derived
import scala.util.control.Breaks._
import util.ParseUtils
import java.util.jar.Attributes.Name
import globals.Config.FormatterNames
import ast.AST

object InlineParser {
    import ast.AST._
    import scala.util.matching.Regex

    trait InlineFormatterObj {
        val delimiter: String
        val contentIsPlaintext: Boolean

        type ConstructorArgs
        def apply(a: ConstructorArgs)(using LineArgs, InlineArgs): InlineNode
    }

    object InlineFormatterSymbols {
        val bold = "*"
        val italic = "/"
        val code = "`"
        val underline = "_"
        val terminator = "}"
        val escapeChar = "\\"

        val named = "{"

        def isFormatSymbol(line: String, pos: Int): Boolean =
            line.startsWith(bold, pos) ||
                line.startsWith(named, pos) ||
                line.startsWith(italic, pos) ||
                line.startsWith(code, pos) ||
                line.startsWith(underline, pos) ||
                line.startsWith(terminator, pos)

    }

    case class RecursionResult(nodes: Vector[InlineNode], endPos: Int)

    def getNamedFormatterBuilder(lineNum: Int, formatterStartPos: Int, name: String, formatterLevel: Int): (NamedFormatterObj, FormatBuilder) = {
        val textSegment = TextSegment(lineNum, lineNum + 1)
        val allFormatters = Seq(NamedUrl, InlineImage, InlineMath, RightAlign, LeftAlign, CenterAlign, GridCanvas, SummaryPrompt, Ref, TreeGraph, Audio, ImportantInline)
        allFormatters.foreach ( formatter =>
            if (formatter.name == name.toLowerCase()) {
                val buildingFunction: FormatBuilder = (args: Any, formatterEndPos: Int) => {
                    val f = formatter.apply(args.asInstanceOf[formatter.ConstructorArgs])(using textSegment, PositionInfo(formatterStartPos, formatterEndPos, formatterLevel))
                    println(s"done casting")
                    f
                }
                val matchedFormatter = formatter
                return (matchedFormatter, buildingFunction)
            }
        )

        val errorBuilder: FormatBuilder = (args: Any, formatterEndPos: Int) =>
            NamedFormatterError(
                NamedFormatterErrorArg(
                    s"Named formatter `$name` does not exist",
                    args.asInstanceOf[String],
                    name
                )
            )(using textSegment, PositionInfo(formatterStartPos, formatterEndPos, formatterLevel))
        (NamedFormatterError, errorBuilder)
        /* TODO: Think about case where nothing matches:
         * (a => NamedFormatterError(s"Named formatter `$name` does not exist", a.asInstanceOf[String], formatterLevel, name), true)
         */
    }


    type FormatBuilder = (constructorArgs: Any, colEndPos: Int) => InlineNode
    case class TentativeFormattingContext(builder: FormatBuilder, formatType: InlineFormatterObj, contentIsPlaintext: Boolean, terminatorsRequired: Int, delimiterStartPos: Int)

    val allUnnamedFormatters = Seq(Bold, Italic, Underline, Code)
    def tryMatchFormatter(lineNum: Int, line: String, pos: Int, requiredDelimiters: Int, activeFormatters: Vector[InlineFormatterObj]): (Int, Option[TentativeFormattingContext]) = {
        var i = 0
        while (i < allUnnamedFormatters.length) {
            val formatter = allUnnamedFormatters(i)
            val matches = ParseUtils.countRepeatedMatch(line, pos, formatter.delimiter)
            if (matches >= requiredDelimiters && isValidFormatter(activeFormatters, formatter)) {
                val textSegment = TextSegment(lineNum, lineNum + 1)
                val formatBuilder: FormatBuilder = (args: Any, colEndPos: Int) => {
                        val positionInfo = PositionInfo(pos, colEndPos, matches)
                        formatter.apply(args.asInstanceOf[formatter.ConstructorArgs])(using textSegment, positionInfo)
                    }
                val newParserPos = pos + matches * formatter.delimiter.length
                val foundContext = TentativeFormattingContext(formatBuilder, formatter, formatter.contentIsPlaintext, matches, pos)
                return (newParserPos, Some(foundContext))
            }
            i += 1
        }


        val namedDelimiters = ParseUtils.countRepeatedMatch(line, pos, InlineFormatterSymbols.named)
        if (namedDelimiters >= requiredDelimiters) {
            val nameStart: Int = pos + InlineFormatterSymbols.named.length * namedDelimiters
            val name = line.drop(nameStart).takeWhile(c => c.isLetter)
            val isSpaceNext = line.lift(nameStart + name.length).exists(_ == ' ')
            val isBraceNext = line.lift(nameStart + name.length).exists(_ == '}')
            if (isSpaceNext || isBraceNext) {
                val (formatType, formatBuilder) = getNamedFormatterBuilder(lineNum, pos, name, namedDelimiters)
                val foundContext = TentativeFormattingContext(formatBuilder, formatType, formatType.contentIsPlaintext, namedDelimiters, pos)
                if (isSpaceNext) {
                    return (nameStart + name.length + 1, Some(foundContext))
                } else {
                    return (nameStart + name.length, Some(foundContext)) // intentionally don't consume brace, so parsing logic is consistent.
                }
            } else {
                return (nameStart + name.length + 1, None)
            }
        }
        (pos + 1, None)
    }

    def isValidFormatter(activeFormatters: Vector[InlineFormatterObj], formatter: InlineFormatterObj): Boolean = !activeFormatters.contains(formatter)

    def recurse(lineNum: Int, line: String, startPos: Int, endPos: Int, requiredDelimiters: Int, contentIsPlaintext: Boolean, activeInlineFormatters: Vector[InlineFormatterObj], currentFormatterBuilder: Option[TentativeFormattingContext]): RecursionResult = {
        var i = startPos
        var isInEscapeSequence = false
        var currentPlaintext = ""
        var nodes = ArrayBuffer[InlineNode]()
        var nodeDivisionIndices = ArrayBuffer[Int](startPos)
        var plaintextStart = 0
        // val requiredDelimiters = currentFormatterBuilder.map(_.terminatorsRequired).getOrElse(1) // TODO: enable this instead?
        // val contentIsPlaintext = currentFormatterBuilder.map(_.contentIsPlaintext).getOrElse(false) // TODO: enable this instead?
        while (i < endPos) {
            if (isInEscapeSequence) {
                if (!InlineFormatterSymbols.isFormatSymbol(line, i) && line(i) != '\\' && false) { // TODO: fix backslash bug >:(
                    // add the preceding backslash character as a literal backslash if it wasn't used to escape something.
                    currentPlaintext += '\\'
                }
                currentPlaintext += line(i)
                isInEscapeSequence = false
            } else if (line(i) == '\\') {
                isInEscapeSequence = true
            } else {
                val foundTerminators = ParseUtils.countRepeatedMatch(line, i, InlineFormatterSymbols.terminator)
                if (foundTerminators >= requiredDelimiters && activeInlineFormatters.length > 0) {
                    // If we hit the termination symbol and are in an inline formatter. Finish up the recursion and return all current nodes
                    val formatterEndPos = i + foundTerminators * InlineFormatterSymbols.terminator.length
                    val formatter =
                        if (contentIsPlaintext) {
                            currentFormatterBuilder.get.builder(currentPlaintext, formatterEndPos)
                        } else {
                            // TODO: the Text node stops at i-1
                            val newTextNode = Text(currentPlaintext)(using TextSegment(lineNum, lineNum+1), PositionInfo(nodeDivisionIndices.last, i, 0))
                            currentFormatterBuilder.get.builder((nodes :+ newTextNode).toVector, formatterEndPos)
                        }
                    return RecursionResult(Vector(formatter), i + requiredDelimiters * InlineFormatterSymbols.terminator.length - 1)
                } else if (!contentIsPlaintext) {
                    val (newPos: Int, maybeContext: Option[TentativeFormattingContext]) = tryMatchFormatter(lineNum, line, i, requiredDelimiters, activeInlineFormatters)
                    if (maybeContext.nonEmpty) {
                        // We've encountered a new formatter. Put all text up to now into a plaintext node and recurse into the formatter.
                        val context: TentativeFormattingContext = maybeContext.get
                        nodes += Text(currentPlaintext)(using TextSegment(lineNum, lineNum + 1), PositionInfo(nodeDivisionIndices.last, i, 0))
                        currentPlaintext = ""
                        val r = recurse(lineNum, line, newPos, endPos, context.terminatorsRequired, context.contentIsPlaintext, activeInlineFormatters :+ context.formatType, Some(context))
                        nodeDivisionIndices += r.endPos+1
                        nodes ++= r.nodes
                        i = r.endPos
                    } else {
                        currentPlaintext += line.slice(i, newPos)
                        i = newPos - 1
                    }
                } else { // contentIsPlaintext == true
                    currentPlaintext += line(i)
                }
            }

            i += 1
        }
        if (isInEscapeSequence) {
            currentPlaintext += '\\'
        }

        /* Since we hit the end of the line without finding the termination symbol, we're in a plaintext node.
         * Add the current formatting symbol to the plaintext and return an unformatted node.
         */
        nodes += Text(currentPlaintext)(using TextSegment(lineNum, lineNum+1), PositionInfo(nodeDivisionIndices.last, i, 0))
        val textNodes =
            currentFormatterBuilder match {
                case Some(context) => Text(line.slice(context.delimiterStartPos, startPos))(using TextSegment(lineNum, lineNum+1), PositionInfo(context.delimiterStartPos, startPos, 0)) +: nodes.toVector
                case None          => nodes.toVector
            }
        RecursionResult(textNodes, i)
    }

    def parseInlineContent(lineNum: Int, startCol: Int, endPos: Int)(using wholeFile: Vector[String]): ParagraphedLine = {
        val line = wholeFile(lineNum)
        val inlineNodes = recurse(lineNum, line, startCol, endPos, 1, false, Vector(), None).nodes
        val lineRange = TextSegment(lineNum, lineNum + 1)
        val colRange = PositionInfo(startCol, inlineNodes.last.i.untilCol, 0)
        ParagraphedLine(inlineNodes)(using lineRange, colRange)
    }
}
