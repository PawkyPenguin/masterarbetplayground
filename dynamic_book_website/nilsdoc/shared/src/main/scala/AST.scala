package ast
import globals.Config
import globals.Config.Grid
import javax.print.Doc
import ast.AST._
import ast.AstToHtml.makeAttribute
import scala.collection.mutable.ArrayBuffer
import scala.concurrent.Future
import javax.swing.plaf.nimbus.State
import globals.Config.DynamicComponentHtmlClasses
import ast.AstToAnnotatedEditor.namedBlock
import api.DataStructures
import lineparse.LineParser
import inlineparser.InlineParser.InlineFormatterObj

object AST {
  import scalatags.Text.all._ // TODO: move all html generation to the visitor. Idea of a visitor is to define behavior after all

  abstract class NamedFormatterObj extends InlineFormatterObj {
    override val delimiter = "{"
    val name: String
    override val contentIsPlaintext: Boolean = true // set this to true for now. Later, maybe let its meaning apply to the first (main) argument only.

    override def apply(args: ConstructorArgs)(using LineArgs, InlineArgs): NamedFormatter
  }

  trait HasLineArgs(using a: LineArgs)
  trait HasInlineArgs(using val i: InlineArgs)
  type LineArgs = (TextSegment)
  type InlineArgs = (PositionInfo)

  case class TextSegment(fromLine: Int, untilLine: Int)
  case class PositionInfo(fromCol: Int, untilCol: Int, formatterLevel: Int)

  sealed abstract class Node(using val l: LineArgs) extends HasLineArgs {
    def postprocess(root: Document): Either[Document, NamedFormatterError] = {
      /* TODO: Use & overwrite this method in nodes that need postprocessing, like rehanging themselves or spitting out errors.
       * Then check speed requirements! Is case class cloning fast enough? Otherwise I need a mutable AST; don't be afraid of this.
       * I don't want my compiler to be slow under any circumstances.
       */
      Left(root)
    }
  }
  sealed abstract class Block(using a: LineArgs) extends Node
  final case class Document(blocks: Vector[Block])(using a: LineArgs) extends Block
  final case class CodeBlock(subblock: UnformattedTextBlock, level: Int)(using a: LineArgs) extends Block
  final case class LatexBlock(subblock: UnformattedTextBlock, level: Int)(using a: LineArgs) extends Block
  final case class ImportantBlock(subblocks: Vector[Block], level: Int)(using a: LineArgs) extends Block
  final case class HeaderBlock(subblocks: Vector[Block], level: Int)(using a: LineArgs) extends Block
  final case class FormattedTextBlock(lines: Vector[FormattedLine])(using a: LineArgs) extends Block
  final case class UnformattedTextBlock(lines: Vector[PlaintextLine])(using a: LineArgs) extends Block

  sealed abstract class FormattedLine(using args: LineArgs) extends Node
  final case class OrderedList(items: Vector[ListItem], nestingLevel: Int)(using LineArgs) extends FormattedLine
  final case class UnorderedList(items: Vector[ListItem], nestingLevel: Int)(using LineArgs) extends FormattedLine
  final case class ListItem(parts: Vector[FormattedLine])(using LineArgs) extends FormattedLine
  final case class Table(startCol: Int, endCol: Int, rows: Vector[TableRow])(using LineArgs) extends FormattedLine
  final case class TableRow(cells: Vector[Node])(using LineArgs, InlineArgs) extends FormattedLine
  final case class Title(parts: Vector[UnparagraphedLine], level: Int)(using LineArgs) extends FormattedLine

  final case class PlaintextLine(line: String)(using l: LineArgs, i: InlineArgs) extends Node with HasInlineArgs // although it's a "line", we extend Node, because this should only be used inside `UnformattedTextBlock`s
  final case class UnparagraphedLine(line: Vector[InlineNode])(using l: LineArgs, i: InlineArgs) extends FormattedLine with HasInlineArgs
  final case class ParagraphedLine(line: Vector[InlineNode])(using l: LineArgs, i: InlineArgs) extends FormattedLine with HasInlineArgs
  //final case class LineAfterLineFormattersWithoutNewline(content: Vector[InlineNode])(using a: LineArgs, i: InlineArgs) extends FormattedLine // `LineAfterLineFormattersWithoutNewline` is to be used inside tables. Or will it really? in tables, there are newlines too, just kinda different. It would suck to have to make a parallel AST just for tables.

  sealed abstract class InlineNode(using l: LineArgs, i: InlineArgs) extends Node with HasInlineArgs
  sealed abstract class InlineFormatter(allowsFormatting: Boolean)(using a: LineArgs, i: InlineArgs) extends InlineNode
  object Bold extends InlineFormatterObj {
    val delimiter = "*"
    val contentIsPlaintext: Boolean = false
    type ConstructorArgs = Vector[InlineNode]
  }
  object Italic extends InlineFormatterObj {
    val delimiter = "/"
    val contentIsPlaintext: Boolean = false
    type ConstructorArgs = Vector[InlineNode]
  }
  object Code extends InlineFormatterObj {
    val delimiter = "`"
    val contentIsPlaintext: Boolean = true
    type ConstructorArgs = String
  }
  object Underline extends InlineFormatterObj {
    val delimiter = "_"
    val contentIsPlaintext: Boolean = false
    type ConstructorArgs = Vector[InlineNode]
  }
  final case class Bold(contents: Vector[InlineNode])(using a: LineArgs, i: InlineArgs) extends InlineFormatter(true)
  final case class Italic(contents: Vector[InlineNode])(using a: LineArgs, i: InlineArgs) extends InlineFormatter(true)
  final case class Underline(contents: Vector[InlineNode])(using a: LineArgs, i: InlineArgs) extends InlineFormatter(true)
  final case class Code(content: String)(using a: LineArgs, i: InlineArgs) extends InlineFormatter(false)
  final case class Text(text: String)(using a: LineArgs, i: InlineArgs) extends InlineNode // might consider changing to Text(line: Int, start: Int, end: Int)
  sealed abstract class NamedFormatter(allowsFormatting: Boolean)(using a: LineArgs, i: InlineArgs) extends InlineFormatter(allowsFormatting)

  object NamedFormatterError extends NamedFormatterObj {
    val name = ""
    type ConstructorArgs = NamedFormatterErrorArg
  }
  case class NamedFormatterErrorArg(errorMsg: String, nonWorkingContent: String, incorrectFormatter: String)

  final case class NamedFormatterError(arg: NamedFormatterErrorArg)(using a: LineArgs, i: InlineArgs) extends NamedFormatter(false)

  final case class Audio(name: String)(using a: LineArgs, i: InlineArgs) extends NamedFormatter(false)
  object Audio extends NamedFormatterObj {
    val name = "audio"
    type ConstructorArgs = String
  }

  object NamedUrl extends NamedFormatterObj {
    val name = "url"
    def unapply(n: NamedUrl) = Some((n.url, n.linkText))
    type ConstructorArgs = String
  }

  final case class NamedUrl(arg: String)(using a: LineArgs, i: InlineArgs) extends NamedFormatter(false) {
    val splitArg: Seq[String] = arg.split(" ").toVector
    val url = splitArg.head
    val linkText =
      if (splitArg.length > 1) {
        splitArg.tail.mkString(" ")
      } else {
        url
      }
  }

  object InlineMath extends NamedFormatterObj {
    val name = "math"
    type ConstructorArgs = String
  }

  final case class InlineMath(contents: String)(using a: LineArgs, i: InlineArgs) extends NamedFormatter(false)

  object ImportantInline extends NamedFormatterObj {
    val name = "important"
    type ConstructorArgs = Vector[InlineNode]
    override val contentIsPlaintext: Boolean = false
  }
  final case class ImportantInline(contents: Vector[InlineNode])(using a: LineArgs, i: InlineArgs) extends NamedFormatter(true)

  object InlineVectors extends NamedFormatterObj {
    val name = "vectors"
    type ConstructorArgs = String
  }

  final case class InlineVectors(contents: String)(using a: LineArgs, i: InlineArgs) extends NamedFormatter(false)

  object GridCanvas extends NamedFormatterObj {
    val name = "grid"
    type ConstructorArgs = String
    def unapply(g: GridCanvas) = Some(g.nameValData)
  }

  final case class GridCanvas(contents: String)(using a: LineArgs, i: InlineArgs) extends NamedFormatter(false) {
    private val splitArgs = contents.split(" ")
    val name = splitArgs(0)
    val x = splitArgs(1)
    val y = splitArgs(2)
    val cellSize = splitArgs(3)
    val nameValData = Seq(
      (Config.Grid.xData, x),
      (Config.Grid.yData, y),
      (Config.Grid.cellSizeData, cellSize)
    )

    val asHtml = DataStructures.makeNilsobjDocumentDiv(name, DynamicComponentHtmlClasses.grid, additionalData = nameValData)
    // s"""<div id="${Config.Grid.makeId(nameId, x.toInt, y.toInt, cellSize.toInt)}" class="${Config.Grid.gridClass}" ${AstToHtml.dataString(nameValData)}></div>"""
  }

  object InlineImage extends NamedFormatterObj {
    val name = "image"
    type ConstructorArgs = String
  }
  final case class InlineImage(val arg: String)(using a: LineArgs, i: InlineArgs) extends NamedFormatter(false) {
    private val spaceSplittedArgs: Vector[String] = arg.split(" ").toVector

    val imageUrl = spaceSplittedArgs.lift(0).getOrElse("")
    val referenceTarget = spaceSplittedArgs.lift(1)
    val scalingStyle: String = spaceSplittedArgs.tail
      .find(arg => arg.init.forall(_.isDigit) && arg.lastOption.contains('%'))
      .map(width => s"width: $width;")
      .getOrElse("")
    val positionClasses: Seq[String] = spaceSplittedArgs
      .lift(2)
      .map(_.toLowerCase)
      .collect {
        case "left"        => Seq(Config.ImageAlignment.aligned, Config.ImageAlignment.leftClasslist.mkString(" "))
        case "right"       => Seq(Config.ImageAlignment.aligned, Config.ImageAlignment.rightClasslist.mkString(" "))
        case "leftcenter"  => Seq(Config.ImageAlignment.aligned, Config.ImageAlignment.leftCenter)
        case "rightcenter" => Seq(Config.ImageAlignment.aligned, Config.ImageAlignment.rightCenter)
        case "center"      => Seq(Config.ImageAlignment.centered)
        case "floatleft"   => Seq("float-start")
        case "floatright"  => Seq("float-end")
      }
      .getOrElse(Seq())

    val referenceTargetData = referenceTarget match
      case None        => ""
      case Some(value) => s"""id="${Config.Ref.makeRefTargetId(value)}""""

    def asHtml = {
      val cssClasses = positionClasses.mkString(" ")
      img(
        src := imageUrl,
        `class` := s"$cssClasses ${Config.PinContent.pinnableCls}",
        style := scalingStyle,
        if (referenceTarget.nonEmpty) {
          id := Config.Ref.makeRefTargetId(referenceTarget.get)
        }
      )
    }
  }

  object SummaryPrompt extends NamedFormatterObj {
    val name = "summary"
    type ConstructorArgs = String
  }
  final case class SummaryPrompt(s: String)(using a: LineArgs, i: InlineArgs) extends NamedFormatter(false)

  object MolviewPreview extends NamedFormatterObj {
    val name = "molview"
    type ConstructorArgs = String
  }

  final case class MolviewPreview(url: String)(using a: LineArgs, i: InlineArgs) extends NamedFormatter(false)
  object RightAlign extends NamedFormatterObj {
    val name = "right"
    type ConstructorArgs = Vector[InlineNode]
    override val contentIsPlaintext: Boolean = false
  }
  final case class RightAlign(contents: Vector[InlineNode])(using a: LineArgs, i: InlineArgs) extends NamedFormatter(true)

  object LeftAlign extends NamedFormatterObj {
    val name = "left"
    type ConstructorArgs = Vector[InlineNode]
    override val contentIsPlaintext: Boolean = false
  }
  final case class LeftAlign(contents: Vector[InlineNode])(using a: LineArgs, i: InlineArgs) extends NamedFormatter(true)
  object CenterAlign extends NamedFormatterObj {
    val name = "center"
    type ConstructorArgs = Vector[InlineNode]
    override val contentIsPlaintext: Boolean = false
  }
  final case class CenterAlign(contents: Vector[InlineNode])(using a: LineArgs, i: InlineArgs) extends NamedFormatter(true)

  object Ref extends NamedFormatterObj {
    val name = "ref"
    type ConstructorArgs = String
  }

  final case class Ref(reference: String)(using a: LineArgs, i: InlineArgs) extends NamedFormatter(false)

  object TreeGraph extends NamedFormatterObj {
    val name = "tree"
    type ConstructorArgs = String
  }

  final case class TreeGraph(contents: String)(using a: LineArgs, i: InlineArgs) extends NamedFormatter(false) {
    val splitArgs = contents.split(" ")
    val name = splitArgs(0)
    val widthString = splitArgs.lift(1).map(s => s.filter(_.isDigit))

    def asHtml = {
      // TODO: respect width
      val width: Int =
        if (widthString.nonEmpty) {
          val w = widthString.get.toInt
          if (w <= 0) {
            1
          } else if (w > 100) {
            100
          } else {
            w
          }
        } else {
          100
        }
      DataStructures.makeNilsobjDocumentDiv(name, DynamicComponentHtmlClasses.tree, additionalData = Seq((Config.TreeGraph.widthData, width.toString)))
    }
  }
}

trait AstVisitor[A] {
  type ImplicitArgs
  def translate(n: AST.Node)(using ImplicitArgs): A
}

object AstToAnnotatedEditor extends AstVisitor[String] {
  type ImplicitArgs = Unit

  import inlineparser.InlineParser.InlineFormatterSymbols
  import inlineparser.InlineParser.InlineFormatterSymbols.terminator
  import inlineparser.InlineParser.InlineFormatterSymbols.named
  import blockparse.NamedBlock
  import blockparse.Block
  import globals.Config.FormatterNames

  def translateWithNewline(v: Vector[AST.Node])(using ImplicitArgs): String = v.map(translate(_) + "\n").mkString
  def translateNodes(v: Vector[AST.Node])(using ImplicitArgs): String = v.map(translate(_)).mkString

  // TODO: backslashes? Oof.

  def namedBlock(content: String, name: String, level: Int) = s"${NamedBlock.delimiter.repeat(level)}$name\n" + content + Block.terminationSymbol.repeat(level) + "\n"

  def stringifyContinuationContent(leadingCharacter: String, formatterCount: Int, item: ListItem)(using ImplicitArgs) =
    leadingCharacter.repeat(formatterCount) + s" ${translate(item.parts.head)}" + item.parts.tail.map(l => s"${" ".repeat(item.parts.length - 1)}- ${translate(l)}").mkString

  def stringifyNamedContent(content: String, name: String, formatterLevel: Int) = named.repeat(formatterLevel) + s"$name $content" + terminator.repeat(formatterLevel)
  def translate(n: AST.Node)(using ImplicitArgs): String = n match
    case Document(blocks)                 => translateWithNewline(blocks)
    case CodeBlock(subblock, level)       => namedBlock(translate(subblock), "code", level)
    case LatexBlock(subblock, level)      => namedBlock(translate(subblock), "math", level)
    case ImportantBlock(subblocks, level) => namedBlock(translateNodes(subblocks), "code", level)
    case HeaderBlock(subblocks, level)    => namedBlock(translateNodes(subblocks), "header", level)
    case FormattedTextBlock(lines)        => translateNodes(lines)
    case UnformattedTextBlock(lines)      => translateNodes(lines)
    case PlaintextLine(line) => line + "\n"
    case l: ListItem => ???
    case UnparagraphedLine(line) => translateNodes(line) + "\n"
    // case Title(lines, level) => stringifyContinuationContent("#", level+1, lines)
    // case OrderedList(items, nestingLevel) => items.map(lines => stringifyContinuationContent("+", 1, lines) + "\n").mkString
    // case UnorderedList(items, nestingLevel) => items.map(lines => stringifyContinuationContent("*", 1, lines) + "\n").mkString
    case Title(lines, level)                => ???
    case OrderedList(items, nestingLevel)   => ???
    case UnorderedList(items, nestingLevel) => ???
    case t: Table                           => ???
    case t: TableRow                        => ???
    case ParagraphedLine(content)   => translateNodes(content) + "\n"
    case Bold(contents)                     => "<b>" + InlineFormatterSymbols.bold + translateNodes(contents) + terminator + "</b>"
    case Italic(contents)                   => InlineFormatterSymbols.italic + translateNodes(contents) + terminator
    case Underline(contents)                => InlineFormatterSymbols.underline + translateNodes(contents) + terminator
    case e: NamedFormatterError             => stringifyNamedContent(e.arg.nonWorkingContent, e.arg.incorrectFormatter, e.i.formatterLevel)
    case n: NamedUrl                        => stringifyNamedContent(n.arg, FormatterNames.namedUrl, n.i.formatterLevel)
    case r: Ref                             => namedBlock(r.reference, FormatterNames.ref, r.i.formatterLevel)
    case t: TreeGraph                       => namedBlock(t.contents, FormatterNames.treeGraph, t.i.formatterLevel)
    case a: Audio                           => stringifyNamedContent(a.name, Audio.name, a.i.formatterLevel)
    case im: InlineMath                     => stringifyNamedContent(im.contents, FormatterNames.inlineMath, im.i.formatterLevel)
    case iv: InlineVectors                  => stringifyNamedContent(iv.contents, FormatterNames.inlineVectors, iv.i.formatterLevel)
    case g: GridCanvas                      => stringifyNamedContent(g.contents, FormatterNames.gridCanvas, g.i.formatterLevel)
    case i: InlineImage                     => stringifyNamedContent(i.arg, FormatterNames.inlineImage, i.i.formatterLevel)
    case s: SummaryPrompt                   => stringifyNamedContent("", FormatterNames.summaryPrompt, s.i.formatterLevel)
    case m: MolviewPreview                  => stringifyNamedContent(m.url, FormatterNames.molviewPreview, m.i.formatterLevel)
    case r: RightAlign                      => stringifyNamedContent(r.contents.mkString("\n") + "\n", FormatterNames.rightAlign, r.i.formatterLevel)
    case l: LeftAlign                       => stringifyNamedContent(l.contents.mkString("\n") + "\n", FormatterNames.leftAlign, l.i.formatterLevel)
    case c: CenterAlign                     => stringifyNamedContent(c.contents.mkString("\n") + "\n", FormatterNames.centerAlign, c.i.formatterLevel)
    case c: Code                            => InlineFormatterSymbols.code + c.content + terminator
    case Text(text)                         => text

}

object AstToHtml extends AstVisitor[scalatags.Text.TypedTag[?]] {
  import AST._
  import scalatags.Text.all._

  type ImplicitArgs = Unit

  extension (s: String) {
    def wrapInTag(tag: String, withNewlines: Boolean = true): String = {
      if withNewlines then s"<$tag>\n$s\n</$tag>\n"
      else s"<$tag>$s</$tag>"
    }

    def wrapDiv(classes: Seq[String], data: Option[Seq[(String, String)]] = None) =
      data match {
        case Some(d) => s"""<div class="${classes.mkString(" ")}" ${dataString(d)}>$s</div>"""
        case None    => s"""<div class="${classes.mkString(" ")}">$s</div>"""
      }
    def wrapDiv(cls: String) = s"""<div class="$cls">$s</div>"""
  }

  private def translateWithBr(v: Vector[ParagraphedLine])(using implicitArgs: ImplicitArgs): String = v.map(translate(_)).mkString("<br>\n")

  def bodyPreamble: String = {
    ""
  }

  extension (t: scalatags.Text.TypedTag[?]) {
    def withLineData(n: Node) = {}

    def withInlineData(n: InlineNode) = {}

    def wrapDiv(classes: String) =
      div(
        `class` := classes,
        t
      )
  }
  extension (t: Vector[scalatags.Text.TypedTag[?]]) {
    def wrapDiv(classes: String) =
      div(
        `class` := classes,
        t
      )
  }


  // TODO: There should be no newline after elements. Requires distinction between inline text nodes following a line formatter and non-formatted text.
  def translate(ast: AST.Node)(using args: ImplicitArgs): scalatags.Text.TypedTag[?] = {
    var translatedNode = ast.match {
      case Document(blocks) =>
        div(
          bodyPreamble,
          blocks.map(translate)
        )
      case Title(lines, level) =>
        /* TODO: This is an ugly special case: I need to descend two recursion levels because titles wrap their contents using <br> and \n,
         * but I normal line is always wrapped in <p>, <br>, \n.
         * Do I add an extra AST node just for this? But that's annoying and I don't see how the LineParser would work then.
         * Alternative is always letting the parent context wrap the line, but that's quite a bit more code duplication. OTOH lists also
         * work fundamentally different from a text block, which works different from a code block, which works different from a ... -> maybe this is a good idea.
         */
        tag("h" + (level + 1).toString)(
          lines.map(translate)
        )
      case OrderedList(items, nestingLevel) =>
        ol(
          items.map(translate)
        )
      case UnorderedList(items, nestingLevel) =>
        ul(
          items.map(translate)
        )
      case ListItem(parts: Vector[FormattedLine]) =>
        li(
          parts.map(translate)
        )
      case t: Table =>
        ???
      case l@ParagraphedLine(contents) => // TODO: line formatter isn't the only type that needs an id -- otherwise we have holes!
        p(
            id := Config.Compiler.makeIdFromLineNumber(l.l.fromLine),
          contents.map(translate(_)),
          br()
        )
      case l@UnparagraphedLine(line) =>
        span(
          id := Config.Compiler.makeIdFromLineNumber(l.l.fromLine),
          line.map(translate),
          br()
        )
      case l@PlaintextLine(line) => span(
        id := Config.Compiler.makeIdFromLineNumber(l.l.fromLine),
        line + "\n"
      )
      case b @ Bold(contents) =>
        strong(
          contents.map(translate)
        )
      // translateNodes(contents).wrapInTag("strong", false)
      case Italic(contents) =>
        em(
          contents.map(translate)
        )
      case Underline(contents) =>
        // TODO: Underlining text is bad practice and the <u> element is not supposed to be used this way. Throw it out of the AST?
        u(
          contents.map(translate)
        )
      case Code(content) =>
        code(
          content
        )
      case Text(s) =>
        span(
          s
        )
      case codeBlock@CodeBlock(contents, _) =>
        pre(
          code(
            translate(contents)
          )
        )
      case latexBlock@LatexBlock(contents, _) =>
        span(
          `class` := s"math display ${Config.PinContent.pinnableCls}",
          translate(contents)
        )
      case formattedBlock@FormattedTextBlock(lines) =>
        div(
          lines.map(translate)
        )
      case unformattedBlock@UnformattedTextBlock(lines) =>
        div(
          lines.map(translate) // do I need another kind of *-TextBlock for <br>? Don't think so though.
        )
      case ImportantBlock(subblocks, _) =>
        div(
          `class` := "alert alert-info",
          subblocks.map(translate)
        )
      case HeaderBlock(subblocks, level) =>
        div(
          `class` := Config.Header.header,
          subblocks.map(translate)
        )
      case NamedUrl(url, linkText) =>
        a(
          href := url,
          linkText
        )
      case i: InlineImage => i.asHtml
      case i: InlineMath =>
        span(
          `class` := s"math inline ${Config.PinContent.pinnableCls}",
          i.contents
        )
      case ImportantInline(contents) => span(`class` := "alert alert-info p-0", contents.map(translate))
      case i: InlineVectors => processVectors(i.contents)
      case m: MolviewPreview =>
        iframe(
          `class` := Config.Molview.molviewCls,
          attr("frameborder") := "0",
          src := m.url
        )
      case n: NamedFormatterError => errorSpan(n.arg.errorMsg)
      case r: RightAlign =>
        r.contents.map(translate).wrapDiv(Config.AlignText.rightAlign)
      case c: CenterAlign =>
        c.contents.map(translate).wrapDiv(Config.AlignText.rightAlign)
      case l: LeftAlign =>
        l.contents.map(translate).wrapDiv(Config.AlignText.rightAlign)
      case s: SummaryPrompt =>
        div(
          `class` := Seq(Config.SummaryPrompt.summaryField, Config.Printing.dontPrint).mkString(" "),
          contenteditable := true
        )
      case g: GridCanvas => g.asHtml
      case r: Ref =>
        a(
          href := s"#${Config.Ref.makeRefTargetId(r.reference)}", // TODO: does this allow XSS?
          data(Config.Ref.targetDataName) := r.reference,
          `class` := Config.Ref.refClassName,
          r.reference
        )
      case t: TreeGraph                             => t.asHtml
      case a: Audio => DataStructures.makeNilsobjDocumentSpan(a.name, Config.DynamicComponentHtmlClasses.audio, Seq(Config.Printing.dontPrint), Seq())
      case t: TableRow                              => ???
    }

    translatedNode = ast match {
      case b: Block =>
        div(
          div(id := Config.Compiler.makeIdFromLineNumber(b.l.fromLine)),
          data(Config.Editor.fromLineData) := b.l.fromLine,
          data(Config.Editor.untilLineData) := b.l.untilLine,
          translatedNode,
          div(id := Config.Compiler.makeIdFromLineNumber(b.l.untilLine-1))
        )
      case _ => translatedNode
    }

    ast match {
      case n: InlineNode =>
        translatedNode(
          data(Config.Editor.fromLineData) := n.l.fromLine,
          data(Config.Editor.untilLineData) := n.l.untilLine,
          data(Config.Editor.fromColData) := n.i.fromCol,
          data(Config.Editor.untilColData) := n.i.untilCol
        )
      case n: Node =>
        translatedNode(
          data(Config.Editor.fromLineData) := n.l.fromLine,
          data(Config.Editor.untilLineData) := n.l.untilLine
        )
    }
  }

  def dataString(nameValuePairs: Seq[(String, String)]): String = nameValuePairs.map(makeAttribute.tupled).mkString(" ")
  def makeAttribute(name: String, value: Option[String]): String = value.map(v => makeAttribute(name, v)).getOrElse("")
  def makeAttribute(name: String, value: String): String = s"""data-$name="$value""""
  def wrapInMolview(url: String) = s"""<iframe class="molview" frameborder="0" src="$url"></iframe>"""
  def wrapInBootstrapInfo(s: String) = s"""<div class="alert alert-info">$s</div>"""

  def errorSpan(s: String) = span(
    `class` := Config.FormatterNames.namedErrorCls,
    s
  )
  def processVectors(s: String) = {
    val vectorStrings: Vector[Vector[String]] = s.split(" ").map(_.split(";").toVector).toVector
    val dimension = vectorStrings.head.length
    if (vectorStrings.forall(_.length == dimension)) {
      val joined = vectorStrings.map(_.mkString("-")).mkString(";")
      div(
        `class` := Config.vectorboxClass,
        s
      )
    } else {
      errorSpan(s"Not all vectors have dimension $dimension.")
    }
  }
}
