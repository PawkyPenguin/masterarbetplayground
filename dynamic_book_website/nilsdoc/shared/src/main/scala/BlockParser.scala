package blockparse

import scala.collection.immutable.HashMap
import scala.collection.mutable.ArrayBuffer
import scala.util.matching.Regex
import javax.swing.text.AbstractDocument.Content
import ast.AST.Code
import java.util.jar.Attributes.Name
import util.ParseUtils
import inlineparser.InlineParser.InlineFormatterSymbols.code
import ast.AST.TextSegment
import lineparse.LineParser.parse

val STANDARD_DELIMITER_COUNT = 3

case class BlockBuilder(delimitersRequired: Int, contentIsPlaintext: Boolean, buildBlock: (args: Any, textSegment: BlockFormatterArgs) => Block)
object BlockParser {

    case class Context(blockToBuild: BlockBuilder, blockStart: Int)
    case class BlockParserResult(parsedBlocks: Vector[Block], newOffset: Int)
    private def parseBlock(lines: Vector[String], lineToParseNext: Int, context: Option[Context]): BlockParserResult = {
        val parsedBlocks = ArrayBuffer[Block]()
        var i  = lineToParseNext
        var currentTextBlockStart = lineToParseNext
        while (i < lines.length) {
            if (context.nonEmpty && ParseUtils.matchExactly(lines(i), 0, Block.terminationSymbol) == context.get.blockToBuild.delimitersRequired) {
                if (context.get.blockToBuild.contentIsPlaintext) {
                    val unformattedBlock = UnformattedTextBlock(currentTextBlockStart, i)
                    val parentBlockSegment = TextSegment(currentTextBlockStart-1, i+1)
                    return BlockParserResult(Vector(context.get.blockToBuild.buildBlock(unformattedBlock, parentBlockSegment)) , i)
                } else {
                    parsedBlocks += FormattedTextBlock(currentTextBlockStart, i)
                    val blockSegment = TextSegment(parsedBlocks.head.b.fromLine, parsedBlocks.last.b.untilLine)
                    return BlockParserResult(Vector(context.get.blockToBuild.buildBlock(parsedBlocks.toVector, blockSegment)) , i)
                }
            } else if (!context.exists(_.blockToBuild.contentIsPlaintext)) {
                val delimiterDepth = context.map(_.blockToBuild.delimitersRequired).getOrElse(STANDARD_DELIMITER_COUNT)
                val maybeBlockBuilder: Option[BlockBuilder] = tryMatchBlock(i, lines(i), delimiterDepth)
                if (maybeBlockBuilder.nonEmpty) {
                    val parsedSubBlocks: BlockParserResult = parseBlock(lines, i+1, Some(Context(maybeBlockBuilder.get, i)))
                    parsedBlocks += FormattedTextBlock(currentTextBlockStart, i)
                    parsedBlocks.addAll(parsedSubBlocks.parsedBlocks)
                    i = parsedSubBlocks.newOffset
                    currentTextBlockStart = i + 1
                }
            }
            i += 1
        }
        // we reached the end without encountering a termination symbol.
        parsedBlocks += FormattedTextBlock(currentTextBlockStart, i)

        val allBlocks: Vector[Block] =
            if (context.nonEmpty) {
                // this is always the case except when we're at the first level of recursion

                // Since we didn't encounter the termination symbol, add a FormattedTextBlock containing the starting delimiter of the current block
                (FormattedTextBlock(context.get.blockStart, context.get.blockStart + 1) +: parsedBlocks.view).toVector
            } else {
                parsedBlocks.toVector
            }
        
        BlockParserResult(allBlocks, i)
    }


    private def tryMatchBlock(i: Int, line: String, requiredDelimiters: Int): Option[BlockBuilder] = {
        val codeblockMatches = ParseUtils.matchExactly(line, 0, CodeBlock.delimiter)
        Option.when(codeblockMatches >= requiredDelimiters)(
            BlockBuilder(
                codeblockMatches,
                true,
                (content: Any, additionalArgs: BlockFormatterArgs) => CodeBlock(content.asInstanceOf[UnformattedTextBlock], codeblockMatches)(using additionalArgs)
            )
        ).orElse{
            val latexMatches = ParseUtils.matchExactly(line, 0, LatexBlock.delimiter)
            Option.when(latexMatches >= requiredDelimiters)(
                BlockBuilder(
                    latexMatches,
                    true,
                    (content: Any, additionalArgs: BlockFormatterArgs) => LatexBlock(content.asInstanceOf[UnformattedTextBlock], latexMatches)(using additionalArgs)
                )
            )
        }.orElse{
            NamedBlock.tryMatchBlock(i, line, requiredDelimiters)
        }
    }
  
    def parseBlocks(lines: Vector[String]): Root = {
        val parsedBlocks = parseBlock(lines, 0, None).parsedBlocks
        val textSegment = if (parsedBlocks.isEmpty) {
            TextSegment(0, 0)
        } else {
            TextSegment(0, parsedBlocks.last.b.untilLine)
        }
        Root(parsedBlocks)(using textSegment)
    }
}


/* TODO: I have multiple ways to represent "types" in my AST/parser-related definitions. It's getting complicated, so decide on a uniform representation.
 * Options:
 * - top level case class + enum that designates type (limitation: can't add specific methods; advantage: no isInstanceOf check needed to compare types)
 * - enum with subcases (limitation: can't add specific methods. If I add arguments, the runtime check needs reflection(?))
 * - sealed trait/final case class hierarchy (can add specific overriden methods; is very verbose)
 * 
 * Also consider ease of refactoring: If I go with first option, I can easily delete the enum and extend with case classes instead. (just confirmed this by switching 1->3)
 * But given the second option (enum only), it's (phsyically) very annoying to go to case class representation.
 * 
 * Another issue I have regarding uniformity is that the "line" string is always implicit. Should I keep it that way? It's sorta scary to have a bunch of parser
 * results floating around in space unattached to any lines: Vector[String]. Easy to misinterpret. I guess that's procedural programming I'm doing atm (as opposed to OOP or FP).
 * However, copying strings is bad style. Consider just having the top of the AST contain `lines: Vector[String]`.
 * 
 */


type BlockFormatterArgs = TextSegment
sealed trait Block(val contentIsPlaintext: Boolean)(using val b: BlockFormatterArgs)
object Block { val terminationSymbol = "}" }

final case class Root(subblocks: Vector[Block])(using b: BlockFormatterArgs) extends Block(false)

final case class CodeBlock(content: UnformattedTextBlock, level: Int)(using b: BlockFormatterArgs) extends Block(true)
object CodeBlock { val delimiter = "`"  //TODO: have delimiter be `{{{code`, i.e. make this named block. ?
}

final case class LatexBlock(content: UnformattedTextBlock, level: Int)(using b: BlockFormatterArgs) extends Block(true)
object LatexBlock { val delimiter = "$" }

sealed abstract class NamedBlock(name: String, contentIsPlaintext: Boolean)(using b: BlockFormatterArgs) extends Block(contentIsPlaintext) // might or might not have subblocks. Unsure before we know the concrete instance. TODO: Make all non-plain blocks into named blocks for consistency?
object NamedBlock {
    val delimiter = "{"
    def namedDelimiter(name: String) = delimiter + name

    def tryMatchBlock(i: Int, line: String, requiredDelimiters: Int): Option[BlockBuilder] = {
        val delimMatches = ParseUtils.countRepeatedMatch(line, 0, delimiter)
        val name = line.drop(delimMatches * delimiter.length).takeWhile(c => c.isLetter)
        val enoughMatches = delimMatches >= requiredDelimiters
        val isAtEndOfLine = line.length == (delimMatches * delimiter.length) + name.length
        if (enoughMatches && isAtEndOfLine) {
            name match {
                case "important" =>
                    Some(
                        BlockBuilder(
                            delimMatches,
                            false,
                            (args: Any, otherArgs: BlockFormatterArgs) => ImportantBlock(args.asInstanceOf[Vector[Block]], delimMatches)(using otherArgs)
                        )
                    )
                case "header" =>
                    Some(
                        BlockBuilder(
                            delimMatches,
                            false,
                            (args: Any, otherArgs: BlockFormatterArgs) => HeaderBlock(args.asInstanceOf[Vector[Block]], delimMatches)(using otherArgs)
                        )
                    )
                case _ => None
            }
        } else {
            None
        }
    }
}
final case class ImportantBlock(subblocks: Vector[Block], level: Int)(using b: BlockFormatterArgs) extends NamedBlock("important", false)
final case class HeaderBlock(subblocks: Vector[Block], level: Int)(using b: BlockFormatterArgs) extends NamedBlock("header", false)

final case class FormattedTextBlock(startLine: Int, endLine: Int) extends Block(false)(using TextSegment(startLine, endLine))
final case class UnformattedTextBlock(startLine: Int, endLine: Int) extends Block(true)(using TextSegment(startLine, endLine))