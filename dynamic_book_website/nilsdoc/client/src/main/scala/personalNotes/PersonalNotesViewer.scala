package personalNotes

import scala.scalajs.js.annotation.JSExportTopLevel
import scala.scalajs.js
import org.scalajs.dom
import scala.scalajs
import globals.Config
import org.scalajs.dom.html
import org.scalajs.dom.HTMLElement
import upickle.default._
import util.Util
import api.ClientApi
import api.Nilsobjects._
import api.Routes
import scalatags.JsDom.all._
import org.scalajs.dom
import org.scalajs.dom.URL
import util.Util.getCurrentUsername
import api.DataStructures.Document

object PersonalNotesViewer {
  @JSExportTopLevel(name = "start", moduleID = "personalNotes")
  def main(): Unit = {
    val htmlContainer = dom.document.getElementById(Config.PersonalNotes.htmlContainerId).asInstanceOf[html.Div]
    val noteContainer = dom.document.getElementById(Config.PersonalNotes.noteContainerId).asInstanceOf[html.Div]
    val storageContainer = dom.document.getElementById(Config.DynamicComponentHtmlClasses.storageAreaId).asInstanceOf[html.Div]
    val notes = ClientApi.loadNilsobjects[Note](Config.DynamicComponentHtmlClasses.note)
    val document = upickle.default.read[Document](Util.byId[html.Script](Config.titleStorageAreaId).textContent)
    val user = Util.getCurrentUsername
    notes.foreach(note =>
        val newDiv = div(
          `class` := "m-5"
        ).render
        //newDiv.innerHTML = note.html
        // TODO
        htmlContainer.appendChild(newDiv)
        val editableNoteContents = textarea(
          note.note
        ).render
        val newDiv2 = div(
          `class` := "m-5",
          editableNoteContents
        ).render
        noteContainer.appendChild(newDiv2)
        editableNoteContents.oninput = _ => {
          val newNote = note.copy(note = editableNoteContents.value)
          ClientApi.storeNilsobjectPayload(user, document.authorId, Some(document.title), newNote)
        }
    )
  }
}
