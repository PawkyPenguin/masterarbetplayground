package nilsdocWorkstation

import scala.scalajs.js.annotation.JSExportTopLevel
import org.scalajs.dom
import org.scalajs.dom
import org.scalajs.dom.html
import globals.Config
import util.Util.DocumentContext
import util.Util
import util.Util.byId
import scala.scalajs.js.annotation._
import scalajs.js
import scalatags.JsDom.all._
import util.Util.showHtmlEl
import util.Util.hideHtmlEl
import api.DataStructures.Document

object NilsdocStudentView {
  
    @JSExportTopLevel(name = "main", moduleID = "studentView")
    def main(): Unit = {
        val container = dom.document.getElementById(Config.nilsdocRenderArea).asInstanceOf[html.Div]
        val nilsdocView = dom.document.getElementById(Config.BookView.previewId).asInstanceOf[html.Div]
        val initialDocument = upickle.default.read[Document](byId[html.Script](Config.titleStorageAreaId).textContent)
        val title: Option[String] = Some(initialDocument.title)
        val documentContents = initialDocument.content.split("\n").toVector
        val documentAuthor = initialDocument.authorId
        val viewingContext = DocumentContext(documentAuthor, Util.getCurrentUsername, title, documentContents, nilsdocView, container)
        NilsdocRenderer.parseAndRenderBook()(using viewingContext)
    }

}
