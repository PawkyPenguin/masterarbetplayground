package nilsdocWorkstation

import scala.scalajs.js.annotation._
import lineparse.LineParser
import ast._
import org.scalajs.dom
import org.scalajs.dom.html
import org.scalajs.dom.HTMLDivElement
import org.scalajs.dom.HTMLTextAreaElement
import scala.scalajs.js.Date
import scala.scalajs.js.Dynamic
import org.scalajs.dom.HTMLButtonElement
import org.scalajs.dom.Event
import scala.CanEqual.derived
import globals.Config
import globals.Config.nilsdocRenderArea
import org.scalajs.dom.Element
import globals.Config.ViewButtons.documentTitle
import util.Util
import api.Routes
import util.Util.DocumentContext


enum Visibility derives CanEqual {
    case Both, Editor, Preview
}

class EditorVisibility(editorContainer: html.Div)(using viewContext: DocumentContext) {
    private var visibility: Visibility = Visibility.Both

    def setVisibility(v: Visibility) = {
        visibility = v
        update()
    }

    private def update() = {
        visibility match {
            case Visibility.Both => 
                viewContext.previewContainer.classList.remove("invisible")
                dom.document.body.classList.remove(Config.PersonalNotes.inReaderView)
                editorContainer.classList.remove("invisible")
            case Visibility.Preview => 
                viewContext.previewContainer.classList.remove("invisible")
                dom.document.body.classList.add(Config.PersonalNotes.inReaderView)
                editorContainer.classList.add("invisible")
            case Visibility.Editor => 
                viewContext.previewContainer.classList.add("invisible")
                editorContainer.classList.remove("invisible")
        }
        NilsdocRenderer.parseAndRenderBook()
    }

    def bump() = {
        visibility = Visibility.fromOrdinal((visibility.ordinal + 1) % Visibility.values.length)
        update()
    }

}