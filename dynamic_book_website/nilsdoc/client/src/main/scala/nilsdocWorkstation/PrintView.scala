package nilsdocWorkstation

import org.scalajs.dom.html
import util.Util.DocumentContext
import scalatags.JsDom.all._
import globals.Config
import scala.scalajs.js.annotation.JSImport
import scala.scalajs.js
import util.Util.showHtmlEl
import util.Util.hideHtmlEl
import org.scalajs.dom
import org.scalajs.dom.html
import util.Util.byId
import scala.scalajs.js.Dynamic.global

private case class CanvasAndRenderedImage(c: html.Canvas, i: html.Image)

class ShowPrintViewButton(previewButton: html.Button, printOutputElement: html.Div)(using viewingContext: DocumentContext) {
  val printedElement = viewingContext.preview
  val printedElementParent = printedElement.parentElement

  def showPrintView(canvasAndImageElements: Vector[CanvasAndRenderedImage], forUserPreview: Boolean) = {
    canvasAndImageElements.foreach { case CanvasAndRenderedImage(canvas, image) =>
      hideHtmlEl(canvas)
      showHtmlEl(image)
    }
    printedElement.remove()
    printOutputElement.appendChild(printedElement)

    val headerElements = dom.document.getElementsByClassName(Config.Header.header).toVector
    if (headerElements.length > 0) {
        val header = headerElements(0) // Todo: Should we handle multiple headers or do we disallow this in Nilsdoc?
        val headerHeight: Double = header.getBoundingClientRect().height
        val cssMarginHeight = s"max(${headerHeight}px, 1cm)"

        if (forUserPreview) {
            // in the preview, we "simulate" the @page margins as a padding on the div.
            printedElement.style.paddingTop = cssMarginHeight
        } else {
            // in print, we set the page margin instead
            //REENABLE printedElement.style.paddingTop = "0"
            val printStylesheet = byId[html.Link](Config.Printing.stylesheetId).sheet.asInstanceOf[js.Dynamic]
            var i = 0
            println("setting print rule")
            while (i < printStylesheet.cssRules.length.asInstanceOf[Int]) {
                val rule = printStylesheet.cssRules.asInstanceOf[js.Array[js.Dynamic]](i)
                println(s"i = $i")
                dom.console.log(rule)
                if (rule.`type` == dom.CSSRule.PAGE_RULE.asInstanceOf[js.Dynamic]) {
                    //REENABLE rule.style.marginTop = cssMarginHeight
                    println("found one:")
                    dom.console.log(rule)
                    println("^^^^")
                }
                i += 1
            }
        }
    }
}

  def removePrintView(canvasAndImageElements: Vector[CanvasAndRenderedImage]) = {
    canvasAndImageElements.foreach { case CanvasAndRenderedImage(canvas, image) =>
      showHtmlEl(canvas)
      image.remove()
    }
    printedElement.remove()
    printedElementParent.prepend(printedElement)
    printedElement.style.paddingTop = "" // don't forget to reset the padding on the print element
  }


  def convertCanvasesToImages(root: html.Element): Vector[CanvasAndRenderedImage] = {

    val canvasAndImageElements = root
      .querySelectorAll("canvas")
      .map(canvas =>
        val imageEl = img(
          `class` := "invisible",
          src := canvas.asInstanceOf[html.Canvas].toDataURL("image/png")
        ).render
        canvas.parentNode.appendChild(imageEl)
        CanvasAndRenderedImage(canvas.asInstanceOf[html.Canvas], imageEl)
      )
    canvasAndImageElements.toVector
  }

  val printStyleSheet = link(
    `type` := "text/css",
    rel := "stylesheet",
    media := "screen",
    href := "/print.css"
  ).render

/*   val pagedjsScript = script(
    src := "https://unpkg.com/pagedjs/dist/paged.polyfill.js"
  ).render */

  var showingPrintView = false
  var canvasAndImageElements: Vector[CanvasAndRenderedImage] = Vector()
  previewButton.onclick = _ => {
    showingPrintView = !showingPrintView
    if (showingPrintView) {
        // save canvas to image elements. We do this upon showingPrintPreview, because we want don't want to also generate images every time we compile nilsdoc.
        canvasAndImageElements = convertCanvasesToImages(viewingContext.preview)
        dom.document.head.append(printStyleSheet)
        //dom.document.body.append(pagedjsScript)
        showPrintView(canvasAndImageElements, true)
    } else {
        // note that we don't modify `canvasAndImageElements` here.
        removePrintView(canvasAndImageElements)
        printStyleSheet.remove()
        //pagedjsScript.remove()
    }
  }

  dom.window.onbeforeprint = _ => {
    canvasAndImageElements.foreach {case CanvasAndRenderedImage(c, i) => i.remove()}
    canvasAndImageElements = convertCanvasesToImages(viewingContext.preview)
    showPrintView(canvasAndImageElements, false)
  }

  dom.window.onafterprint = _ => {
    removePrintView(canvasAndImageElements)
  }
}
