package nilsdocWorkstation
import scala.scalajs.js.annotation._
import lineparse.LineParser
import ast._
import org.scalajs.dom
import org.scalajs.dom.html
import org.scalajs.dom.HTMLDivElement
import org.scalajs.dom.HTMLTextAreaElement
import scala.scalajs.js.Date
import scala.scalajs.js
import scala.scalajs.js.Dynamic
import org.scalajs.dom.HTMLButtonElement
import org.scalajs.dom.Event
import scala.CanEqual.derived
import globals.Config
import scala.scalajs.js.Dynamic.literal
import lineparse.LineParser.parse
import org.scalajs.dom.Element
import org.scalajs.dom.HTMLUListElement
import org.scalajs.dom.HTMLLIElement
import dynamicComponents.PinnableContent
import dynamicComponents.AlignedImage
import dynamicComponents.GridEditor
import dynamicComponents.SummaryPrompt
import scala.scalajs.js.Dynamic.global
import scala.scalajs.js.Dynamic.newInstance
import globals.Database
import org.scalajs.dom.RequestInit
import org.scalajs.dom.HttpMethod
import org.scalajs.dom.Headers
import org.scalajs.dom.KeyboardEvent
import globals.Config.nilsdocRenderArea
import globals.ApplicationConfig.EditorType
import globals.ApplicationConfig
import org.scalajs.dom.URL
import visualEditor.VisualEditor
import api.TitleValidator
import org.scalajs.dom.HTMLElement
import scalajs.js.annotation._
import scala.concurrent.Future
import org.scalajs.dom.Response
import concurrent.ExecutionContext.Implicits.global
import api.Routes
import api.Nilsobjects
import api.DataStructures._
import api.DataStructures
import api.Nilsobjects._
import api.ClientApi._
import api.ClientApi
import util.Util
import util.Util._
import ast.AST.Title

@js.native
@JSImport("@popperjs/core", "createPopper")
class createPopper(target: HTMLElement, tooltip: HTMLElement, options: js.Dynamic) extends js.Any

case class SaveButton(saveIcon: HTMLElement, postRequest: (title: String, document: String) => Future[Response]) {
  var isSaving: Boolean = false

  def show() = {
    saveIcon.classList.remove("hidden")
  }

  def hide() = {
    saveIcon.classList.add("hidden")
  }

  /* Acquire a "lock" on the save button while we're waiting for the POST request
   * Returning an Option may seem a bit strange, but it's just for allowing a nice "fluent API" in Scala via flatMapping over options.
   */
  def acquire(): Option[SaveButton] = {
    if (!isSaving) {
      isSaving = true
      hide()
      Some(this)
    } else {
      None
    }
  }

  // Release the "lock" on the save button
  def release(): Unit = isSaving = false
}

class TitleField(titleInput: html.Input, editorArea: html.TextArea, errorField: html.Div, saveButton: SaveButton, var lastTitleWhenSaved: Option[String]) {
  import scalatags.Text.all._

  var lastSaveRequestFailed = false

  errorField.onclick = _ => errorField.classList.add("invisible")

  titleInput.oninput = _ => {
    updateTitleErrorMessage()
    updateSaveIcon()
  }

  saveButton.saveIcon.onclick = _ => {
    val title = getTitle
    if (TitleValidator.isValid(title)) {
      saveButton
        .acquire()
        .foreach(_ =>
          saveButton
            .postRequest(title, editorArea.value)
            .foreach(response =>
              if (response.ok) {
                // TODO: do I update the URL title here?
                saveButton.release()
                lastTitleWhenSaved = Some(title)
                val splitUrl = URL(dom.window.location.href).pathname.split("/")
                val baseName = splitUrl.take(splitUrl.length - 2).mkString("/")
                val encodedTitle = Routes.urlEncode(title)
                js.Dynamic.global.history.pushState(literal(), title, baseName + s"/documents/$encodedTitle")
                lastSaveRequestFailed = false
              } else {
                lastSaveRequestFailed = true
              }
              updateChangesCouldNotBeSaved()
            )
        )
    } else {
      updateTitleErrorMessage()
      blinkErrorMessage()
    }
  }

  def blinkErrorMessage() = Util.letItBlink(errorField)

  def getTitle = titleInput.value

  def getTitleOrErrorHtml(): Either[String, String] = {
    val makeListElements = (l: Seq[String]) => ul(l.map(li(_))).render
    TitleValidator.validateOrPrintError(titleInput.value, makeListElements)
  }

  def showPopup(html: String) = {
    errorField.innerHTML = html
    createPopper(
      titleInput,
      errorField,
      literal(
        placement = "top"
      )
    )
    showHtmlEl(errorField)
  }

  def updatePleaseEnterTitleMessage() = if (getTitle.isEmpty && lastTitleWhenSaved.isEmpty) {
    showPopup("Enter a title to save the document")
    true
  } else {
    hideHtmlEl(errorField)
    false
  }

  def updateSaveIcon() = if (getTitle.nonEmpty && (lastTitleWhenSaved.isEmpty || lastTitleWhenSaved.exists(_ != getTitle))) {
    saveButton.show()
  } else {
    saveButton.hide()
  }

  def updateChangesCouldNotBeSaved() = if (lastSaveRequestFailed) {
    showPopup("⚠️ Failed to save the document.\n Please check your internet connection.")
  } else {
    hideHtmlEl(errorField)
  }

  def updateChangesNotYetSaved() = if (!getTitle.isEmpty && lastTitleWhenSaved.isEmpty || lastTitleWhenSaved.exists(_ != getTitle)) {
    showPopup("Changes have not been saved as a new document yet")
    true
  } else {
    false
  }

  def updateTitleErrorMessage() = if (getTitle.nonEmpty) {
    getTitleOrErrorHtml() match {
      case Left(_)          => hideHtmlEl(errorField)
      case Right(errorHtml) => showPopup(errorHtml)
    }
  } else {
    hideHtmlEl(errorField)
  }

  def canSave = lastTitleWhenSaved.exists(_ == getTitle)
}

//@JSExportTopLevel("NilsdocWorkstation", moduleID = "nilsdocWorkstation")
object NilsdocWorkstation {
  val TODO_USER_ID = Util.getCurrentUsername
  val TODO_VERSION_NUMBER = 1
  val TODO_IS_AUTHOR = Util.isAuthor

  @JSExportTopLevel(name = "start", moduleID = "nilsdocWorkstation")
  def main(): Unit = {
    val window = js.Dynamic.global.window
    window.bootstrap = js.Dynamic.global.require("/src/bootstrap/dist/js/bootstrap.bundle.min.js");
    dom.window.onload = _ => scalaJSEntry()
  }

  def scalaJSEntry() = {
    val preview: html.Div = dom.document.getElementById(Config.BookView.previewId).asInstanceOf[html.Div]
    val previewContainer: html.Div = dom.document.getElementById(Config.nilsdocRenderArea).asInstanceOf[html.Div]
    val editorContainer: html.Div = dom.document.getElementById("editorContainer").asInstanceOf[html.Div]
    val editorArea: Element = dom.document.getElementById("editor")

    // val textareaEditor: html.TextArea = dom.document.getElementById("editor").asInstanceOf[HTMLTextAreaElement]

    ApplicationConfig.editorType match {
      case EditorType.PROSEMIRROR =>
        //val prosemirror = setupProsemirror(editorArea.asInstanceOf[html.Div])
        //setupProsemirrorOnChangeEvent(prosemirror, preview, previewContainer)
        ???
      case EditorType.NO_EDITOR =>
        val e = editorArea.asInstanceOf[html.TextArea]
        setupMyEditor(editorContainer, e, preview, previewContainer)
      case EditorType.MY_EDITOR => ???
    }

  }

  def saveEditorPUT(documentTitle: String, documentContent: String): Future[Response] = {
    val document = api.DataStructures.Document(None, TODO_USER_ID, TODO_VERSION_NUMBER, documentTitle, documentContent)
    jsonFetch(Routes.nilsdocPutUrl(), Some(document), HttpMethod.PUT)
  }

  def setupMyEditor(editorContainer: html.Div, editorTextarea: html.TextArea, preview: html.Div, previewContainer: html.Div): Unit = {
    val titleInput = dom.document.getElementById(Config.ViewButtons.documentTitle).asInstanceOf[html.Input]
    val initialDocument: Document = upickle.default.read[Document](byId[html.Script](Config.titleStorageAreaId).textContent)
    val initialTitle: Option[String] = Some(initialDocument.title)
    editorTextarea.value = initialDocument.content
    val titleErrors = dom.document.getElementById(Config.ViewButtons.ndTitleErrors).asInstanceOf[html.Div]
    val saveIcon = dom.document.getElementById(Config.ViewButtons.ndSaveIcon).asInstanceOf[HTMLElement]
    val saveButton = SaveButton(saveIcon, saveEditorPUT(_, _))
    val titleField = TitleField(titleInput, editorTextarea, titleErrors, saveButton, initialTitle)
    editorTextarea.oninput = _ => {
      val textEditorLines: Vector[String] = editorTextarea.value.split("\n").toVector
      val cursorLine = editorTextarea.value.slice(0, editorTextarea.selectionStart).split("\n").length-1
      titleField.updatePleaseEnterTitleMessage() || titleField.updateChangesNotYetSaved()
      if (titleField.canSave) {
        val title = titleField.getTitle
        saveEditorPUT(title, editorTextarea.value).foreach(response =>
          if (response.ok) {
            println(s"saved document under title $title")
          } else {
            titleField.lastSaveRequestFailed = true
            titleField.updateChangesCouldNotBeSaved()
          }
          val viewContext = DocumentContext(TODO_USER_ID, TODO_USER_ID, Some(title), textEditorLines, preview, previewContainer)
          NilsdocRenderer.parseAndRenderBook()(using viewContext)
          NilsdocRenderer.scrollToLine(cursorLine)
        )
      } else {
        val viewContext = DocumentContext(TODO_USER_ID, TODO_USER_ID, titleField.lastTitleWhenSaved, textEditorLines, preview, previewContainer)
        NilsdocRenderer.parseAndRenderBook()(using viewContext)
        NilsdocRenderer.scrollToLine(cursorLine)
      }
    }
    val maybeTitle = Option.when(titleField.getTitle.nonEmpty)(titleField.getTitle)
    val viewContext = DocumentContext(TODO_USER_ID, TODO_USER_ID, maybeTitle, editorTextarea.value.split("\n").toVector, preview, previewContainer)
    setupButtonsToChangeEditorVisibility(editorContainer)(using viewContext)
    NilsdocRenderer.parseAndRenderBook()(using viewContext)
    setupShowPrintViewButton()(using viewContext)
    setupPublishButton(titleField)(using viewContext)
  }

  //def setupProsemirror(editorDiv: html.Div): EditorView = {
  //  import typings.prosemirrorState.mod._
  //  import typings.prosemirrorView.mod._
  //  import typings.prosemirrorModel.mod._
  //  import typings.prosemirrorCommands.mod._
  //  import typings.prosemirrorCommands.mod._
  //  import typings.prosemirrorSchemaBasic.mod.schema
  //  import typings.prosemirrorHistory.mod.{undo, redo, history}
  //  import typings.prosemirrorKeymap.mod.keymap

  //  val myNewlineCommand: Command = (
  //      (
  //          state: EditorState,
  //          dispatch: js.UndefOr[js.Function1[Transaction, Unit]],
  //          view: js.UndefOr[EditorView]
  //      ) => {
  //        if (dispatch.isEmpty) {
  //          true
  //        } else {
  //          dispatch.get(state.tr.insertText("\n"))
  //          state.doc.check()
  //          dom.console.log(state.doc)
  //          true
  //        }
  //      }
  //  )

  //  val mySpace: Command = (
  //      (
  //          state: EditorState,
  //          dispatch: js.UndefOr[js.Function1[Transaction, Unit]],
  //          view: js.UndefOr[EditorView]
  //      ) => {
  //        if (dispatch.isEmpty) {
  //          true
  //        } else {
  //          dispatch.get(state.tr.insertText(" "))
  //          state.doc.check()
  //          dom.console.log(state.doc)
  //          true
  //        }
  //      }
  //  )

  //  val myBackspace: Command = (state: EditorState, dispatch: js.UndefOr[js.Function1[Transaction, Unit]], view: js.UndefOr[EditorView]) => {
  //    println("backspacing")
  //    if (dispatch.isEmpty) {
  //      true
  //    } else {
  //      val (from, to) = if (state.selection.empty) {
  //        (state.selection.from - 1, state.selection.from)
  //      } else {
  //        (state.selection.from, state.selection.to)
  //      }
  //      dispatch.get(state.tr.delete(from, to))
  //      dom.console.log(state.doc)
  //      true
  //    }
  //  }

  //  val myDelete: Command = (state: EditorState, dispatch: js.UndefOr[js.Function1[Transaction, Unit]], view: js.UndefOr[EditorView]) => {
  //    println("deleting")
  //    if (dispatch.isEmpty) {
  //      true
  //    } else {
  //      val (from, to) = if (state.selection.empty) {
  //        (state.selection.from, state.selection.from + 1)
  //      } else {
  //        (state.selection.from, state.selection.to)
  //      }
  //      dispatch.get(state.tr.delete(from, to))
  //      dom.console.log(state.doc)
  //      true
  //    }
  //  }

  //  val mySchema = schema.asInstanceOf[Schema[Any, Any]]
  //  val myState = EditorState.create(new EditorStateConfig {
  //    doc = DOMParser.fromSchema(mySchema).parse(editorDiv.asInstanceOf[DOMNode])
  //    plugins = js.Array(
  //      // typings.prosemirrorExampleSetup.mod.exampleSetup(typings.prosemirrorExampleSetup.anon.FloatingMenu(mySchema)),
  //      history(),
  //      keymap(
  //        StringDictionary(
  //          ("Mod-z", undo),
  //          ("Mod-y", redo),
  //          ("Enter", myNewlineCommand),
  //          ("Space", mySpace),
  //          ("Delete", myDelete),
  //          ("Backspace", myBackspace)
  //        )
  //      )
  //    )
  //  })

  //  val view = new EditorView(
  //    editorDiv.asInstanceOf[DOMNode],
  //    new DirectEditorProps {
  //      var state = myState
  //    }
  //  )

  //  view
  //}

  //def setupProsemirrorOnChangeEvent(prosemirror: EditorView, preview: html.Element, previewContainer: html.Element) = {
  //  import typings.prosemirrorState.mod.Transaction
  //  import typings.prosemirrorState.mod._
  //  val previewCallbackProp = new PartialDirectEditorProps {
  //    dispatchTransaction = transaction => {
  //      val transactionStart = new Date().getTime()
  //      val newState = prosemirror.state.asInstanceOf[EditorState].apply(transaction.asInstanceOf[Transaction])
  //      prosemirror.updateState(newState)
  //      //NilsdocRenderer.parseAndRenderBook(Some(documentTitle.getTitle), prosemirrorText(prosemirror), preview, previewContainer)
  //      ??? // TODO: Need to pass the document title properly as its current value (requires validation!)
  //      val transactionEnd = new Date().getTime()
  //      println(s"Took ${transactionEnd - transactionStart}ms to execute & render prosemirror transaction.")
  //    }
  //  }
  //  prosemirror.pasteHTML("heyo <b>bold</b> not bold. ")
  //  prosemirror.setProps(previewCallbackProp)
  //}

  //def prosemirrorText(editor: EditorView): Vector[String] = {
  //  editor.state.asInstanceOf[EditorState].doc.textContent.split("\n").toVector
  //}

  def setupButtonsToChangeEditorVisibility(editorContainer: html.Div)(using viewContext: DocumentContext) = {
    val editorVisibility = EditorVisibility(editorContainer)
    val buttonEditor: html.Button = dom.document.getElementById(Config.ViewButtons.showEditor).asInstanceOf[HTMLButtonElement]
    val buttonBoth: html.Button = dom.document.getElementById(Config.ViewButtons.showBoth).asInstanceOf[HTMLButtonElement]
    val buttonPreview: html.Button = dom.document.getElementById(Config.ViewButtons.showPreview).asInstanceOf[HTMLButtonElement]
    buttonEditor.onclick = _ => editorVisibility.setVisibility(Visibility.Editor)
    buttonBoth.onclick = _ => editorVisibility.setVisibility(Visibility.Both)
    buttonPreview.onclick = _ => editorVisibility.setVisibility(Visibility.Preview)
    dom.window.addEventListener(
      "keydown",
      (ev: dom.KeyboardEvent) =>
        if (ev.key == "Tab") {
          editorVisibility.bump()
          ev.preventDefault()
        }
    )
  }

  def setupShowPrintViewButton()(using viewContext: DocumentContext) = {
    ShowPrintViewButton(byId[html.Button](Config.Printing.togglePrintButtonId), byId[html.Div](Config.Printing.printOutputDivId))
    // TODO: figure out how to let user click the print icon. Must happen via the ShowPrintViewButton class probably
  }

  def setupPublishButton(titleField: TitleField)(using viewContext: DocumentContext) = {
    val publishButton = byId[html.Anchor](Config.ViewButtons.publishButtonId)
    publishButton.onclick = _ => {
      viewContext.title match {
        case Some(title) => 
          dom.window.location.href = s"/users/${TODO_USER_ID}/documents/${title}/publish"
        case None =>
          titleField.updateTitleErrorMessage()
          titleField.blinkErrorMessage()
      }

    }
  }
}
