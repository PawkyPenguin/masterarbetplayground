package nilsdocWorkstation

import concurrent.ExecutionContext.Implicits.global

import scala.scalajs.js.annotation.JSExportStatic
import scala.scalajs.js.annotation.JSExportTopLevel
import util.Util
import org.scalajs.dom.html
import org.scalajs.dom
import util.Util._
import globals.Config
import api.DataStructures.Document
import scala.collection.mutable
import api.DataStructures.getPublishForReaderButtonMessage
import org.scalajs.dom.HTMLElement
import api.ClientApi
import api.Routes
import scala.concurrent.Future
import scala.scalajs.js.timers._
import scala.util.Success
import scala.util.Failure
import dynamicComponents.GridEditor.loadImages

object PublishView {

    def getUserId(checkbox: html.Input): Int = checkbox.dataset(Config.PublishView.userIdData).toInt

    @JSExportTopLevel(name = "main", moduleID = "publishView")
    def main() = {
        val userId = Util.getCurrentUsername
        val document = upickle.default.read[Document](byId[html.Script](Config.titleStorageAreaId).textContent)

        val errorMessageField = byId[html.Span](Config.PublishView.serverRequestErrorMsgId)
        val checkboxes = byClass[html.Input](Config.PublishView.checkboxClass).toVector
        val checkedUsers: mutable.Set[Int] = mutable.Set(checkboxes.filter(_.checked).map(getUserId(_))*)
        val loadingIcon = byId[HTMLElement](Config.PublishView.loadSpinnerId)
        val publishButton = byId[html.Button](Config.PublishView.publishForRealButtonId)
        publishButton.textContent = getPublishForReaderButtonMessage(checkedUsers.size) // even though it's set by the server, we must set it again here because the browser can cache different values for checkbox state
        publishButton.onclick = _ => {
            println("clicked")
            def onRequestFail(msg: String) = {
                publishButton.disabled = false
                Util.hideHtmlEl(loadingIcon)
                errorMessageField.textContent = msg
                Util.showHtmlEl(errorMessageField)
            }
            def onRequestSucceed() = {
                publishButton.disabled = false
                Util.hideHtmlEl(loadingIcon)
            }
            publishButton.disabled = true
            Util.hideHtmlEl(errorMessageField)
            Util.showHtmlEl(loadingIcon)
            val usersWithReadPermission = api.DataStructures.NewReadPermissions(checkedUsers.toSeq)
            val fetchRequest = ClientApi.jsonFetch(Routes.makePublishUrl(userId.toString, document.title), Some(usersWithReadPermission), org.scalajs.dom.HttpMethod.PUT)
            val attemptedFetch = Util.runFutureWithTimeout(fetchRequest, 5000)
            val errorMessage = attemptedFetch match {
                case None => onRequestFail(Config.PublishView.requestErrorMessage)
                case Some(f) => f.onComplete {
                    case Success(_) => onRequestSucceed()
                    case Failure(exception) => onRequestFail(Config.PublishView.requestErrorMessage)
                }
            }
        }

        checkboxes.foreach(c => c.onchange = _ => {
            if (c.checked) {
                checkedUsers += getUserId(c)
            } else {
                checkedUsers -= getUserId(c)
            }
            publishButton.textContent = getPublishForReaderButtonMessage(checkedUsers.size)
        })

        val searchField = byId[html.Input](Config.PublishView.searchFieldId)
        searchField.oninput = _ => {
            val currentText = searchField.value
            val searchedForUsers = checkboxes.filter(c => c.id.startsWith(currentText))
            searchedForUsers.foreach(_.parentElement.classList.remove("invisible"))
            val notSearchedForUsers = checkboxes.filterNot(c => c.id.startsWith(currentText))
            notSearchedForUsers.foreach(_.parentElement.classList.add("invisible"))
        }
    }
}
