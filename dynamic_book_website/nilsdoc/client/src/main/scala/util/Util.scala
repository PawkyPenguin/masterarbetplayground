package util

import org.scalajs.dom.HTMLElement
import globals.Config
import org.scalajs.dom.HttpMethod
import org.scalajs.dom
import org.scalajs.dom.RequestInit
import org.scalajs.dom.Headers
import scalajs.js
import scalajs.js.JSConverters._
import api.Routes
import scala.collection.mutable.ArrayBuffer
import scala.concurrent.Future
import api.Routes.urlEncode
import concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Promise
import org.scalajs.dom.Response
import globals.Config.DynamicComponentHtmlClasses
import scalatags.JsDom.all._
import api.DataStructures
import org.scalajs.dom.URL
import org.scalajs.dom.html
import api.DataStructures._
import scalatags.JsDom.TypedTag
import api.ClientApi._
import api.ClientApi
import api.DataStructures
import org.scalajs.dom.URL
import org.scalajs.dom.html
import api.DataStructures._
import util.Util._
import api.Nilsobjects._
import upickle.default.ReadWriter
import scala.scalajs.js.timers._

object Util {
  def isAuthor: Boolean = !dom.document.body.classList.contains(Config.isReaderCssClass)

  case class DocumentContext(authorId: Int, userId: Int, title: Option[String], editorContents: Vector[String], preview: html.Element, previewContainer: html.Element) {
    val isAuthor = userId == authorId
  }

  def getCurrentUsername: Int = URL(dom.window.location.href).pathname.split("/")(2).toInt

  def letItBlink(el: HTMLElement) = {
    el.classList.remove(Config.Ref.blinkClass)
    el.offsetHeight
    el.classList.add(Config.Ref.blinkClass)
  }

  def hideHtmlEl(el: HTMLElement) = {
    el.classList.add("invisible")
  }

  def showHtmlEl(el: HTMLElement) = {
    el.classList.remove("invisible")
  }

  def byId[A](id: String) = dom.document.getElementById(id).asInstanceOf[A]

  def byClass[A](className: String) = dom.document.getElementsByClassName(className).map(_.asInstanceOf[A])

  def runFutureWithTimeout(future: Future[?], timeoutMs: Int): Option[Future[?]] = {
    val fiveSeconds = Future {
        setTimeout(timeoutMs) {
            Future.successful(())
        }
    }
    Future.firstCompletedOf(Seq(future, fiveSeconds)) match {
      case _ if fiveSeconds.isCompleted => None
      case f: Future[?] => Some(f)
    }
  }

}
