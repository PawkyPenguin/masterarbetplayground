package api

import concurrent.ExecutionContext.Implicits.global
import org.scalajs.dom.HTMLElement
import globals.Config
import org.scalajs.dom.HttpMethod
import org.scalajs.dom
import org.scalajs.dom.RequestInit
import org.scalajs.dom.Headers
import scalajs.js
import scalajs.js.JSConverters._
import api.Routes
import scala.collection.mutable.ArrayBuffer
import scala.concurrent.Future
import api.Routes.urlEncode
import concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Promise
import org.scalajs.dom.Response
import globals.Config.DynamicComponentHtmlClasses
import scalatags.JsDom.all._
import api.DataStructures
import org.scalajs.dom.html
import api.DataStructures._
import util.Util._
import api.Nilsobjects._
import upickle.default.ReadWriter

object ClientApi {
  def jsonFetch[A : upickle.default.ReadWriter](route: String, payload: Option[A], m: HttpMethod): Future[Response] = {
    val fut = dom.fetch(
      route,
      new RequestInit {
        method = m
        body = payload.map(p => upickle.default.write(p)).orUndefined
        headers = new Headers {
          js.Array(
            js.Array("Content-Type", "application/json"),
            js.Array("Accept", "application/json"),
          )
        }
      }
    ).toFuture
    fut.foreach(response => response.json().toFuture.foreach(dom.console.log(_)))
    fut
  }

  def loadNilsobjects[A <: NilsobjectPayload : ReadWriter](nilsobjType: Config.DynamicComponentHtmlClasses.NilsobjType): Vector[A] =
    byId[html.Div](DynamicComponentHtmlClasses.storageAreaId)
      .getElementsByClassName(DataStructures.getNilsobjStorageClassFromType(nilsobjType.asString))
      .toVector
      .map(e => upickle.default.read[A](e.textContent))

  def loadNilsobjPayload[A <: NilsobjectPayload : ReadWriter : Emptiable](e: html.Element): A = loadNilsobjPayload(e.dataset(DynamicComponentHtmlClasses.nilsobjNameData))

  def loadNilsobjPayload[A <: NilsobjectPayload : ReadWriter : Emptiable](nilsobjName: String): A = {
      val storageElement = dom.document.getElementById(DynamicComponentHtmlClasses.makeNilsobjStorageId(nilsobjName))
      val data =
        if (storageElement == null) {
          summon[Emptiable[A]].emptyContent(nilsobjName)
        } else {
          val storedElement = upickle.default.read[A](storageElement.textContent)
          storedElement
        }
      data
  }

  def responseToJsonString(r: Future[Response]): Future[String] = r.flatMap(response =>
      response.json().toFuture
    ).map(js.JSON.stringify(_))


  def getJsonObj(route: String) = jsonFetch[Unit](route, None, HttpMethod.GET)

  private def postOrDeleteNilsdocObject(route: String, userId: Int, documentAuthor: Int, documentTitle: String, obj: NilsobjectPayload) =
    val nilsobject = DataStructures.Nilsobject.fromPayload(userId, documentAuthor: Int, documentTitle, obj)
    if (obj.isEmpty) {
      jsonFetch(route, Some(nilsobject), HttpMethod.DELETE)
    } else {
      jsonFetch(route, Some(nilsobject), HttpMethod.POST)
    }

  object JsHtmlTemplates extends DataStructures.SharedHtmlTemplates(scalatags.JsDom)
  def storeNilsobjectPayload(userId: Int, documentAuthor: Int, documentTitle: Option[String], o: NilsobjectPayload) = {
    if (documentTitle.nonEmpty) {
      ClientApi.postOrDeleteNilsdocObject(Routes.nilsObjectPostOrDeleteUrl(), userId, documentAuthor: Int, documentTitle.get, o).flatMap(_.json().toFuture).map(println(_))
    }
    var storageElement = byId[html.Script](Config.DynamicComponentHtmlClasses.makeNilsobjStorageId(o.name))
    if (storageElement == null) {
      storageElement = JsHtmlTemplates.makeStorageHtmlForNilsobj(o.name, o).render.asInstanceOf[html.Script]
      val storageArea = byId[html.Element](Config.DynamicComponentHtmlClasses.storageAreaId)
      storageArea.append(storageElement)
    } else {
      storageElement.textContent = upickle.default.write(o)
    }
  }


}
