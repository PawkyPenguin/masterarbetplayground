package dynamicComponents
import org.scalajs.dom
import org.scalajs.dom.html.Button
import org.scalajs.dom.{Element, HTMLCollection, MessageEvent, html}
import scalatags.JsDom
import scalatags.JsDom.all._

import scala.scalajs.js.annotation.{JSExport, JSExportTopLevel}
import globals.Config
import util.Util.DocumentContext



object ScalaJSExample {
  def main(): Unit = {
    val assemblerInstructionsHtmls = dom.document.getElementsByTagName(Config.codeTag)
    setupMachines(assemblerInstructionsHtmls)

    val tangledMathboxes = FunctionTangler.setupFunctions()
    // call mathbox setup with dummy context.
    MathboxSetup.showMathboxVectors()(using DocumentContext(-1, -1, None, null, dom.document.body, dom.document.body))
  }

  private def onWebSocketMessage(programsHtml: HTMLCollection[Element], socket: WebsocketClient) = (ev: MessageEvent) => {
    def markCurrentInstruction(programHtml: dom.Element, stateTable: Seq[ujson.Value]): Unit = {
      val currentInstruction: Int = stateTable.last(3)(0).num.toInt - 1
      tryToMarkInstruction(programHtml, currentInstruction)
    }

    def getCurrentStep(state: ujson.Value) = state(0).num.toInt

    def createMachineStateTable(stateTable: Seq[ujson.Value]) = {
      val tableRows = stateTable.map {state =>
        val step = getCurrentStep(state)
        val cpuREGS = state(2).arr.map(_.num.toInt)
        val registers = state(3).arr.map(_.num.toInt)
        tr(
          td(step),
          cpuREGS.map(reg => td(reg)).toIndexedSeq,
          td(span(`class` := Config.lineNumHightlightClass, registers.head)),
          registers.tail.map(reg => td(reg)).toIndexedSeq
        )
      }

      table(
        tr(
          th("Step"),
          (0 until stateTable.last(2).arr.length).map(i => th(s"REG${i+1}")),
          (0 until stateTable.last(3).arr.length).map(i => th(s"R(${i})"))
        ),
        tableRows.toIndexedSeq
      ).render
    }

    def putAssemblerError(program: dom.Element, message: String) = {
      val errorElement = getProgramErrorHtml(program)
      errorElement.innerHTML = ""
      errorElement.appendChild(div(b("Error: "), span(message)).render)
    }

    def appendOutputMessage(program: dom.Element, message: String, currentStep: Int): Unit = {
      if (message != "") {
        getProgramOutputHtml(program).appendChild(p(b(s"Output in Step $currentStep: "), message).render)
        ()
      }
    }

    val content = ev.data.toString
    println(s"Message: ${content}")
    val json = ujson.read(content)
    json("type").str match {
      case "createdMachine" =>
        println("created")
      case "output" =>
        val machine = json("content")("machineId").num.toInt
        val programHtml = programsHtml(machine)
        val stateTable: Vector[ujson.Value] = json("content")("state").arr.toVector
        markCurrentInstruction(programHtml, stateTable)

        val output = json("content")("output").str
        appendOutputMessage(programHtml, output, getCurrentStep(stateTable.last)-1)


        val outputTable = getProgramOutputTable(programHtml)
        outputTable.innerHTML = ""
        outputTable.appendChild(createMachineStateTable(stateTable))

      case "finished" =>
        val id = json("content").num
        println("machine finished")
      case "error" =>
        val machine = json("content")("machineId").num.toInt
        val programHtml = programsHtml(machine)
        val errorMsg = json("content")("errorMessage").str
        putAssemblerError(programHtml, errorMsg)
    }
  }

  def getProgramErrorHtml(program: dom.Element) = program.getElementsByClassName(Config.assemblerError)(0)
  def getProgramOutputHtml(program: dom.Element) = program.getElementsByClassName(Config.machineOutputClass)(0)
  def getProgramOutputTable(program: dom.Element) = program.getElementsByClassName(Config.machineOutputTableClass)(0)

  def tryToMarkInstruction(programHtml: dom.Element, instruction: Int) = {
    val instructionsHtml = programHtml.getElementsByClassName(Config.instructionClass)
    instructionsHtml.foreach(_.classList.remove(Config.markedCss))
    if (instruction < instructionsHtml.length) {
      instructionsHtml(instruction).classList.add(Config.markedCss)
    }
  }

  private def resetProgramHtml(programHtml: dom.Element): Unit = {
    tryToMarkInstruction(programHtml, 0)
    getProgramOutputTable(programHtml).innerHTML = ""
    getProgramErrorHtml(programHtml).innerHTML = ""
    getProgramOutputHtml(programHtml).innerHTML = ""
  }

  def getProgramsFromHTML(programHtmls: HTMLCollection[Element]): Seq[Machine] = {
    val programs: Vector[Array[String]] = programHtmls.map(program => program.textContent.trim.split(";")).toVector
    programs.map(program => Machine(program.head, program.tail.toIndexedSeq))
  }

  def renderHtml(programHtml: dom.Element, program: Machine, stepButton: JsDom.TypedTag[Button]) = {
    val inputField = input(`class` := Config.assemblerInputClass).render
    inputField.defaultValue = program.input
    val programContainer = div(
      "Input: ",
      inputField,
      div(
        `class` := "programOutputWrapper",
        div(
          `class` := "codeBlock",
          pre(
            program.instructions.zipWithIndex.map { case (instruction, instructionNum) =>
              div(
                `class` := Config.instructionClass,
                span(
                  `class` := Config.lineNumHightlightClass,
                  instructionNum + 1
                ),
                s" - $instruction"
              )
            }.toIndexedSeq),
          div(
            stepButton
          )
        ),
        div(
          `class` := s"machineOutputBlock ${Config.machineOutputTableClass}"
        )
      ),
      div(
        `class` := Config.assemblerError
      ),
      div(
        `class` := Config.machineOutputClass
      )
    ).render
    programHtml.innerHTML = ""
    programHtml.appendChild(programContainer)
    tryToMarkInstruction(programHtml, 0) // FIXME: Duplicate code logic. I also call this on machine recreate
  }

  @JSExport
  def setupMachines(programHtmls: HTMLCollection[Element]): Unit = {
    val programs = getProgramsFromHTML(programHtmls)
    val ws = new WebsocketClient(programs)
    ws.open(onWebSocketMessage(programHtmls, ws))

    var j = 0
    while (j < programHtmls.length) {
      val i = j
      val stepButton: JsDom.TypedTag[Button] = button("Step through", `onclick` := { () => ws.step(i)})
      renderHtml(programHtmls(j), programs(j), stepButton)
      j += 1
    }


    val inputElements = dom.document.querySelectorAll("input").map(el => el.asInstanceOf[html.Input])
    inputElements.zipWithIndex.foreach {case (input, i) =>
      input.onblur = _ => {
        resetProgramHtml(programHtmls(i))
        val currentMachine: Machine = programs(i)
        val newMachine = currentMachine.copy(input = input.value)
        ws.newMachine(i, newMachine)
      }
    }
  }

}

