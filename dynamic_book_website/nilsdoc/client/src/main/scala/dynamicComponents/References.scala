package dynamicComponents
import org.scalajs.dom.html
import org.scalajs.dom
import scala.scalajs.js.timers
import globals.Config
import scala.scalajs.js.timers.SetTimeoutHandle
import scalajs.js.Dynamic.global
import scala.scalajs.js.Dynamic.literal
import scala.scalajs.js.Dynamic
import scala.scalajs.js.Dynamic.newInstance
import org.scalajs.dom.HTMLElement
import org.scalajs.dom.Element
import util.Util.DocumentContext


object References {
  def copyCanvas(from: Element, to: Element): Unit = {
    from.children.toVector.zip(to.children.toVector).foreach((from, to) =>
      if (from.isInstanceOf[html.Canvas]) {
        val canvasFrom = from.asInstanceOf[html.Canvas]
        val ctxTo = to.asInstanceOf[html.Canvas].getContext("2d")
        ctxTo.drawImage(canvasFrom, 0, 0)
      } else {
        copyCanvas(from, to)
      }
    )
  }
  def renderReferences()(using viewingContext: DocumentContext) = {
    import scalatags.JsDom.all._
    val refElements = viewingContext.preview.getElementsByClassName(Config.Ref.refClassName)
    val container = dom.document.getElementById(Config.Ref.refHoverContainerId).asInstanceOf[dom.html.Div]
    var mouseIsOverContainer = false
    val observerOptions = literal(
        treshold = 1.0
    )
    var observations = 0
    val callback = (entries: Dynamic, observer: Dynamic) => {
        println(s"observed: $observations")
        if (observations >= 2) {
            entries.forEach((e: Dynamic) => {
                println("got interesction")
                observer.unobserve(e.target)
                e.target.classList.remove(Config.Ref.blinkClass)
                e.target.offsetWidth
                e.target.classList.add(Config.Ref.blinkClass)
                observations = 0
            })
        } else {
            observations += 1
        }
    }
    val observer = newInstance(global.IntersectionObserver)(callback, observerOptions)
    container.onmouseover = _ => {
      mouseIsOverContainer = true
    }
    container.onmouseleave = _ => {
      //container.style.left = "-10000px";
      container.classList.add("invisible")
      mouseIsOverContainer = false
    }
    refElements.foreach(el => {
      val anchor = el.asInstanceOf[dom.html.Anchor]
      val nilsdocTarget = anchor.dataset.get(Config.Ref.targetDataName)
      val referencedElementId = Config.Ref.makeRefTargetId(nilsdocTarget.get)
      val referencedElement = dom.document.getElementById(referencedElementId).asInstanceOf[dom.HTMLElement]
      if (referencedElement == null) {
        anchor.text = Config.Ref.makeReferenceErrorMsg(nilsdocTarget.get)
        anchor.classList.add("named-formatter-error")
      } else {
        val cloneToDisplay = referencedElement.cloneNode(deep = true).asInstanceOf[dom.HTMLElement]
        cloneToDisplay.style.width = s"${referencedElement.clientWidth}px"
        dom.console.log(referencedElement)
        cloneToDisplay.id = ""
        cloneToDisplay.onclick = _ => referencedElement.click() // XXX: Is this too brittle? But the other option is keeping a global map of pinnable_node<->their_clones, which isn't nice.
        copyCanvas(referencedElement, cloneToDisplay)
        var timeoutHandle: SetTimeoutHandle = null
        anchor.onmouseenter = _ => {
            timers.clearTimeout(timeoutHandle) // clear any leftover timeouts, because otherwise the previewed element would flicker incorrectly if user repeatedly hovers and un-hovers over link anchor.
            container.innerHTML = ""
            container.appendChild(cloneToDisplay)
            container.classList.remove("invisible")
            container.style.left = s"${anchor.getBoundingClientRect().left}px"
            container.style.top = s"${anchor.getBoundingClientRect().top - referencedElement.getBoundingClientRect().height}px"
        }
        anchor.onmouseleave = _ => {
            timeoutHandle = timers.setTimeout(100)(
                if (!mouseIsOverContainer) {
                    //container.style.left = "-10000px";
                    container.classList.add("invisible")
                }
        )}
        anchor.onclick = _ => {
            container.classList.add("invisible")
            referencedElement.classList.remove(Config.Ref.blinkClass)
            referencedElement.offsetWidth // trigger reflow for animation
            referencedElement.classList.add(Config.Ref.blinkClass)
            observer.observe(anchor)
        }
        
      }
    })
  }
  
}
