package dynamicComponents

import org.scalajs.dom.html
import org.scalajs.dom.console
import scalatags.JsDom.all._
import globals.Config.ImageAlignment
import util.Util.DocumentContext


object AlignedImage {
  def alignImagesToParagraphs()(using viewingContext: DocumentContext) = {
    val images = viewingContext.preview.getElementsByClassName(ImageAlignment.aligned)
    images.foreach(e =>
      val img = e.asInstanceOf[html.Image]
      val parentNode = img.parentNode match {
        case parentParagraph: html.Paragraph =>
          img.remove() // remove img from DOM
          val grandparent = parentParagraph.parentNode // this should be turned into a div.
          val flexboxDiv = div(
            `class` := ImageAlignment.alignContainer
          ).render
          grandparent.replaceChild(flexboxDiv, parentParagraph) // swap paragraph with new node

          // add image back in. Use correct order for alignment left, resp. right.
          if (img.classList.contains(ImageAlignment.left)) {
            flexboxDiv.appendChild(img)
            flexboxDiv.appendChild(parentParagraph)
          } else if (img.classList.contains(ImageAlignment.leftCenter)) {
            flexboxDiv.appendChild(img)
            flexboxDiv.appendChild(parentParagraph)
            flexboxDiv.classList.add(ImageAlignment.containerCenterImgContent)
          } else if (img.classList.contains(ImageAlignment.right)) {
            flexboxDiv.appendChild(parentParagraph)
            flexboxDiv.appendChild(img)
          } else if (img.classList.contains(ImageAlignment.rightCenter)) {
            flexboxDiv.appendChild(parentParagraph)
            flexboxDiv.appendChild(img)
            flexboxDiv.classList.add(ImageAlignment.containerCenterImgContent)
          }
        case _ => console.log("Image not a child of paragraph! (this is a bit strange)")
      }
    )
  }
}
