package dynamicComponents

import globals.Config
import org.scalajs.dom
import org.scalajs.dom.{document, window}

import scala.scalajs.js
import scala.scalajs.js.Dynamic.{global, literal}
import org.scalajs.dom.ext._
import org.scalajs.dom.html
import scalatags.JsDom.all._
import scala.scalajs.js.annotation.JSGlobal
import util.Util.DocumentContext

object MathboxSetup {

  def createMathbox(box: dom.Element, dimensions: Int, camPos: js.Array[Double] = js.Array(0, 0, 2),
                    range: js.Array[_ <: js.Any]): (js.Dynamic, js.Dynamic) = {
    val mathBox = global.mathBox(literal(
      plugins = js.Array("core", "controls", "cursor", "mathbox"),
      element = box,
      size = literal(
        width = document.body.offsetWidth * 0.5,
        height = document.body.offsetWidth * 0.5 //idk lol
      ),
      controls = literal(klass = global.THREE.OrbitControls)
    ))

    val three = mathBox.three
    three.renderer.setClearColor(js.Dynamic.newInstance(global.THREE.Color)((0xFFFFFF), 1.0))

    val cam = mathBox.camera(literal(
      proxy = true,
      position = camPos
    ))
    val view = mathBox.cartesian(literal(
      range = range, //TODO: Range doesn't get set properly...
      scale = js.Array(1, 1)
    ))
    view.axis(literal(
      axis = 1,
      width = 3
    )).axis(literal(
      axis = 2,
      width = 3
    ))

    if (dimensions == 2) {
      view.grid(literal(
        width = 1,
        divideX = 10,
        divideY = 10
      ))
    } else {
      view.axis(literal(
        axis = 3,
        width = 3
      ))
    }

    mathBox.select("axis").set("color", "black")
    mathBox.set("focus", 3)

    var scale = view.scale(literal(
      divide = 4
    )).ticks(literal(
      width = 4,
      size = 10,
      color = "black"
    ))

    var format = view.format(literal(
      digits = 2,
      weight = "bold"
    ))
    var labels = view.label(literal(
      color = "red",
      zIndex = 1
    ))

    (mathBox, view)
  }

  def showMathboxVectors()(using viewingContext: DocumentContext) = {
    def getVectorsFromDom(box: dom.Element) = {
      window.console.log(s"Vectorbox string: ${box.innerText}")
      val vectorStrings: Array[Array[String]] = box.innerText.split(";").map(_.split("-"))
      val maybeCompleteVectors = vectorStrings.map(_.map(_.trim.split(" ").map(_.toDouble)))
      maybeCompleteVectors.map(maybeCompleteVector =>
        if (maybeCompleteVector.length == 2)
          MathVector(maybeCompleteVector(0), maybeCompleteVector(1))
        else {
          val dimensions = maybeCompleteVector(0).length
          MathVector(Array.fill(dimensions)(0.0), maybeCompleteVector(0))
        }
      )
    }

    def calculateViewProperties(vectors: Array[MathVector]) = {
      val maximumsInEachDimension = vectors.map(_.asArray).transpose.map(dimension => dimension.max)
      val minimumsInEachDimension = vectors.map(_.asArray).transpose.map(dimension => dimension.min)
      val rangePadding = 1.1
      val range = minimumsInEachDimension.zip(maximumsInEachDimension).map {
        case (min, max) => js.Array(min * rangePadding, max * rangePadding)
      }.asInstanceOf[js.Array[js.Dynamic]]

      val camZ = if (maximumsInEachDimension.length == 3) maximumsInEachDimension(2).max(3) else 3
      val camPos = js.Array(1.2, 1.2, camZ)
      (range, camPos)
    }

    case class MathVector(start: Array[Double], end: Array[Double]) {
      def length: Int = start.length
      def asArray: Array[Double] = start ++ end
    }

    val vectorboxes = viewingContext.preview.getElementsByClassName(Config.vectorboxClass)
    val data: Seq[Array[MathVector]] = vectorboxes.map(getVectorsFromDom).toSeq

    vectorboxes.zipWithIndex.foreach{case (box, i) =>
      box.innerHTML = ""
      val vectors = data(i)
      val dimensions = vectors(0).length
      assert(dimensions == 2 || dimensions == 3)
      vectors.foreach{v => assert(v.start.length == dimensions && v.end.length == dimensions)}

      import js.Dynamic.literal

      val (range, camPos) = calculateViewProperties(vectors)
      val (mathBox, view) = createMathbox(box, dimensions, camPos, range)

      val vectorsInJsFormat: Array[js.Array[Double]] = vectors.map(v => js.Array(v.asArray*))
      val flattenedJsVectorArray: js.Array[Double] = vectorsInJsFormat.reduce(_ ++ _)


      //val vectorsInJsFormat: js.Array[Double] = vectors.map(v => v.asArray.to[js.Array[Double]]).reduce(_.++(_))
      val arrayTest = view.array(literal(
        data = flattenedJsVectorArray,
        channels = dimensions,
        items = vectors.length * 2
      )).vector(literal(
        end = true,
        width = 2,
        color = "#50A000"
      ))
    }
  }

  @js.native
  @JSGlobal("mathBox")
  class Mathbox(l: js.Dynamic) extends js.Object {

  }

}
