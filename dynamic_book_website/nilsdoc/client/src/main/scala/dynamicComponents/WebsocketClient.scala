package dynamicComponents
import org.scalajs.dom.{MessageEvent, WebSocket, document, html}

case class Machine(input: String, instructions: Seq[String])

class WebsocketClient(machines: Seq[Machine]) {
  var ws: WebSocket = null

  def open(onMessage: Function1[MessageEvent, _]) = {
    ws = new WebSocket("ws://localhost:8088")

    ws.onmessage = onMessage
    ws.onopen = _ => {
      println("opened client socket!")
      var i = 0
      while (i < machines.length) {
        newMachine(i, machines(i))
        i += 1
      }
    }
  }

  def step(machineId: Int) = send(WebsocketProtocol.step(machineId))

  def newMachine(machineId: Int, machine: Machine): Unit = {
    send(WebsocketProtocol.newMachine(machineId, machine))
  }

  private def send(message: ujson.Obj): Unit = {
    ws.send(message.toString)
  }

  private object WebsocketProtocol {
    def newMachine(machineId: Int, machine: Machine) = ujson.Obj(
      "type" -> "newMachine",
      "content" -> ujson.Obj(
        "machineId" -> machineId,
        "instructions" -> machine.instructions,
        "input" -> machine.input
      ))

    def step(machineId: Int) = ujson.Obj("type" -> "step", "content" -> machineId)
  }
}
