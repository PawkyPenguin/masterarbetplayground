package dynamicComponents

import org.scalajs.dom.html
import org.scalajs.dom.document
import globals.Config
import scalajs.js
import org.scalajs.dom
import scalajs.js.Dynamic.global
import scalajs.js.Dynamic
import scalajs.js.Dynamic.literal
import scalajs.js.Dynamic.newInstance
import globals.Config.Grid.cellSizeData
import dynamicComponents.GridEditor.loadImages
import dynamicComponents.Grid.rowPadding
import scala.collection.mutable.ArrayBuffer
import util.Util.DocumentContext
import api.Nilsobjects
import api.Nilsobjects._
import api.ClientApi._
import api.ClientApi
import util.Util
import util.Util._


val Konva = global.Konva

object GridEditor {
  val gridCache = collection.mutable.Map[String, Grid]()

  def loadImages(): Vector[Dynamic] = {
    val imageNames = Seq(
      "background.png",
      "bike.png",
      "car.png",
      "man.png",
      "water.png",
      "woman.png"
    )
    (for (img <- imageNames) yield {
      val imageObj = newInstance(global.Image)()
      imageObj.src = Grid.imagePath + img
      imageObj
    }).toVector
  }

  def renderGrids()(using viewingContext: DocumentContext) = {
    val images = loadImages()
    val gs = viewingContext.preview.getElementsByClassName(Config.DynamicComponentHtmlClasses.grid.asString)
    val grids = gs.map(el =>
      val grid = el.asInstanceOf[html.Div]
      val id = grid.id
      val cachedGrid = gridCache.getOrElse(
        id, {
          val x = grid.dataset(Config.Grid.xData).toInt
          val y = grid.dataset(Config.Grid.yData).toInt
          val cellSize: Int = grid.dataset(Config.Grid.cellSizeData).toInt
          Grid(GridData(id, y, x, cellSize), viewingContext.isAuthor, images)
        }
      )
      gridCache(id) = cachedGrid
      cachedGrid.rerender()
    )
  }
}

case class GridData(id: String, rows: Int, cols: Int, cellSize: Int)

object Grid {
  val padding: Int = 20
  val rowPadding: Int = 15
  val imagePath = "/dynamicComponents/grids/"
}

class Grid(gd: GridData, isInteractive: Boolean, images: Vector[Dynamic]) {
  import scalajs.js.DynamicImplicits.number2dynamic

  val konvaImages = Array.fill[Dynamic](images.length)(null)
  val gridImageIds = Array.fill[Array[Int]](gd.cols)(Array.fill[Int](gd.rows)(0))
  var activeImageId: Int = 0

  val containerDiv = document.getElementById(gd.id)
  val rowMinWidth = gd.cellSize * images.length + Grid.rowPadding * images.length + 2 * Grid.padding
  val stage = newInstance(Konva.Stage)(
    literal(
      "container" -> gd.id,
      "width" -> (Seq((gd.cellSize * gd.cols + 2 * Grid.padding), rowMinWidth).max),
      "height" -> (gd.cellSize * gd.rows + 3 * Grid.padding + gd.cellSize)
    )
  )
  val layer = newInstance(Konva.Layer)()
  drawGrid()
  drawImageSelectionRow()

  stage.add(layer)
  layer.draw()
  setMousedown(stage, (evt: Dynamic) => {
    konvaImages(activeImageId).strokeEnabled(false)
    activeImageId = 0
    konvaImages(0).strokeEnabled(true)
  })


  def setMousedown(konvaNode: Dynamic, func: (evt: Dynamic) => Unit): Unit = {
    if (!isInteractive) {
      return
    } else {
      konvaNode.on("mousedown", func)
    }
  }

  def drawGrid() = {
    for (row <- 0.until(gd.rows)) {
      for (col <- 0.until(gd.cols)) {
        val x = Grid.padding + col * gd.cellSize
        val y = Grid.padding + row * gd.cellSize
        val konvaImg = newInstance(Konva.Image)(
          literal(
            "x" -> x,
            "y" -> y,
            "width" -> gd.cellSize,
            "height" -> gd.cellSize,
            "stroke" -> "black",
            "strokeWidth" -> 4,
            "image" -> images(0)
          )
        )

        setMousedown(konvaImg, (evt: Dynamic) => {
          if (gridImageIds(col)(row) == activeImageId) {
              gridImageIds(col)(row) = 0
              konvaImg.image(images(0))
          } else {
              gridImageIds(col)(row) = activeImageId
              konvaImg.image(images(activeImageId))
          }
          evt.cancelBubble = true
          konvaImg.draw()
        })

        layer.add(konvaImg)
      }
    }
  }

  def drawImageSelectionRow() = {
    val imgRowStart = 2 * Grid.padding + gd.rows * gd.cellSize
    for (i <- 0.until(images.length)) {
      val img = images(i)
      img.onload = () => {
        var konvaImg = newInstance(Konva.Image)(
          literal(
            "x" -> (Grid.padding + (Grid.rowPadding + gd.cellSize)*i),
            "y" -> imgRowStart,
            "width" -> gd.cellSize,
            "height" -> gd.cellSize,
            "image" -> img,
            "stroke" -> "red",
            "strokeWidth" -> 3
          )
        )
        konvaImages(i) = konvaImg
        if (i == 0) {
          konvaImg.strokeEnabled(true)
        } else {
          konvaImg.strokeEnabled(false)
        }

        setMousedown(konvaImg, (evt: Dynamic) => {
          konvaImages(activeImageId).strokeEnabled(false)
          activeImageId = i
          konvaImg.strokeEnabled(true)
          evt.cancelBubble = true
        })
        layer.add(konvaImg)
        layer.draw()
      }
    }
  }

  def rerender() = {
    stage.container(gd.id)
    layer.draw()
  }

}
