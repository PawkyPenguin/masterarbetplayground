package dynamicComponents

import util.Util.DocumentContext
import scala.scalajs.js.annotation._
import lineparse.LineParser
import org.scalajs.dom
import org.scalajs.dom.html
import org.scalajs.dom.HTMLDivElement
import org.scalajs.dom.HTMLTextAreaElement
import scala.scalajs.js.Date
import scala.scalajs.js
import scala.scalajs.js.Dynamic
import org.scalajs.dom.HTMLButtonElement
import org.scalajs.dom.Event
import scala.CanEqual.derived
import globals.Config
import scala.scalajs.js.Dynamic.literal
import lineparse.LineParser.parse
import org.scalajs.dom.Element
import scala.scalajs.js.Dynamic.global
import scala.scalajs.js.Dynamic.newInstance
import globals.Database
import org.scalajs.dom.RequestInit
import org.scalajs.dom.HttpMethod
import org.scalajs.dom.Headers
import org.scalajs.dom.KeyboardEvent
import globals.Config.nilsdocRenderArea
import globals.ApplicationConfig.EditorType
import globals.ApplicationConfig
import org.scalajs.dom.URL
import visualEditor.VisualEditor
import scala.collection.mutable.ArrayBuffer
import scala.concurrent.Future
import api.Routes
import util.Util
import util.Util.byId
import globals.Config.SummaryPrompt.summaryViewContainerId
import util.Util.DocumentContext
import scalatags.JsDom.all._
import api.ClientApi.loadNilsobjPayload
import api.Nilsobjects.Audio
import org.scalajs.dom.MouseEvent
import api.ClientApi.storeNilsobjectPayload
import js.Dynamic.literal
import org.scalajs.dom.MediaStreamConstraints
import scala.scalajs.js.Dynamic.global
import concurrent.ExecutionContext.Implicits.global
import scala.collection.mutable.ArrayBuffer
import org.scalajs.dom.Blob
import org.scalajs.dom.BlobPropertyBag


import scalajs.js.JSConverters.JSRichIterableOnce
import scalajs.js.JSConverters.iterableOnceConvertible2JSRichIterableOnce
import org.scalajs.dom.FileReader
import scala.scalajs.js.typedarray.Int8Array
import scala.concurrent.Promise
import org.scalajs.dom.HTMLElement
import scala.scalajs.js.typedarray.Uint8Array


object AudioSetup {
    case class RecordingNode(name: String, parentDiv: html.Element, recordButton: html.Element, audioContent: html.Audio)

    def blobToByteArray(blob: Blob): Future[Array[Byte]] = blob.arrayBuffer().`then`(buffer =>
            new Uint8Array(buffer).toArray.map(_.toByte)
        ).toFuture
    
    def byteArrayToBlob(a: Array[Byte]) = makeBlob(js.Array(new Int8Array(a.toJSArray)))

    def makeBlob(blobParts: js.Iterable[dom.BlobPart]) = 
        new Blob(blobParts, new BlobPropertyBag {
            `type` = "audio/ogg; codecs=opus"
        })


    var currentlyRecordingNode: Option[(RecordingNode, () => Unit)] = None

    class Recorder(node: RecordingNode, initialAudio: Array[Byte])(using viewingContext: DocumentContext) {
        var recordedChunks: ArrayBuffer[Blob] = ArrayBuffer.empty
        if (initialAudio.nonEmpty) {
            val blob = byteArrayToBlob(initialAudio)
            setAudio(blob)
            recordedChunks += blob
        }
        var isRecording = false

        def setAudio(audioBlob: Blob) = {
            val blobUrl = dom.URL.createObjectURL(audioBlob)
            node.audioContent.src = blobUrl
            node.audioContent.load()
            node.audioContent.classList.remove("invisible")
        }

        val mediaStream = dom.window.navigator.mediaDevices.getUserMedia(new MediaStreamConstraints {
            audio = true
        }).toFuture.foreach(stream => {
            val mediaRecorder = js.Dynamic.newInstance(js.Dynamic.global.MediaRecorder)(stream)
            println("here2")
            mediaRecorder.ondataavailable = (e: dom.Event) => {
                recordedChunks += e.asInstanceOf[js.Dynamic].data.asInstanceOf[Blob]
            }
            mediaRecorder.onstart = (_: dom.Event) => recordedChunks.clear()
            mediaRecorder.onstop = (_: dom.Event) => {
                val blob = makeBlob(recordedChunks.toJSArray)
                blobToByteArray(blob).foreach(byteArray => {
                    storeNilsobjectPayload(viewingContext.userId, viewingContext.authorId, viewingContext.title, Audio(node.name, byteArray))
                    setAudio(blob)
                })
            }

            node.recordButton.onclick = _ => {

                if (currentlyRecordingNode.nonEmpty && !isRecording) {
                    // another node is recording already. Alert user of it.
                    if (dom.window.confirm(Config.Audio.stillRecordingMessage(currentlyRecordingNode.get._1.name))) {
                        // we only cancel currently active node and do not start a new recording yet. Doing so requires another click from the user.
                        currentlyRecordingNode.get._2()
                    } else {
                        // user has declined, so do nothing
                    }
                } else if (!isRecording) {
                    if (recordedChunks.nonEmpty) { 
                        // ask user if they wish to delete old recording
                        if (dom.window.confirm(Config.Audio.cancelOldRecordingMessage)) {
                            node.audioContent.pause()
                            node.audioContent.classList.add("invisible")
                            node.audioContent.src = ""
                            startRecording()
                        }
                    } else {
                        startRecording()
                    }
                } else {
                    stopRecording()
                }
            }

            def startRecording() = {
                    isRecording = true
                    currentlyRecordingNode = Some(node, stopRecording)
                    node.recordButton.classList.add(Config.Audio.isRecordingClass)
                    mediaRecorder.start()
            }

            def stopRecording(): Unit = {
                isRecording = false
                currentlyRecordingNode = None
                node.recordButton.classList.remove(Config.Audio.isRecordingClass)
                mediaRecorder.stop()
            }
        })
    }

    def setupAudio()(using viewingContext: DocumentContext) = {
        val audioDivs = viewingContext.preview.getElementsByClassName(Config.DynamicComponentHtmlClasses.audio.asString).toVector.asInstanceOf[Vector[html.Span]]
        audioDivs.foreach(audioSpan => {
            val loadedNilsobject = loadNilsobjPayload[Audio](audioSpan)
            val name = audioSpan.dataset(Config.DynamicComponentHtmlClasses.nilsobjNameData)

            val recordButton = button(
                `class` := "btn btn-outline-dark rounded-pill m-2",
                i(
                    `class` := "fa-solid fa-microphone"
                )
            ).render
                
            val playButton = i(
                `class` := "fa-solid fa-play p-2"
            ).render // unused

            val audioNode = audio(
                `class` := "invisible"
            ).render
            audioNode.controls = true
            audioNode.src = ""
            audioSpan.appendChild(recordButton)
            audioSpan.appendChild(audioNode)

            val recorder = Recorder(RecordingNode(name, audioSpan, recordButton, audioNode), loadedNilsobject.data)
        })
    }
  
}
