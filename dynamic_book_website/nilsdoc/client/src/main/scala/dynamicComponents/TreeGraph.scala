package dynamicComponents

import scala.scalajs.js
import collection.mutable.ArrayBuffer
import org.scalajs.dom.html
import org.scalajs.dom
import globals.Config
import dynamicComponents.TreeGraph.nodeMinWidth
import scala.scalajs.js.Dynamic.global
import org.scalajs.dom.KeyboardEvent
import globals.Config.DynamicComponentHtmlClasses
import org.scalajs.dom.HTMLElement
import upickle.default._
import api.Routes
import api.DataStructures
import globals.Config.TreeGraph.rootDivId
import org.scalajs.dom.MouseEvent
import inlineparser.InlineParser
import ast.AstToHtml
import nilsdocWorkstation.NilsdocRenderer
import util.Util.DocumentContext
import api.DataStructures._
import api.DataStructures
import api.Nilsobjects._
import api.ClientApi._
import api.ClientApi
import util.Util
import util.Util._


object TreeGraph {

  def getMeasureTextWidthFunction(textInputElementForTrees: html.TextArea): (s: String) => Double = {
    val textMeasurementCanvas = dom.document.createElement("canvas").asInstanceOf[html.Canvas]
    val ctx = textMeasurementCanvas.getContext("2d")
    ctx.font = dom.window.getComputedStyle(textInputElementForTrees).font;
    val measureText = (s: String) =>  ctx.measureText(s).width.asInstanceOf[Double] + 20.0
    measureText
  }

  def setupTrees(textInputElementForTrees: html.TextArea, nodeDivsRoot: html.Div)(using viewingContext: DocumentContext) = {
    val measureTextWidth = getMeasureTextWidthFunction(textInputElementForTrees)
    textInputElementForTrees.oninput = _ => {
      textInputElementForTrees.style.width = s"${measureTextWidth(textInputElementForTrees.value)}px" // grow width according to content
    }
    textInputElementForTrees.oncontextmenu = _.preventDefault()

    val treesInRenderArea = viewingContext.preview.getElementsByClassName(Config.DynamicComponentHtmlClasses.tree.asString)
    treesInRenderArea.foreach(e => {
      val treeDiv = e.asInstanceOf[html.Div]
      val tree = ClientApi.loadNilsobjPayload[Tree](treeDiv)
      TreeGraph(treeDiv, tree, textInputElementForTrees, nodeDivsRoot, measureTextWidth)
    })
  }

  val nodeMinWidth = 40.0
  val nodeHeight = 20.0
  val newTreeLayerAreaHeight: Double = nodeHeight
  val vertPadding: Double = 100
  val startEndPadding: Double = 20
  case class Padding()
  def calculateStageHeight(treeLayers: Int) = 2 * startEndPadding + (treeLayers-1) * (nodeHeight + vertPadding) + newTreeLayerAreaHeight

  def deepestLayer(root: TreeNode, depth: Int): Int = {
    if (root.children.isEmpty) {
      depth
    } else {
      root.children.map(deepestLayer(_, depth + 1)).max
    }
  }
}

class TreeGraph(treeDiv: html.Div, tree: Tree, textInput: html.TextArea, nodeDivsRoot: html.Div, measureTextWidth: String => Double)(using viewingContext: DocumentContext) {
  import scalajs.js.Dynamic.newInstance
  import scalajs.js.Dynamic.newInstance
  import scalajs.js.Dynamic.literal
  import TreeGraph._

  val Konva = global.Konva
  val stageWidth: Double = treeDiv.clientWidth * treeDiv.dataset.get(Config.TreeGraph.widthData).get.toDouble / 100.0
  val divNodesContainer = dom.document.createElement("div").asInstanceOf[html.Div]
  val stage = newInstance(Konva.Stage)(
    literal(
      container = treeDiv.id,
      width = stageWidth,
      height = 0
    )
  )
  treeDiv.appendChild(divNodesContainer)
  treeDiv.oncontextmenu = _.preventDefault()
  stage.on(
    "contextmenu",
    (e: Any) => {
      e.asInstanceOf[js.Dynamic].evt.preventDefault();
    }
  )
  val layer = newInstance(Konva.Layer)()

  def rerenderAndStore() = {
    ClientApi.storeNilsobjectPayload(viewingContext.userId, viewingContext.authorId, viewingContext.title, tree)
    render()
  }

  def render(): Unit = {
    def calcNodeY(layerNum: Int) = startEndPadding + layerNum * (nodeHeight + vertPadding)
    def recurse(node: TreeNode, xOffset: Double, xRatio: Double, nodeSegmentWidth: Double, layerNum: Int, parentCoords: Option[(Double, Double)], parent: Option[TreeNode], thisId: Int): Unit = {
      import js.DynamicImplicits.number2dynamic
      // TODO: should adjust stage height & width if necessary until we hit a max.
      val midX = (xOffset + 0.5 * nodeSegmentWidth) * stageWidth
      val newDiv = dom.document.createElement("div").asInstanceOf[html.Div]
      val parsedNilsdown = InlineParser.parseInlineContent(0, 0, node.text.length)(using Vector(node.text))
      newDiv.innerHTML = AstToHtml.translate(parsedNilsdown)(using ()).render
      NilsdocRenderer.renderKatex(newDiv)

      newDiv.classList.add(Config.TreeGraph.divNodeClass)
      newDiv.style.left = "-10000px"
      newDiv.style.minWidth = s"${nodeMinWidth}px"
      newDiv.oncontextmenu = _.preventDefault()
      divNodesContainer.appendChild(newDiv)
      val textWidth = measureTextWidth(node.text)
      val x = midX - newDiv.clientWidth / 2
      val y = calcNodeY(layerNum)
      newDiv.style.left = s"${x}px"
      newDiv.style.top = s"${y + nodeHeight - 2 - 1 - newDiv.clientHeight.asInstanceOf[js.Dynamic]}px" // -1 for the div border (set to 1 in CSS) and the konva stroke border of the node below
      //newDiv.style.width = s"${textWidth}px"
      newDiv.onmouseup = (evt: MouseEvent) => {
        if (textInput.classList.contains("invisible")) {
          evt.preventDefault()
          if (evt.button == 2) {
            if (parent.nonEmpty) {
              parent.get.children.remove(thisId)
              newDiv.remove()
              rerenderAndStore()
            }
          } else {
            textInput.value = node.text
            val bounds = newDiv.getBoundingClientRect()
            textInput.style.left = s"${bounds.left}px"
            textInput.style.top = s"${bounds.top}px"
            textInput.style.height = s"${bounds.height}px"
            textInput.style.lineHeight = newDiv.style.lineHeight
            textInput.style.width = s"${measureTextWidth(node.text)}px"
            textInput.style.textAlign = newDiv.style.textAlign
            textInput.style.color = newDiv.style.color

            newDiv.classList.add("invisible")
            textInput.classList.remove("invisible")
            textInput.focus()
            def textAreaCommit() = {
              node.text = textInput.value
              textInput.classList.add("invisible")
              rerenderAndStore()
            }
            textInput.onblur = _ => textAreaCommit()
            textInput.onkeydown = (e: KeyboardEvent) => {
              if (e.keyCode == 13) {
                textAreaCommit()
              } else if (e.keyCode == 27) {
                textInput.classList.add("invisible")
                newDiv.classList.remove("invisible")
              }
            }
          }
        }
      }
      if (parentCoords.nonEmpty) {
        val x1 = parentCoords.get._1
        val y1 = parentCoords.get._2 + nodeHeight
        val x2 = x + nodeMinWidth / 2
        val y2 = y
        val line = newInstance(Konva.Line)(
          literal(
            points = js.Array(x1, y1, x2, y2),
            stroke = "black",
            strokeWidth = 2
          )
        )
        layer.add(line)
      }
      if (node.children.isEmpty) {
        val nextLayerClickableArea = newInstance(Konva.Rect)(
          literal(
            x = x,
            y = y + nodeHeight,
            width = nodeMinWidth,
            height = newTreeLayerAreaHeight,
            fill = "red",
            stroke = "black"
          )
        )
        nextLayerClickableArea.on(
          "mousedown",
          (evt: Any) => {
            node.children.addOne(TreeNode.empty)
            rerenderAndStore()
          }
        )
        layer.add(nextLayerClickableArea)
      }
      def createSiblingAdder(x: Double, layerNum: Int, width: Double, height: Double, layer: js.Dynamic) = {
        val strokeWidth = 2
        val siblingAdder = newInstance(Konva.Rect)(
          literal(
            x = x,
            y = calcNodeY(layerNum + 1) - 2 - 2 * strokeWidth, // -2 is for 2*div border
            width = width,
            height = height + 2 * strokeWidth,
            strokeWidth = strokeWidth,
            zIndex = 5
          )
        )
        siblingAdder.on("mouseenter", (evt: Any) => siblingAdder.stroke("grey"))
        siblingAdder.on("mouseout", (evt: Any) => siblingAdder.stroke("rgba(0,0,0,0)"))
        layer.add(siblingAdder)
        siblingAdder
      }

      node.children.zipWithIndex.foreach((c, i) => {
        val xRatioOfChildNode = (i.toDouble + 1) / (node.children.length + 1)
        val nextSegmentWidth = nodeSegmentWidth / node.children.length
        val nextSegmentOffset = i * nextSegmentWidth + xOffset
        if (i == 0) {
          val siblingAdder = createSiblingAdder(
            nextSegmentOffset * stageWidth,
            layerNum,
            nextSegmentWidth / 2 * stageWidth - nodeMinWidth / 2,
            nodeHeight,
            layer
          )
          siblingAdder.on(
            "mousedown",
            (evt: Any) => {
              node.children.insert(0, TreeNode.empty)
              rerenderAndStore()
            }
          )
        } else {
          val siblingAdder = createSiblingAdder(
            (nextSegmentOffset - nextSegmentWidth / 2.0) * stageWidth + nodeMinWidth / 2.0,
            layerNum,
            nextSegmentWidth * stageWidth - nodeMinWidth,
            nodeHeight,
            layer
          )
          siblingAdder.on(
            "mousedown",
            (evt: Any) => {
              node.children.insert(i, TreeNode.empty)
              rerenderAndStore()
            }
          )
        }
        if (i == node.children.length - 1) {
          val siblingAdder = createSiblingAdder(
            (nextSegmentOffset + nextSegmentWidth / 2.0) * stageWidth + nodeMinWidth / 2.0,
            layerNum,
            nextSegmentWidth / 2.0 * stageWidth - nodeMinWidth / 2.0,
            nodeHeight,
            layer
          )
          siblingAdder.on(
            "mousedown",
            (evt: Any) => {
              node.children.append(TreeNode.empty)
              rerenderAndStore()
            }
          )
        }
        recurse(c, nextSegmentOffset, xRatioOfChildNode, nextSegmentWidth, layerNum + 1, Some((midX, y)), Some(node), i)
      })
    }

    layer.destroyChildren()
    divNodesContainer.innerHTML = ""
    divNodesContainer.oncontextmenu = _.preventDefault()
    val stageHeight = calculateStageHeight(deepestLayer(tree.root, 1))
    stage.height(stageHeight)
    recurse(tree.root, 0, 1 / 2.0, 1, 0, None, None, 0)
    layer.draw()
  }

  stage.add(layer)
  render()

  def newNode(x: Int, y: Int) = {
    newInstance(Konva.Rect)(
      literal(
        x = x - nodeMinWidth / 2,
        y = y,
        width = nodeMinWidth,
        height = nodeHeight
      )
    )
  }
}
