package dynamicComponents

import nilsdocWorkstation.NilsdocRenderer
import util.Util
import org.scalajs.dom
import Util.DocumentContext
import org.scalajs.dom.Event
import globals.Config
import org.scalajs.dom.html
import globals.Config.DynamicComponentHtmlClasses
import api.DataStructures._
import api.DataStructures
import api.Nilsobjects._
import api.ClientApi._
import api.ClientApi
import util.Util
import util.Util._
import scalatags.JsDom.all._
import org.scalajs.dom.HTMLElement
import org.scalajs.dom.Node
import scala.util.boundary.break
import scala.collection.mutable.ArrayBuffer

def setupOnclick(addButton: html.Div, selection: dom.Selection, range: org.scalajs.dom.Range, counter: PostitCounter)(using viewContext: DocumentContext) = {
    addButton.onclick = _ => {
        Util.hideHtmlEl(addButton)
        PostitNotes.renderPostits()
        val (location, closestHtmlNode) = findNilsdocLoction(range, range.startContainer, viewContext.preview)
        val note = Note(Config.PersonalNotes.makeId(counter.counter), location, "")
        ClientApi.storeNilsobjectPayload(viewContext.userId, viewContext.authorId, viewContext.title, note)
        counter.counter += 1
        renderNote(note) // we could use the `closestHtmlNode` here, but for consistency, we use the same method as for postit notes loaded from the server

        //val endLocation = findNilsdocLoction(range, range.endContainer, viewContext.preview)
        //val region = NilsdocRegion(startLocation.y, endLocation.y, startLocation.x, endLocation.x)
        //findNilsdocNodesInRegion(region, viewContext.preview).foreach(node =>
        // mark nodes visually
        
    }
}

def renderNote(n: Note)(using viewContext: DocumentContext) = {
    //val closestNodeToNote = findNodeContainingLocation(viewContext.preview, n.location)
    val noteContainer = byId[html.Div](Config.PersonalNotes.studentViewNoteContainerId)
    val txtArea = textarea().render
    txtArea.oninput = _ => {
        ClientApi.storeNilsobjectPayload(viewContext.userId, viewContext.authorId, viewContext.title, Note(n.name, n.location, txtArea.value))
    }
    noteContainer.appendChild(textarea().render)

    //closestHtmlNode.appendChild(span(
    //    s"[${counter.counter}]"
    //).render)
}

def findNodeContainingLocation(root: HTMLElement, l: NilsdocLocation): HTMLElement = {
    var el = root
    //while (el != null)
    ???
}

def findNilsdocNodesInRegion(r: NilsdocRegion, root: HTMLElement): Vector[HTMLElement] = {
    import Config.Editor.{fromLineData,fromColData,untilColData,untilLineData}
    def isNilsdownNode(el: Node) = el.isInstanceOf[HTMLElement] && el.asInstanceOf[HTMLElement].dataset.contains(fromLineData)
    var el = root
    val children = el.children
    var i = 0
    val collectedChildren = ArrayBuffer[HTMLElement]()
    while (i < children.length) {
        val c = children(i)
        if (isNilsdownNode(c)) {
            val child = c.asInstanceOf[HTMLElement]
            if (r.fromLine <= child.dataset(fromLineData).toInt && child.dataset(untilLineData).toInt < r.toLine) {
                // child is wholly contained, so add it. No need to recurse.
                collectedChildren += child
            } else if (r.toLine <= child.dataset(fromLineData).toInt) {
                ???
            }

        }
        i += 1
    }
    ???
}

case class NilsdocRegion(fromLine: Int, toLine: Int, fromCol: Option[Int], toCol: Option[Int])
def findNilsdocLoction(range: dom.Range, selectionAnchor: Node, preview: HTMLElement): (NilsdocLocation, Node) = {
    var mayBeBelowCoordinateSpans = true
    var currentEl: Node = selectionAnchor
    while (currentEl != null && currentEl != preview && !(currentEl.isInstanceOf[HTMLElement] && currentEl.asInstanceOf[HTMLElement].dataset.contains(Config.Editor.fromColData))) {
        currentEl = currentEl.parentNode
    }
    if (currentEl == null) {
        throw new IllegalArgumentException("Traversed up the parent hierarchy and skipped nilsdoc view node!")
    }
    if (currentEl == preview) {
        // We went upward but should have been going downward to find a node with nilsdoc location information
        currentEl = selectionAnchor
        while (currentEl != null && !(currentEl.isInstanceOf[HTMLElement] && currentEl.asInstanceOf[HTMLElement].dataset.contains(Config.Editor.fromLineData))) {
            currentEl = currentEl.firstChild
        }
        if (currentEl == null) {
            throw new IllegalArgumentException("Found no node containing nilsdoc line information above and below selected node in preview container.") // should never happen
        }
    }
    dom.console.log(currentEl)
    val closestNodeToSelectionAnchor = currentEl.asInstanceOf[HTMLElement]
    val colInfo = if (closestNodeToSelectionAnchor.dataset.contains(Config.Editor.fromColData)) {
        var from = closestNodeToSelectionAnchor.dataset(Config.Editor.fromColData).toInt
        if (closestNodeToSelectionAnchor.children.isEmpty) {
            // we've selected a bottom-level node. Add the range's offset.
            from += range.startOffset
        }
        Some(from)
    } else {
        None
    }
    val lineInfo = closestNodeToSelectionAnchor.dataset(Config.Editor.fromLineData)
    (NilsdocLocation(lineInfo.toInt, colInfo), closestNodeToSelectionAnchor)
}

class PostitCounter {
    var counter = 0
}

object PostitNotes {
    def renderPostits()(using viewContext: DocumentContext): Unit = {
        val counter = new PostitCounter
        val addButton = dom.document.getElementById(Config.PersonalNotes.noteButtonId).asInstanceOf[html.Div]
        if (NilsdocRenderer.isInReaderView()) {
            val allNotes = ClientApi.loadNilsobjects[Note](Config.DynamicComponentHtmlClasses.note)
            counter.counter += allNotes.length
            allNotes.foreach(renderNote(_))
            // show notes somewhere here!

            dom.document.onselectionchange = (e: Event) => {
                val selection = dom.document.getSelection()
                val range = selection.getRangeAt(0)
                var parent = range.commonAncestorContainer
                while (parent != null && parent != viewContext.preview) {
                    parent = parent.parentNode
                }
                if (parent == viewContext.preview && range.collapsed) { 
                    //dom.document.body.prepend(div(s"${selection.focusNode}, ${selection.anchorNode}").render)
                    val rect = range.getBoundingClientRect()
                    addButton.style.left = s"${rect.x}px"
                    addButton.style.top = s"${rect.y - 50}px"
                    Util.showHtmlEl(addButton)
                    setupOnclick(addButton, selection, range, counter)
                } else {
                    Util.hideHtmlEl(addButton)
                }
            }
        } else {
            dom.document.onselectionchange = _ => ()
        }
    }
}
