package dynamicComponents

import org.scalajs.dom.html
import org.scalajs.dom
import globals.Config
import org.scalajs.dom.HTMLElement
import org.scalajs.dom.HTMLCollection
import util.Util.DocumentContext


object PinnableContent {
    val river: html.Div = dom.document.getElementById(Config.PinContent.riverId).asInstanceOf[html.Div] // TODO: pass as arg?

    def augmentContent()(using viewingContext: DocumentContext) = {

        val pinnableElements = viewingContext.preview.getElementsByClassName(Config.PinContent.pinnableCls).toVector.asInstanceOf[Vector[HTMLElement]]
        pinnableElements.foreach(augmentNode)
        // TODO: I might want to clear the image river here, because recompiling the book makes the images have a new identity -> I can add them twice to the river. Doesn't bother too much tho.
    }

    def augmentNode(node: HTMLElement): HTMLElement = {
        val clonedNode = createCloneDiv(node)
        node.onclick = _ => {
            if (clonedNode.isConnected) {
                // If the node is already pinned, move its clone to the top of the river and let it blink
                clonedNode.classList.remove(Config.Ref.blinkClass)
                clonedNode.remove()
                river.prepend(clonedNode)
                clonedNode.classList.add(Config.Ref.blinkClass)
            } else {
                river.appendChild(clonedNode)
            }
        }
        clonedNode
    }

    def augmentAndPinNode(node: HTMLElement) = {
        val c = augmentNode(node)
        river.appendChild(c)
    }

    def createCloneDiv(node: HTMLElement): html.Div = {
        val clonedNode = node.cloneNode(deep = true).asInstanceOf[HTMLElement]
        val cloneWrapper: html.Div = dom.document.createElement("div").asInstanceOf[html.Div]
        cloneWrapper.append(clonedNode)
        clonedNode.id = ""
        if (node.isInstanceOf[html.Image]) {
            clonedNode.className = ""
            clonedNode.style = ""
        }
        clonedNode.classList.add(Config.PinContent.cloneCls)
        cloneWrapper.onclick = e => {
            if (e.button == 2) {
                unpinNode(node, cloneWrapper)
            } else {

            }
        }
        cloneWrapper.oncontextmenu = e => e.preventDefault()
        cloneWrapper
    }

    def unpinNode(node: HTMLElement, cloneToUnpin: HTMLElement) = {
        if (!cloneToUnpin.isConnected) {
            dom.console.error(s"Pinned image $node does not have a clone that is connected, even though we tried to unpin its clone $cloneToUnpin.")
        }
        cloneToUnpin.remove()
        cloneToUnpin.classList.remove(Config.Ref.blinkClass)
    }
}
