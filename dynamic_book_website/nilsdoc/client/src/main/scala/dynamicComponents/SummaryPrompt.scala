package dynamicComponents

import globals.Config
import org.scalajs.dom.html
import org.scalajs.dom
import org.scalajs.dom.document
import org.scalajs.dom.FocusEvent
import util.Util.DocumentContext
import util.Util.byId
import globals.Config.DynamicComponentHtmlClasses
import api.Nilsobjects
import api.Nilsobjects._
import api.ClientApi._
import api.ClientApi
import util.Util
import util.Util._

object SummaryPrompt {

    private def setupSummaryViewButton(summaryFields: Vector[html.Div], summaryContainer: html.Div)(using viewingContext: DocumentContext) = {
        var showingSummary = false
        val button = byId[html.Button](Config.SummaryPrompt.summaryViewButtonId)
        button.onclick = e => {
            showingSummary = !showingSummary
            if (showingSummary) {
                viewingContext.previewContainer.classList.add(Config.SummaryPrompt.inSummaryView)
                summaryContainer.innerHTML = ""
                summaryFields.foreach(field =>
                    val clonedField = field.cloneNode(deep = true).asInstanceOf[html.Div]
                    clonedField.classList.add(Config.SummaryPrompt.inSummaryView)
                    clonedField.classList.add("my-2")
                    summaryContainer.appendChild(clonedField)
                )
                summaryContainer.classList.remove("invisible")
            } else {
                viewingContext.previewContainer.classList.remove(Config.SummaryPrompt.inSummaryView)
                summaryContainer.classList.add("invisible")
            }
        }
    }

    private def getSummaryFields(root: html.Element) = root.getElementsByClassName(Config.SummaryPrompt.summaryField).map(_.asInstanceOf[html.Div]).toVector
    def renderElements()(using viewingContext: DocumentContext) = {
        val summaryContainer = byId[html.Div](Config.SummaryPrompt.summaryViewContainerId)
        val summaryFields: Vector[html.Div] = getSummaryFields(viewingContext.preview)
        var i = 0 // shouldn't the index of a summary node be set by the AST already? I mean yeah, it's easier here, but less consistent. Need to take care to never forget data that is relevant for a nilsobj.
        summaryFields.foreach(s =>
            val name = Config.SummaryPrompt.makeId(i) // should I make this a ref target? Could unify setup. 
            val fetchedSummary = ClientApi.loadNilsobjPayload[Summary](name)
            s.id = name
            s.dataset(DynamicComponentHtmlClasses.nilsobjNameData) = name
            s.textContent = fetchedSummary.writtenSummary
 
            val previousParagraph = s.previousElementSibling
            if (previousParagraph != null) {
                s.onfocus = (e => {
                    previousParagraph.classList.add(Config.SummaryPrompt.blur)
                })
                s.onblur = (e => {
                    previousParagraph.classList.remove(Config.SummaryPrompt.blur)
                })
            }
            s.oninput = _ => {
                val newSummary = Summary(name, s.textContent)
                ClientApi.storeNilsobjectPayload(viewingContext.userId, viewingContext.authorId, viewingContext.title, newSummary)
            }

            i += 1
        )

        setupSummaryViewButton(summaryFields, summaryContainer)
    }
  
}