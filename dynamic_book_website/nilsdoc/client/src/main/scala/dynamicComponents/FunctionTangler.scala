package dynamicComponents

import org.scalajs.dom
import org.scalajs.dom.{Element, document, html}
import org.scalajs.dom.ext._
import org.scalajs.dom.HTMLCollection
import scalatags.JsDom.all._

import scala.collection.immutable
import scala.collection.mutable.ArrayBuffer
import scala.scalajs.js
import scala.scalajs.js.Dynamic.{global, literal}
import globals.Config

private object IdManager {
  private var functionId = 0
  private var variableId = 0

  def newFunctionId = {
    val id = functionId
    functionId += 1
    s"function-${id}"
  }

  def newVariableId = {
    val id = variableId
    variableId += 1
    s"variable-${id}"
  }

  def tangleVariableName(functionId: String, variableName: String) = s"tangle-variable-${variableName}-in-${functionId}"
}

object FunctionTangler {
  case class SubstitutionInFunction(substitutionPositions: Seq[SubstitutionPosition])
  case class SubstitutionData(substitutedVariable: String, variableMin: Double, variableMax: Double, variableStep: Double, formatString: String)
  case class SubstitutionPosition(index: Int, variableId: String)
  case class SortedPos(position: SubstitutionPosition, variableName: String, formatString: String)

  def getSubstitutionsForFunction(functionText: String, substitutionData: Seq[SubstitutionData]): Seq[SubstitutionInFunction] = {
    substitutionData.map(calculateSubstitutionLocations(_, functionText))
  }

  def getSubstitutionData(binding: dom.Element): SubstitutionData = {
    val variable = binding.textContent.trim
    val start = binding.getAttribute("start").toDouble
    val end = binding.getAttribute("end").toDouble
    val step = binding.getAttribute("steps").toDouble
    val granularity: Int = Math.ceil(-Math.log10(step)).toInt
    val dataFormatString = s"%.${granularity}f"
    SubstitutionData(variable, start, end, step, dataFormatString)
  }

  def calculateSubstitutionLocations(substitutionData: SubstitutionData, functionText: String): SubstitutionInFunction = {
    val functionTextParts = functionText.split(substitutionData.substitutedVariable)
    val lengths = functionTextParts.map(part => part.length)
    var sum = 0
    val substitutionPositions: Array[SubstitutionPosition] = (for {
      length <- lengths
    } yield {
      sum += length
      val substitutionId = IdManager.newVariableId
      SubstitutionPosition(sum, substitutionId)
    }).drop(1)
    SubstitutionInFunction(substitutionPositions.toIndexedSeq)
  }

  def setupTangleBinding(functionId: String, fun: dom.Element, substitutionsPerVariable: Seq[SubstitutionData], mathboxView: js.Dynamic): html.Element = {
    def addFunctionIntoMathbox(view: js.Dynamic, compiledExpression: js.Dynamic, mathScope: js.Dynamic, color: String) = {
      val data = view.interval(literal(
        expr = (emit: js.Dynamic, x: Double, i: Int, t: Double) => {
          mathScope.updateDynamic("x")(x)
          val fOfX = compiledExpression.evaluate(mathScope)
          emit(x, fOfX)
        },
        width = 30,
        channels = 2
      ))
      val curve = view.line(literal(
        width = 2,
        color = color
      ))
      view
    }

    def createInteractionRegion(functionId: String, substitutions: Seq[SubstitutionData]): html.Div =
      div(
        `class` := "substitutionsWrapper",
        substitutions.map(createInteractionSpan(functionId, _)).toIndexedSeq
      ).render

    def createInteractionSpan(functionId: String, substitution: SubstitutionData): html.Element = {
      val interactiveVariableSpan = span(
        `class` := "TKAdjustableNumber",
        "1"
      ).render
      interactiveVariableSpan.setAttribute("data-var", IdManager.tangleVariableName(functionId, substitution.substitutedVariable))
      interactiveVariableSpan.setAttribute("data-min", substitution.variableMin.toString)
      interactiveVariableSpan.setAttribute("data-max", substitution.variableMax.toString)
      interactiveVariableSpan.setAttribute("data-step", substitution.variableStep.toString)
      interactiveVariableSpan.setAttribute("data-format", substitution.formatString)
      val tangleInterfaceHtml =
        div(
          `class` := "variableSubstitution",
          substitution.substitutedVariable,
          " = ",
          interactiveVariableSpan
        )
      tangleInterfaceHtml.render
    }

    def tangleSpan(variableId: String, formatString: String): html.Span = {
      val substitutedVarSpan = span().render
      substitutedVarSpan.setAttribute("data-var", variableId)
      substitutedVarSpan.setAttribute("data-format", formatString)
      substitutedVarSpan
    }


    def substituteSpansIntoFunctionText(functionText: String, substitutions: Seq[SubstitutionInFunction], substitutionsPerVariable: Seq[SubstitutionData]): html.Div = {
      val substitutionsOrderedByPos = substitutions.zipWithIndex.flatMap { case (substitution, variableIndex) =>
        substitution.substitutionPositions.map { position =>
          SortedPos(position, substitutionsPerVariable(variableIndex).substitutedVariable, substitutionsPerVariable(variableIndex).formatString)
        }
      }.sortBy(_.position.index)
      val substitutionSpans: Seq[html.Span] = substitutionsOrderedByPos.map(sub => tangleSpan(sub.position.variableId, sub.formatString))

      var splitFunctionText: Seq[String] = Seq(functionText)
      for ( data <- substitutionsPerVariable ) yield {
        val variableName = data.substitutedVariable
        val furtherSplitFunctionText = splitFunctionText.flatMap(_.split(variableName))
        splitFunctionText = furtherSplitFunctionText
      }

      val spanStrings = substitutionSpans.map(_.outerHTML) :+ ""
      val spanifiedFunction = splitFunctionText.zip(spanStrings).map{case (functionText, span) => {
      functionText + span
      }}.mkString("")
      val functionDiv = div().render
      functionDiv.innerHTML = spanifiedFunction
      functionDiv
    }

    def createFunctionDiv(functionId: String, functionHtmls: Seq[html.Element]): html.Element =
      div(
        `id` := functionId,
        `class` := Config.interactiveFunctionWrapper,
        functionHtmls.toIndexedSeq
      ).render

    def setupTangleInstance(buildIntoElement: html.Element, functionId: String, substitutionsPerVariable: Seq[SubstitutionData], subPosPerFunction: Seq[Seq[SubstitutionInFunction]], mathScope: js.Dynamic): js.Dynamic = {
      val getTangleVariableName = (substitution: SubstitutionData) => IdManager.tangleVariableName(functionId, substitution.substitutedVariable)

      val tangle = js.Dynamic.newInstance(js.Dynamic.global.Tangle)(buildIntoElement, js.Dynamic.literal(
        initialize = ((tangleInstance: js.Dynamic) => substitutionsPerVariable.map(data => {
          val sourceVariable = getTangleVariableName(data)
          tangleInstance.updateDynamic(sourceVariable)(1)
        })): js.ThisFunction,
        update = ((tangleInstance: js.Dynamic) => substitutionsPerVariable.zipWithIndex.foreach{case (data, i) =>
          val sourceVariable = getTangleVariableName(data)
          val updatedValue = tangleInstance.selectDynamic(sourceVariable)
          mathScope.updateDynamic(data.substitutedVariable)(updatedValue)
          subPosPerFunction.foreach(subPosInFunction => {
            subPosInFunction(i).substitutionPositions.foreach(position => {
              val reactingVar = position.variableId
              tangleInstance.updateDynamic(reactingVar)(updatedValue)
            })})
        }): js.ThisFunction)
      )
      tangle
    }

    val functionStrings: Seq[String] = fun.getElementsByTagName("code").map(_.innerText).toSeq

    val subPosPerFunction = functionStrings.map(functionText => {
      getSubstitutionsForFunction(functionText, substitutionsPerVariable)
    })
    val functionDivs: Seq[html.Div] = functionStrings.zipWithIndex.map{case (functionText, i) => {
      substituteSpansIntoFunctionText(functionText, subPosPerFunction(i), substitutionsPerVariable)
    }}

    val functionContainer = createFunctionDiv(functionId, functionDivs)

    val mathScope = js.Dynamic.literal( )
    val compiledExpression = functionStrings.map(js.Dynamic.global.math.compile(_))
    val colors = Vector("#3090FF", "#90FF30")
    var i = 0
    compiledExpression.foreach(expression => {
      addFunctionIntoMathbox(mathboxView, expression, mathScope, colors(i % colors.length))
      i += 1
    })

    val interactionRegion = createInteractionRegion(functionId, substitutionsPerVariable)
    functionContainer.appendChild(interactionRegion)
    val tangle = setupTangleInstance(functionContainer, functionId, substitutionsPerVariable, subPosPerFunction, mathScope)
    functionContainer
  }

  def setupFunctions(): ArrayBuffer[js.Dynamic] = {
    def createMathboxIntoElement(element: Element) = {
      val (mathbox, view) = MathboxSetup.createMathbox(element,
        dimensions = 2,
        range = js.Array(js.Array(-3, 3), js.Array(-10, 10)))
      (mathbox, view)
    }

    val functions = document.getElementsByTagName(Config.functionTag)
    val mathboxes: ArrayBuffer[js.Dynamic] = ArrayBuffer()
    functions.foreach ( fun => {

      val bindings = fun.getElementsByTagName(Config.bindingTag)
      val substitutionData: immutable.Seq[SubstitutionData] = bindings.map(getSubstitutionData(_)).toSeq
      val functionId = IdManager.newFunctionId

      val mathboxHtml = dom.document.createElement("mathbox").render
      val (mathBox, view) = createMathboxIntoElement(mathboxHtml)
      val functionDiv = setupTangleBinding(functionId, fun, substitutionData, view)
      mathboxes.append(view)
      fun.parentNode.appendChild(mathboxHtml)
      fun.parentNode.appendChild(functionDiv)
      fun.outerHTML = ""

    })
    mathboxes
  }
}
