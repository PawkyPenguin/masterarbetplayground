package visualEditor

import scala.collection.mutable.Stack
import scala.scalajs.js.annotation.JSExportTopLevel
import org.scalajs.dom
import org.scalajs.dom.html
import org.scalajs.dom.KeyboardEvent
import org.scalajs.dom.Event
import scalajs.js
import scalajs.js.Dynamic
import org.scalajs.dom.Node
import visualEditor.VisualEditor.EditorCommand
import collection.mutable.ListBuffer

class VisualEditor(container: html.Div) {
  import VisualEditorTest.myPrint
  private var document: String = ""
  /* Invariant: undoList(redoIndex), if defined, is the command that is executed when user requests the redo command.
   * [undoList(redoIndex-1), undoList(redoIndex-2), ..., undoList(0)] are commands that, when executed on `document`, take us back to "" (the empty string)
   * [undoList(redoIndex), undoList(redoIndex+1), undoList(undoList.length-1)] are redo commands that take the current `document` to the state it was when the redo stack was last empty
   */
  private val undoList: ListBuffer[EditorCommand] = ListBuffer()
  private var redoIndex = 0
  private var redoEnd = -1
  private val editorState = dom.document.getElementById("editorState")

  // Execute command on document while also keeping track of undo stack
  def executeCommand(command: EditorCommand) = {
    val (newDoc, undo) = command.execute(document)
    document = newDoc
    undoList.append(undo)
    redoIndex += 1
    redoEnd = redoIndex
  }
  
  def executeUndo() = {
    if (redoIndex > 0) {
      val (newDoc, redoCmd) = undoList(redoIndex-1).execute(document)
      document = newDoc
      undoList(redoIndex-1) = redoCmd
      redoIndex -= 1
    }
  }

  def executeRedo() = {
    if (redoIndex < redoEnd && redoIndex < undoList.length) {
      val (newDoc, undoCmd) = undoList(redoIndex).execute(document)
      undoList(redoIndex) = undoCmd
      redoIndex += 1
    }
  }

  container.asInstanceOf[Dynamic].onbeforeinput = (ev: Event) => {
    //ev.preventDefault()
    val e = ev.asInstanceOf[Dynamic]
    val selection: dom.Selection = dom.window.getSelection()
    var i = 0
    dom.console.log(e)
    while (i < selection.rangeCount) {
      val range = selection.getRangeAt(i)
      matchInputEvent(e.inputType.asInstanceOf[String], e.data.asInstanceOf[String], range, e)
      i+= 1
    }
    
    dom.console.log(selection)

    editorState.asInstanceOf[html.TextArea].value = document
  }

  def matchInputEvent(inputEventType: String, input: String, range: dom.Range, event: Dynamic) = {
    val ancestorNode: Node = range.commonAncestorContainer
    inputEventType match {
      case "insertText" =>
        if (container.contains(ancestorNode)) { // TODO: is this condition guaranteed to be true?
          println("range within div!")
          executeCommand(VisualEditor.ReplaceRange(input, range.startOffset, range.endOffset)) // ReplaceRange also works if the range is empty!
        }
      case "deleteContentForward" =>
        if (range.startOffset == range.endOffset) {
          println("del for1 ")
          executeCommand(VisualEditor.DeleteOne(range.startOffset))
        } else {
          println("del for2")
          executeCommand(VisualEditor.DeleteRange(range.startOffset, range.endOffset))
        }
      case "deleteContentBackward" =>
        if (range.collapsed) {
          println("del back1")
          executeCommand(VisualEditor.BackspaceOne(range.startOffset))
        } else {
          executeCommand(VisualEditor.DeleteRange(range.startOffset, range.endOffset))
          println("del back2")
        }
      case "historyUndo" => executeUndo()
      case "historyRedo" => executeRedo()
      case "insertCompositionText" =>
        if (!event.isComposing.asInstanceOf[Boolean]) {
          executeCommand(VisualEditor.ReplaceRange(event.data.asInstanceOf[String], range.startOffset, range.endOffset))
        }
      case "insertParagraph" =>
          executeCommand(VisualEditor.ReplaceRange("\n", range.startOffset, range.endOffset))
      case "insertFromPaste" => ???
      case "insertFromDrop" => ???
    }
  }
}


object VisualEditor {
  private def doInsert(doc: String, text: String, position: Int): String = {
    println(s"insert `$text` into `$doc` at `$position`")
    val (headBit, tailBit) = doc.splitAt(position)
    headBit + text + tailBit
  }

  sealed trait EditorCommand {
    def execute(doc: String): (String, EditorCommand)
    def applyTo(documentState: (String, List[EditorCommand])) = {
      val (newDoc, undo) = execute(documentState._1)
      (newDoc, documentState._2 :+ undo)
    }
  }

  final case class Compose(command1: EditorCommand, command2: EditorCommand) extends EditorCommand {
    def execute(doc: String): (String, EditorCommand) = {
      val (doc2, undo1) = command1.execute(doc)
      val (doc3, undo2) = command2.execute(doc2)
      (doc3, Compose(undo2, undo1))
    }
  }

  final case class Insert(text: String, position: Int) extends EditorCommand {
    override def execute(doc: String): (String, EditorCommand) = (doInsert(doc, text, position), DeleteRange(position, position + text.length))
  }

  final case class DeleteRange(from: Int, to: Int) extends EditorCommand {
    override def execute(doc: String): (String, EditorCommand) = {
      val deletedPart = doc.slice(from, to)
      val undo: EditorCommand = Insert(deletedPart, from)
      (doc.slice(0, from) + doc.slice(to, doc.length), undo)
    }
  }

  final case class NoOp() extends EditorCommand {
    def execute(doc: String): (String, EditorCommand) = (doc, NoOp())
  }

  final case class DeleteOne(position: Int) extends EditorCommand {
    override def execute(doc: String): (String, EditorCommand) = DeleteRange(position, position+1).execute(doc)
  }
  final case class BackspaceOne(position: Int) extends EditorCommand {
    override def execute(doc: String): (String, EditorCommand) = DeleteRange(position-1, position).execute(doc)
  }
  final case class ReplaceRange(text: String, from: Int, to: Int) extends EditorCommand {
    override def execute(doc: String): (String, EditorCommand) = {
      Compose(
        DeleteRange(from, to),
        Insert(text, from)
      ).execute(doc)
    }
  }

  // TODO: Undo collapsing: whenever text is inerted or deleted, must use this as undo block. Good separation: Spaces/newlines between words. Also ctrl-v.
  // Might want to define a method "collapse" on all Editor commands that will join two commands into one if possible (return Seq[EditorCommand], similar to a flatten).
  // Only execute collapse on an undo/redo keypress, otherwise it takes too long tbh.
}

//@JSExportTopLevel("EditorTest")
object VisualEditorTest {
  val log = dom.document.getElementById("log").asInstanceOf[html.Div]
  def myPrint(s: String) = {
    val p = dom.document.createElement("p").asInstanceOf[html.Paragraph]
    p.innerText = s
    p.style.margin = "0"
    log.prepend(p)
  }

  @JSExportTopLevel(name = "main", moduleID = "visualEditor")
  def main() = {
    import VisualEditor._

    val editorComponent = dom.document.getElementById("editor").asInstanceOf[html.Div]
    val myEditor = new VisualEditor(editorComponent)

    dom.document.onselectionchange = _ => {
      val selection = dom.window.getSelection()
      if (selection.rangeCount > 0) {
        val range = selection.getRangeAt(0)
        myPrint(s"Start: ${range.startContainer}, End: ${range.endContainer}")
        myPrint(s"Start: ${range.startOffset}, End: ${range.endOffset}")
      }
    }

    test()
  }

  def test() = {
    import VisualEditor._
    val startDoc = ""
    val commands = Seq(
      VisualEditor.Insert("hello", 0),
      VisualEditor.DeleteRange(1, 3),
      VisualEditor.Insert(" world", 3),
      VisualEditor.BackspaceOne(4)
    )
    val (resultingDoc: String, undoStack: List[EditorCommand]) = commands.foldLeft((startDoc, List[EditorCommand]()))((t, c) => {
      val (newDoc, undo) = c.execute(t._1)
      (newDoc, t._2 :+ undo)
    })
    println(resultingDoc)
    println(undoStack)
    val (undoneDocument, _) = undoStack.foldRight((resultingDoc, List[EditorCommand]()))((command, docState) => command.applyTo(docState))
    println(s"Undone document: `$undoneDocument`")
  }
}