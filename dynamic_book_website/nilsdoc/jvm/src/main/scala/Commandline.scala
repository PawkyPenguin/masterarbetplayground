import lineparse.LineParser
import os._
import ast._
import scala.collection.mutable.ArrayBuffer

object Commandline {
    def main(args: Array[String]): Unit = {
        val wd = os.pwd
        if (args.length == 1) {
            val fileToCompile: os.Path = wd / os.RelPath(args.head)
            val lines: Vector[String] = os.read.lines(fileToCompile).toVector
            val ast = ASTBuilder.buildAst(lines)
            val html = AstToHtml.translate(ast)(using ())
            println(html)
        } else {
            println("Give a file as input!")
        }
    }
}
