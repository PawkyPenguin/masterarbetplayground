import app._
import org.scalatra._
import javax.servlet.ServletContext
import com.mchange.v2.c3p0.ComboPooledDataSource
import org.slf4j.LoggerFactory
import slick.jdbc.SQLiteProfile.api._

class ScalatraBootstrap extends LifeCycle {

  // Database setup code largely taken from https://scalatra.org/guides/2.8/persistence/slick.html
  val logger = LoggerFactory.getLogger(getClass)
  val cpds = new ComboPooledDataSource
  logger.info("Created c3p0 connection pool")

  override def init(context: ServletContext) = {
    val db = Database.forDataSource(cpds, Some(50))

    context.mount(new MyScalatraServlet(db), "/*")
  }

  override def destroy(context: ServletContext): Unit = {
    super.destroy(context)
    logger.info("Closing c3p0 connection pool")
    cpds.close()
  }
}
