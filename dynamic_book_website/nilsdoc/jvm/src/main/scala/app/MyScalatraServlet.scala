package app

import org.scalatra._
import globals.Config.ViewButtons.documentTitle
import slick.jdbc.SQLiteProfile.api.Database
import app.HtmlPages.userProfile
import _root_.api.DataStructures._
import slick.lifted.ProvenShape
import scalatags.Text.StringFrag
import api.TitleValidator
import api.Routes
import api.Routes.urlDecode
import api.Routes.urlEncode
import scala.concurrent.Future
import scala.util.Try
import scala.util.Failure
import api.DataStructures
import scala.collection.mutable.ArrayBuffer

object Tables {
  import slick.jdbc.SQLiteProfile.api._

  class Users(tag: Tag) extends Table[User](tag, "USERS") {
    def id = column[Int]("ID", O.PrimaryKey, O.AutoInc)
    def username = column[String]("USERNAME")
    def password = column[String]("PASSWORD")
    def `*` : ProvenShape[User] = (id.?, username, password).mapTo[User]
  }
  val users = TableQuery[Users]

  class Documents(tag: Tag) extends Table[Document](tag, "DOCUMENTS") {
    def id = column[Int]("DOC_ID", O.PrimaryKey, O.AutoInc)
    def title = column[String]("TITLE")
    def versionNumber = column[Long]("VERSION_NUM")
    def authorId = column[Int]("USER_ID")
    def content = column[String]("CONTENT")

    def author = foreignKey("USER_FK", authorId, users)(_.id)
    def * = (id.?, authorId, versionNumber, title, content).mapTo[Document]
  }
  val documents = TableQuery[Documents]

  case class School(id: Option[Int], name: String)
  class Schools(tag: Tag) extends Table[School](tag, "SCHOOLS") {
    def id = column[Int]("ID", O.PrimaryKey, O.AutoInc)
    def name = column[String]("NAME")
    def * = (id.?, name).mapTo[School]
  }
  val schools = TableQuery[Schools]

  case class HasReadPermission(userId: Int, documentId: Int)
  class HasReadPermissions(tag: Tag) extends Table[HasReadPermission](tag, "HAS_READ_PERMISSIONS") {
    def userId = column[Int]("USER_ID")
    def documentId = column[Int]("DOCUMENT_ID")

    def pk = primaryKey("PK_READ_PERMISSIONS", (userId, documentId))
    def fkReader = foreignKey("FK_USERNAME", userId, users)
    def * = (userId, documentId).mapTo[HasReadPermission]
  }
  val hasReadPermissions = TableQuery[HasReadPermissions]

  case class TeachesInSchool(teacherId: Int, schoolId: Int)
  class TeachesInSchools(tag: Tag) extends Table[TeachesInSchool](tag, "TEACHES_IN_SCHOOLS") {
    def teacherId = column[Int]("TEACHER_ID")
    def schoolId = column[Int]("SCHOOL_ID")
    def * = (teacherId, schoolId).mapTo[TeachesInSchool]
    def referencedTeacher = foreignKey("TEACHER_FK", teacherId, users)
    def referencedSchool = foreignKey("SCHOOL_FK", schoolId, schools)
    def pk = primaryKey("PK_TEACHES", (teacherId, schoolId))
  }
  val teachesInSchool = TableQuery[TeachesInSchools]

  case class Student(userId: Int, studentName: String, birthday: String, mailaddress: String, schoolId: Int)
  class Students(tag: Tag) extends Table[Student](tag, "STUDENTS") { 
    def userId = column[Int]("USER_ID", O.PrimaryKey)
    def studentName = column[String]("STUDENT_NAME")
    def birthday = column[String]("BIRTHDAY")
    def mailaddress = column[String]("MAIL_ADDRESS")
    def school = column[Int]("SCHOOL_ID")

    def * = (userId, studentName, birthday, mailaddress, school).mapTo[Student]
    def referencedUser = foreignKey("USER_FK", userId, users)
    def referencedSchool = foreignKey("SCHOOL_FK", school, schools)
  }
  val students = TableQuery[Students]

  case class Teacher(userId: Int, teacherName: String, mailaddress: String)
  class Teachers(tag: Tag) extends Table[Teacher](tag, "TEACHERS") {
    def userId = column[Int]("USER_ID", O.PrimaryKey)
    def teacherName = column[String]("TEACHER_NAME")
    def mailaddress = column[String]("MAIL_ADDRESS")

    def * = (userId, teacherName, mailaddress).mapTo[Teacher]
    def referencedUser = foreignKey("USER_FK", userId, users)
  }
  val teachers = TableQuery[Teachers]

  case class NilsdocObjectWithDocumentId(userId: Int, documentId: Int, name: String, pickledContent: String, objectType: String)
  class NilsdocObjects(tag: Tag) extends Table[NilsdocObjectWithDocumentId](tag, "NILSDOC_OBJECTS") {
    def user = column[Int]("USER_ID")
    def documentId = column[Int]("DOCUMENT")
    def name = column[String]("NAME")
    def pickledContent = column[String]("CONTENT")
    def objectType = column[String]("HTML_CLASS")


    def pk = primaryKey("PK_NILSDOC_OBJECTS", (documentId, name, user))
    def fkUser = foreignKey("USER_FK", user, users)
    def document = foreignKey("DOCUMENT_FK", documentId, documents)(_.id)

    def * = (user, documentId, name, pickledContent, objectType) <> (
      (u, d, n, c, o) => NilsdocObjectWithDocumentId(u, d, n, c, o), 
      n => (n.userId, n.documentId, n.name, n.pickledContent, n.objectType)
    )
  }
  val nilsdocObjects = TableQuery[NilsdocObjects]

  val instantiateTables = (users.schema ++ documents.schema ++ nilsdocObjects.schema ++ teachers.schema ++ schools.schema ++ students.schema ++ teachesInSchool.schema ++ hasReadPermissions.schema).createIfNotExists
  val dropItBobby = (users.schema ++ documents.schema ++ nilsdocObjects.schema ++ teachers.schema ++ schools.schema ++ students.schema ++ teachesInSchool.schema ++ hasReadPermissions.schema).dropIfExists

  val populateTables = DBIO.seq(
    users += User(None, "teacher", "password"),
    users += User(None, "student", "password"),
    documents += Document(None, 1, 0, "Test Document", "My doc\n {tree myTree}"),
    schools += School(None, ""),
    teachers += Teacher(1, "Franz Friedlich", "b@b.b"),
    students += Student(2, "Max Muster", "01.01.2023", "a@a.a", 1),
    teachesInSchool += TeachesInSchool(1, 1),
    hasReadPermissions += HasReadPermission(2, 1)
  )
}

class MyScalatraServlet(val db: Database) extends ScalatraServlet with FutureSupport {
  import slick.jdbc.SQLiteProfile.api._
  import Tables._

  db.run(DBIO.seq(Tables.dropItBobby, Tables.instantiateTables)).flatMap(_ => db.run(populateTables))

  override protected implicit def executor = scala.concurrent.ExecutionContext.Implicits.global

  val storageDir = os.pwd / "nilsdoc" / "webappStorage"

  extension (n: Tables.NilsdocObjectWithDocumentId) {
    def toNilsobject(): Future[Nilsobject] =
      db.run(Tables.documents.filter(_.id === n.documentId).result).map(d =>
        val document = d.head
        Nilsobject(n.userId, document.authorId, document.title, n.name, n.pickledContent, n.objectType)
      )
  }

  extension (n: Nilsobject) {
    def toDBObject(): Future[Tables.NilsdocObjectWithDocumentId] =
      db.run(Tables.documents.filter(_.authorId === n.documentAuthor).filter(_.title === n.documentTitle).result).map(document =>
        val docId = document.head.id.get
        Tables.NilsdocObjectWithDocumentId(n.userId, docId, n.name, n.pickledContent, n.objectType)
      )
  }



  get("/users/?") {
    contentType = "text/html"
    db.run(Tables.users.result)
      .map(users => {
        HtmlPages.listStrings(users.map(_.username))
      })
  }

  get("/users/:userId/?") {
    contentType = "text/html"
    val userId = params("userId").toInt
    db.run(Tables.users.filter(_.id === userId).result)
      .map(users =>
        users.headOption match
          case None => NotFound("No such user.")
          case Some(user) =>
            db.run(queryAllDocuments(user.id.get).result).map(documents => userProfile(users.head, documents))
      )
  }

  get("/users/:userId/documents") {
    contentType = "text/html"
    val userId = params("userId").toInt
    //db.run(query.result).map(users => s"Executed! Found ${users.length}")
    val r = for {
      documents <- db.run(queryAllDocuments(userId).result)
      user <- db.run(queryUser(userId).result)
    } yield (documents, user)
    r.map((documents, users) =>
      val user = users.head
      HtmlPages.documentList(documents, user)
    )
  }

  val queryUser = Compiled((userId: Rep[Int]) => Tables.users.filter(_.id === userId))
  val queryAllDocuments = Compiled((userId: Rep[Int]) => Tables.documents.filter(_.authorId === userId))
  val queryDocument = Compiled((userId: Rep[Int], documentTitle: Rep[String]) => Tables.documents.filter(_.title === documentTitle).filter(_.authorId === userId))
  val queryAllNilsobjects = Compiled((userId: Rep[Int], documentTitle: Rep[String]) =>
    for {
      document <- Tables.documents.filter(_.authorId === userId)
      nilsdoc <- Tables.nilsdocObjects.filter(_.documentId === document.id).filter(_.user === userId)
    } yield nilsdoc
  )
  val queryReadPermissions = Compiled((userId: Rep[Int], documentId: Rep[Int]) => Tables.hasReadPermissions.filter(_.userId === userId).filter(_.documentId === documentId))

  get("/users/:userId/documents/:docId/notes") {
    contentType = "text/html"
    val userId = params("userId").toInt
    val documentTitle = Routes.urlDecode(params("docId").asInstanceOf[Routes.UrlEncodedString])
    db.run(queryDocument(userId, documentTitle).result).flatMap(documents =>
      documents.headOption match {
        case Some(doc) => 
          db.run(queryAllNilsobjects(userId, documentTitle).result).map(storedObjects =>
            HtmlPages.personalNoteListing(doc, storedObjects)
          )
        case None => Future(NotFound(s"No such document: $documentTitle"))
      }
    )
  }

  // take care the two last url parts match with /documents/docId, because I split the URL string in the client (NilsdocWorkstation)
  get("/users/:userId/newDocument/empty") {
    contentType = "text/html"
    val userId = params("userId").toInt
    HtmlPages.workstation(Document(None, userId, 0, "", ""),  Seq())
  }

  def queryStudents(teacherId: Rep[Int]) = for {
        teachEntry <- Tables.teachesInSchool if teachEntry.teacherId === teacherId
        school <- Tables.schools if teachEntry.schoolId === school.id
        student <- Tables.students if student.school === school.id
        user <- Tables.users if user.id === student.userId
      } yield user

  val queryStudentsWithReadPermissions = Compiled((teacherId: Rep[Int], docId: Rep[Int]) =>
    queryStudents(teacherId)
      .joinLeft(Tables.hasReadPermissions)
      .on((user, permission) => user.id === permission.userId && permission.documentId === docId)
      .map((user, maybePermission) => (user, maybePermission.nonEmpty))
  )

  get("/users/:username/documents/:docId/publish") {
    contentType = "text/html"
    val authorId = params("username").toInt
    val documentTitle = Routes.urlDecode(params("docId").asInstanceOf[Routes.UrlEncodedString])
    db.run(queryDocument(authorId, documentTitle).result).flatMap(documents =>
      documents.headOption match {
        case Some(doc) =>
            db.run(Tables.users.filter(_.id === authorId).result).flatMap(user =>
              db.run(queryStudentsWithReadPermissions(authorId, doc.id.get).result).map(allResults =>
                val users = allResults.map(studentWithPermission =>
                  println(studentWithPermission)
                  HtmlPages.ReaderWithReadPermission(studentWithPermission._1, studentWithPermission._2)
                )
                HtmlPages.publishView(doc, user.head, Seq(), users)
              )
            )
        case None => Future(NotFound(s"No such document: $documentTitle"))
      }
    )
  }

  put(Routes.makePublishUrl(":userId", ":title")) {
    val permissions = upickle.default.read[NewReadPermissions](ujson.read(request.body))
    val userId = params("userId").toInt
    val docTitle = params("title")
    db.run(queryDocument(userId, docTitle).result).foreach(documents =>
      documents.headOption match {
        case None => BadRequest(s"No such document $docTitle")
        case Some(doc) =>
          val dbPermissions: Seq[HasReadPermission] = permissions.readerIds.map(HasReadPermission(_, doc.id.get))
          db.run(
            DBIO.seq(
              (Tables.hasReadPermissions.filter(_.documentId === doc.id).delete +:
              dbPermissions.map(
                Tables.hasReadPermissions.insertOrUpdate(_)
              ))*
            )
          ).map(_ => Ok())
      }
    )

  }

  get("/users/:username/documents/:docId") {
    contentType = "text/html"
    val userId = params("username").toInt
    val documentTitle = Routes.urlDecode(params("docId").asInstanceOf[Routes.UrlEncodedString])
    db.run(documents.filter(_.title === documentTitle).result)
      .flatMap(documents =>
        documents.headOption match {
          case Some(doc) =>
            if (doc.authorId == userId) {
              // serve full webpage
              db.run(queryAllNilsobjects(userId, documentTitle).result).map(storedObjects =>
                HtmlPages.workstation(doc, storedObjects)
              )
            } else {
              db.run(hasReadPermissions.result).flatMap(readPermissions =>
                if (readPermissions.nonEmpty) {
                  // serve read-only version
                  val authorAndStudentObjectsQuery = nilsdocObjects.filter(obj => obj.documentId === doc.id &&
                    (obj.user =!= doc.authorId || (obj.user === doc.authorId &&
                      !nilsdocObjects.filter(o2 => o2.documentId === doc.id && o2.user =!= doc.authorId && o2.name === obj.name).exists)))
                  db.run(authorAndStudentObjectsQuery.result).map(storedObjects =>
                    HtmlPages.studentView(doc, storedObjects)
                  )
                } else {
                  Future(Unauthorized("No read permissions for this user."))
                }
              )
            }
          case None => Future(NotFound(s"No such document: $documentTitle"))
        }
      )
  }

  get("/myEditorTest/?") {
    views.html.myEditorTest()
  }

  post(api.Routes.nilsObjectPostOrDeleteUrl()) {
    import upickle.default._
    val nilsObject = read[Nilsobject](ujson.read(request.body))
    db.run(queryDocument(nilsObject.documentAuthor, nilsObject.documentTitle).result).flatMap(docs =>
      docs.headOption match {
        case None => Future(BadRequest(s"No such document: ${nilsObject.documentTitle}"))
        case Some(_) =>
          nilsObject.toDBObject().flatMap(dbNilsobj =>
            db.run(nilsdocObjects.insertOrUpdate(dbNilsobj)).map(result =>
              Ok(s"Inserted ${nilsObject}")
            )
          )
      }
    )
  }

  delete(api.Routes.nilsObjectPostOrDeleteUrl()) {
    import upickle.default._
    val nilsObject = read[Nilsobject](request.body)
    nilsObject.toDBObject().flatMap(dbNilsobj =>
      db.run(Tables.nilsdocObjects.filter(_.documentId === dbNilsobj.documentId).filter(_.user === dbNilsobj.userId).delete)
    )
  }

  put(api.Routes.nilsdocPutUrl()) {
    import upickle.default._
    // TODO: authorization, obviously
    // TODO: error handling (what if no such user)
    val document = read[Document](request.body)
    if (TitleValidator.isValid(document.title)) {
      db.run(queryDocument(document.authorId, document.title).result).flatMap(d =>
        d.headOption match {
          case None => db.run(documents += document).map(_ => Created())
          case Some(docInDb) =>
            val updatedDocument = Document(docInDb.id, document.authorId, document.versionNumber, document.title, document.content)
            db.run(Tables.documents.update(updatedDocument)).map(_ => Ok())
        }
      )
    } else {
      Future(BadRequest(s"Invalid title: `$document`"))
    }
  }
}
