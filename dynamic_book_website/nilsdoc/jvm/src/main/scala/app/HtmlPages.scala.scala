package app
import scalatags.Text.all._
import scalatags.Text.TypedTag
import api.DataStructures._
import globals.Config.DynamicComponentHtmlClasses
import api.DataStructures
import globals.Config
import globals.Config.DynamicComponentHtmlClasses.storageAreaId
import app.Tables.nilsdocObjects
import api.DataStructures._
import api.DataStructures
import api.Nilsobjects._
import scalatags.generic.StylePair
import app.Tables.Student
import app.Tables.NilsdocObjectWithDocumentId

object HtmlPages {
  val commonStyles =
    Seq(
      "/src/bootstrap/dist/css/bootstrap.min.css",
      "/editor.css",
      "/src/fontawesome-free-6.4.2/css/all.min.css",
      "/src/katex/katex.css"
    ).map(src => link(rel := "stylesheet", `type` := "text/css", href := src))

  def base(content: TypedTag[String]*) = {
    doctype("html")(
      html(
        head(
          commonStyles
        ),
        body(
          content
        )
      )
    ).render
  }

  def userProfile(user: User, documents: Seq[Document]): String = base(
    div(
      `class` := "container",
      h1(user.username),
      ul(
        documents.map(d => li(documentLink(user.id.get, d)))
      )
    )
  )

  def documentLink(userId: Int, document: Document) = a(
    href := s"/users/$userId/documents/${api.Routes.urlEncode(document.title)}",
    document.title
  )

  def documentList(documents: Seq[Document], user: User): String = base(
    div(
      `class` := "container",
      h1(
        `class` := "display-1",
        "Welcome back, ",
        user.username,
        "!"
      ),
      hr(
        `class` := "border border-primary border-3 opacity-75"
      ),
      if (documents.nonEmpty) {
        div(
          h2(`class` := "pt-5 pb-3", "My Documents"),
          ul(
            documents.map(d => li(documentLink(user.id.get, d)))
          ),
          // button(
          //  `type` := "button",
          a(
            `class` := "btn btn-outline-primary m-2",
            href := api.Routes.emptyDoc(),
            "Create New Document"
          )
          // )
        )
      } else {
        p(
          "No documents currently. Create a new one ",
          a(
            href := api.Routes.emptyDoc(),
            "here."
          )
        )

      }
    )
  )

  private def listView(liElements: Seq[TypedTag[String]]): String =
    base(
      ul(
        liElements
      )
    )

  def listStrings(elements: Seq[String]): String = listView(elements.map(li(_)))

  def personalNoteListing(nilsdoc: Document, notes: Seq[Tables.NilsdocObjectWithDocumentId]): String = {
    doctype("html")(
      html(
        head(
          commonStyles,
        ),
        body(
          div(
            `class` := "flex-container",
            div(
              `class` := "mycontainer container",
              `id` := Config.PersonalNotes.htmlContainerId
            ),
            div(
              `class` := "mycontainer container",
              `id` := Config.PersonalNotes.noteContainerId
            )
          ),
          storageArea(notes),
          nilsdocMetadata(nilsdoc),
          script(`type` := "module", src := "/dist/personalNotes.js"),
        )
      )
    ).render

  }

  // Create the Html for a nilsobject storage element
  def makeStorageHtmlForNilsobj(n: NilsdocObjectWithDocumentId): TypedTag[String] = // impementation for serverside calls
    script(
      id := DynamicComponentHtmlClasses.makeNilsobjStorageId(n.name),
      `type` := "application/json",
      `class` := Seq(DynamicComponentHtmlClasses.ndNilsobjStorage, DataStructures.getNilsobjStorageClassFromType(n.objectType)).mkString(" "),
      data(Config.DynamicComponentHtmlClasses.nilsobjTypeData) := n.objectType,
      data(Config.DynamicComponentHtmlClasses.nilsobjNameData) := n.name,
      raw(n.pickledContent)
    )

  def storageArea(storedObjects: Seq[Tables.NilsdocObjectWithDocumentId]) =
    div(
      id := DynamicComponentHtmlClasses.storageAreaId,
      storedObjects.map(obj => makeStorageHtmlForNilsobj(obj)),
    )

  def printButton() = iconButton(
    Config.Printing.printIconId,
    Seq(),
    "fa-print",
  )(
    onclick := "window.print()"
  )

  def iconButton(htmlId: String, additionalClasses: Seq[String], icon: String) =
    div( id := htmlId,
      `class` := "btn btn-outline-primary " + additionalClasses.mkString(" "),
      i(`class` := s"fa-solid $icon")
    )

  def printView2(nilsdoc: Document, storedObjects: Seq[Nilsobject]): String = {
    // TODO: unused
    doctype("html")(
      html(
        head(
          Seq(
            "/src/bootstrap/dist/css/bootstrap.min.css",
            "/src/fontawesome-free-6.4.2/css/all.min.css",
            "/src/katex/katex.css",
            "/src/TangleKit/TangleKit.css",
            "/src/mathbox/build/mathbox.css"
          ).map(src => link(rel := "stylesheet", `type` := "text/css", href := src)),
          meta(name := "viewport", content := "width=device-width, initial-scale=1"),
          link(id := Config.Printing.stylesheetId, rel := "stylesheet", media := "print", `type` := "text/css", href := "/print.css"),
          link(rel := "stylesheet", media := "screen", `type` := "text/css", href := "/print.css"),
        ),
        body(
          `class` := s"${Config.isPrintingCssClass} ${Config.isPrintingCssClass}",
          div(
            margin := "auto",
            width := "100%",
            div(
              `class` := s"container ${Config.buttonRowClass}",
              button(id := Config.SummaryPrompt.summaryViewButtonId, `type` := "button", `class` := "hidden btn btn-outline-primary m-2", "Toggle my summary"),
              button(id := Config.Printing.togglePrintButtonId, `type` := "button", `class` := "btn btn-outline-primary m-2", "Return to Editor"),
              printButton()
            ),
            div(id := Config.BookView.previewId),
          ),
          script(id := Config.titleStorageAreaId, `type` := "application/json", raw(upickle.default.write(nilsdoc))),
          script(
            src := "https://unpkg.com/pagedjs/dist/paged.polyfill.js"
          )
        )
      )
    ).render
  }

  def studentView(document: Document, storedObjects: Seq[NilsdocObjectWithDocumentId]): String = {
    doctype("html")(
      html(
        head(
          commonStyles,
          link(rel := "stylesheet", `type` := "text/css", href := "/src/bootstrap-toc/bootstrap-toc.min.css"),
          link(rel := "stylesheet", href := "/src/TangleKit/TangleKit.css", `type` := "text/css"),
          link(id := Config.Printing.stylesheetId, rel := "stylesheet", media := "print", `type` := "text/css", href := "/print.css"),
          link(rel := "stylesheet", href := "/src/mathbox/build/mathbox.css"),
          meta(name := "viewport", content := "width=device-width, initial-scale=1"),
          tag("title")(document.title)
        ),
        body(
          `class` := Config.isReaderCssClass,
          div(
            `class` := "flex-container",
            div(
              id := Config.PersonalNotes.studentViewNoteContainerId,
            ),
            div(
              margin := "auto",
              width := "100%",
              div(
                `class` := s"container ${Config.buttonRowClass}",
                button(id := Config.SummaryPrompt.summaryViewButtonId, `type` := "button", `class` := "btn btn-outline-primary m-2", "Toggle my summary"),
              ),
              div(
                `class` := s"container",
                nilsdocRenderArea(false)
              ),
              div(
                id := Config.Printing.printOutputDivId,
                `class` := "invisible"
              ),
            ),
            div(
              margin := "auto" // for centering the view area
            )

          ),
          storageArea(storedObjects),
          nilsdocMetadata(document),
          script(`type` := "module", src := "/dist/studentView.js"),
          script(`type` := "text/javascript", src := "/src/konva/konva.min.js"),
          script(`type` := "text/javascript", src := "/src/katex/katex.min.js"),
          script(`type` := "text/javascript", src := "https://cdnjs.cloudflare.com/ajax/libs/mathjs/7.5.1/math.min.js"),
          script(`type` := "text/javascript", src := "/src/@popperjs/core/dist/umd/popper.min.js"),
        )
      )
    ).render
  }

  def nilsdocRenderArea(withAuthorStyles: Boolean) = 
    div(
      `class` := "mycontainer container",
      id := Config.nilsdocRenderArea,
      if (withAuthorStyles) {
        Seq(
          overflowY := "scroll",
          height := "90vh",
        )
      } else {
        ()
      },
      div(id := Config.Ref.refHoverContainerId, `class` := "invisible"),
      div(
        `class` := "row",
        div(`class` := "col-9",
          div(
            id := Config.BookView.previewId, 
          )
        ),
        div(
          `class` := "col-3 thin-left-border",
          div(
            id := "right-river",
            `class` := "sticky-top",
            tag("nav")(id := Config.BookView.tocId, data("toggle") := "toc", `class` := "sticky-top navbar-light bg-light flex-column", attr("aria-orientation") := "vertical"),
            div(id := Config.PinContent.riverId)
          )
        )
      ),
      textarea(id := Config.TreeGraph.treeNodeTextInput, `class` := "invisible"),
      div(id := Config.TreeGraph.rootDivId),
      div(id := Config.SummaryPrompt.summaryViewContainerId, `class` := "invisible"),
      iconButton(Config.PersonalNotes.noteButtonId, Seq("invisible"), "fa-plus")
    )

  case class ReaderWithReadPermission(reader: User, hasReadPermission: Boolean)
  def publishView(nilsdoc: Document, author: User, students: Seq[Student], otherPeopleInSchool: Seq[ReaderWithReadPermission]): String = {
    doctype("html")(
      html(
        head(
          link(rel := "stylesheet", `type` := "text/css", href := "/src/bootstrap/dist/css/bootstrap.min.css"),
          link(rel := "stylesheet", `type` := "text/css", href := "/publishView.css"),
          link(rel := "stylesheet", href := "/src/fontawesome-free-6.4.2/css/all.min.css"),
          meta(name := "viewport", content := "width=device-width, initial-scale=1"),
          tag("title")(s"Publish '${nilsdoc.title}'")
        ),
        body(
          div(
            `class` := "container",
            h1(`class` := "border-bottom pb-2 mb-3", s"Publish Document '${nilsdoc.title}'"),
            //h2("My students"),
            //form(
            //  students.map(student =>
            //    div(
            //      `class` := "form-check",
            //      input(id := student.username, `type` := "checkbox", `class` := "form-check-input"),
            //      label(`class` := "form-check-label", `for` := student.username, s"${student.username} (${student.mailaddress})")
            //    )
            //  ),
            //),
            div(
              `class` := "d-flex",
              span(
                margin := "auto"
              ),
              span(
                id := Config.PublishView.serverRequestErrorMsgId,
                `class` := "m-2 border border-danger invisible"
              ),
              i(
                id := Config.PublishView.loadSpinnerId,
                `class` := "fa-solid fa-spinner m-2"
              ),
              button(
                id := Config.PublishView.publishForRealButtonId,
                `class` := "btn btn-success m-2",
                getPublishForReaderButtonMessage(otherPeopleInSchool.filter(_.hasReadPermission).length)
              )
            ),
            h2("Other People In This School"),
            input(id := Config.PublishView.searchFieldId, placeholder := "Search for a person..."),
            otherPeopleInSchool.map(person =>
              div(
                `class` := "form-check",
                {
                  val checkbox = input(
                    id := person.reader.username,
                    data(Config.PublishView.userIdData) := person.reader.id.get,
                    `type` := "checkbox",
                    `class` := s"form-check-input ${Config.PublishView.checkboxClass}",
                    autocomplete := "off"
                  )
                  if (person.hasReadPermission) {
                    checkbox(checked)
                  } else {
                    checkbox
                  }
                },
                label(`class` := "form-check-label", `for` := person.reader.username,
                  person.reader.username
                )
              )
            )
          ),
          nilsdocMetadata(nilsdoc),
          script(`type` := "module", src := "/dist/publishView.js"),
        )
      )
    ).render
  }

  def nilsdocMetadata(doc: Document) = script(`type` := "application/json", id := Config.titleStorageAreaId, raw(upickle.default.write(doc)))

  def workstation(nilsdoc: Document, storedObjects: Seq[NilsdocObjectWithDocumentId]): String = {
    doctype("html")(
      html(
        head(
          link(rel := "stylesheet", `type` := "text/css", href := "/src/bootstrap/dist/css/bootstrap.min.css"),
          link(rel := "stylesheet", `type` := "text/css", href := "/src/bootstrap-toc/bootstrap-toc.min.css"),
          link(rel := "stylesheet", `type` := "text/css", href := "/editor.css"),
          link(id := Config.Printing.stylesheetId, rel := "stylesheet", media := "print", `type` := "text/css", href := "/print.css"),
          link(rel := "stylesheet", href := "/src/TangleKit/TangleKit.css", `type` := "text/css"),
          link(rel := "stylesheet", href := "/src/mathbox/build/mathbox.css"),
          link(rel := "stylesheet", href := "/src/katex/katex.css"),
          link(rel := "stylesheet", href := "/src/prosemirror-menu/style/menu.css"),
          link(rel := "stylesheet", href := "/src/prosemirror-gapcursor/style/gapcursor.css"),
          link(rel := "stylesheet", href := "/src/prosemirror-view/style/prosemirror.css"),
          link(rel := "stylesheet", href := "/src/prosemirror-example-setup/style/style.css"),
          link(rel := "stylesheet", href := "/src/fontawesome-free-6.4.2/css/all.min.css"),
          meta(name := "viewport", content := "width=device-width, initial-scale=1"),
          tag("title")("Dynamic Book Editor")
        ),
        body(
          div(
            `class` := s"flex-container ${Config.buttonRowClass} ${Config.Printing.dontPrint}",
            button(id := Config.ViewButtons.showEditor, `type` := "button", `class` := "btn btn-outline-primary m-2", "Show Editor"),
            button(id := Config.ViewButtons.showBoth, `type` := "button", `class` := "btn btn-outline-primary m-2", "Show Both"),
            button(id := Config.ViewButtons.showPreview, `type` := "button", `class` := "btn btn-outline-primary m-2", "Show Preview"),
            button(id := Config.Printing.togglePrintButtonId, `type` := "button", `class` := "btn btn-outline-primary m-2", "Show Print View"),
            div(id := Config.ViewButtons.ndTitleErrors, `class` := "invisible", role := "tooltip"),
            input(id := Config.ViewButtons.documentTitle, autocomplete := "off", `type` := "text", `class` := "ms-auto m-2", value := nilsdoc.title),
            i(id := Config.ViewButtons.ndSaveIcon, `class` := "fa-regular fa-floppy-disk hidden m-2"),
            button(id := Config.SummaryPrompt.summaryViewButtonId, `type` := "button", `class` := "btn btn-outline-primary m-2", "Toggle my summary"),
            a(id := Config.ViewButtons.publishButtonId, `class` := "btn btn-success m-2", `type` := "button", "Publish")
          ),
          div(
            id := Config.editorAndPreviewContainerWrapperId,
            `class` := "flex-container",
            div(id := "editorContainer", `class` := "mycontainer", pre(textarea(id := "editor"))),
            nilsdocRenderArea(true)
          ),
          storageArea(storedObjects),
          div(id := Config.Printing.printOutputDivId),
          /*script( src:="/quill/dist/quill.min.js"), // if I initialize this below mathbox, it breaks.
              script( src:="/quill/dist/quill.min.js"), // if I initialize this below mathbox, it breaks.
              script( src:="/mathbox/build/mathbox-bundle.js"),
              script( `type`:="text/javascript", src:="/Tangle.js"),
              script( `type`:="text/javascript", src:="/TangleKit/mootools.js"),
              script( `type`:="text/javascript", src:="/TangleKit/sprintf.js"),
              script( `type`:="text/javascript", src:="/TangleKit/BVTouchable.js"),
              script( `type`:="text/javascript", src:="/TangleKit/TangleKit.js"),
           */
          nilsdocMetadata(nilsdoc),
          script(`type` := "text/javascript", src := "https://cdnjs.cloudflare.com/ajax/libs/mathjs/7.5.1/math.min.js"),
          script(`type` := "text/javascript", src := "/src/@popperjs/core/dist/umd/popper.min.js"),
          // script( `type`:="text/javascript", src:="/bootstrap/dist/js/bootstrap.min.js"),
          script(`type` := "text/javascript", src := "/src/konva/konva.min.js"),
          script(`type` := "text/javascript", src := "/src/katex/katex.min.js"),
          script(`type` := "module", src := "/dist/nilsdocWorkstation.js"),
        )
      )
    ).render
  }
}
