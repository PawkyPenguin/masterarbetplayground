const path = require('path');

module.exports = {
  mode: 'development',
  entry: {
    'nilsdocWorkstation': ['./src/nilsdocWorkstation.js'],
    'visualEditor': ['./src/visualEditor.js'],
    'personalNotes': ['./src/personalNotes.js'],
    'publishView': ['./src/publishView.js'],
    'studentView': ['./src/studentView.js']
  },
  devtool: 'inline-source-map',
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'dist'),
  },
  module: {
    rules: [
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.js$/,
        enforce: "pre",
        use: ["source-map-loader"],
      },
    ],
  },
};
