#!/usr/bin/env bash
java_pids=$(ps -h | grep "java" | grep -v sbt-launch | grep -v grep | awk '{print $1}')
[[ -n "$java_pids" ]] && kill "$java_pids"

webpack_pids=$(ps -h | grep "webpack" | grep -v grep | awk '{print $1}')
# unsurprisingly, webpack can't listen properly, so it gets a kill 9.
[[ -n "$webpack_pids" ]] && kill -9 "$webpack_pids"
