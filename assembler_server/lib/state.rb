module Machine
  class State
    attr_reader :registers, :reg1, :reg2, :steps, :programCounter, :instructions, :stopped, :stateTable
    def initialize(instructions, input)
      @registers = [0.to_r, 0.to_r, 0.to_r, 0.to_r]
      @reg1 = 0.to_r
      @reg2 = 0.to_r
      @steps = 0
      @programCounter = 0
      @instructions = instructions
      @stopped = false
      @input = input
      @stateTable = []
    end

    def step()
      if @stopped or @programCounter >= @instructions.length then
        return
      end
      @output = ""
      @terminateInThisStep = false
      instruction = @instructions[@programCounter]
      puts "running #{instruction.to_s}"
      self.send(instruction.opcode.to_sym, *instruction.args)
      @programCounter += 1
      @steps += 1
      if @terminateInThisStep then
        @stopped = true
      end
      @stateTable << currentState
      return @output == "" ? "" : @output.to_f.to_s
    end

    def currentState
      [@steps, @input.clone.map{|r| r.to_f}, [@reg1.to_f, @reg2.to_f], [@programCounter+1] + (@registers.map{|r| r.to_f})]
    end

    def writeState
      puts "=============="
      puts "Has run for #{@steps} steps"
      puts "Input: #{@input}"
      puts "Registers: #{@registers}"
      puts "Reg1 = #{@reg1}"
      puts "Reg2 = #{@reg2}"
      puts "Instruction ctr = #{@programCounter + 1}"
      puts "Next instruction: #{@instructions[@programCounter]}"
    end

    def noop
    end

    def terminate
      @terminateInThisStep = true
    end

    def read
      if @input.length == 0 then
        raise "Tried to read from input while input was empty."
      end
      @reg1 = @input.pop.to_r
    end

    def add
      p @reg1
      p @reg2
      @reg1 = @reg1 + @reg2
    end

    def sub
      @reg1 = @reg1 - @reg2
    end

    def mult
      @reg1 = @reg1 * @reg2
    end

    def div
      if @reg2 == 0 then
        raise 'Division by 0.'
      end
      @reg1 = @reg1 / @reg2
    end

    def add1
      @reg1 += 1
    end

    def sub1
      @reg1 -= 1
    end

    def write1
      @output = @reg1
    end

    def store(arg)
      @registers[arg-1] = @reg1
    end

    def self.ensureLength(array, length)
      if array.length < length then
        array[length - 1] = 0
        array.map! {|e| e || 0}
      end
    end

    def store_addr(arg)
      State.ensureLength(@registers, @registers[arg - 1])
      @registers[@registers[arg - 1] - 1] = @reg1
    end

    def load_addr(arg)
      State.ensureLength(@registers, @registers[arg - 1])
      @reg1 = @registers[@registers[arg - 1] - 1]
    end

    def load1_addr(arg)
      load_addr(arg)
    end

    def load2_addr(arg)
      State.ensureLength(@registers, @registers[arg - 1])
      @reg2 = @registers[@registers[arg - 1] - 1]
    end

    def load(arg)
      @reg1 = @registers[arg-1]
    end

    def load1(arg)
      load(arg)
    end

    def load2(arg)
      @reg2 = @registers[arg-1]
    end

    def load_direct(arg)
      @reg1 = arg.to_r
    end

    def load1_direct(arg)
      load_direct(arg)
    end

    def load2_direct(arg)
      @reg2 = arg.to_r
    end

    def write(arg)
      @output = @registers[arg-1]
    end

    def write_direct(arg)
      @output = arg
    end

    def write_addr(arg)
      State.ensureLength(@registers, @registers[arg - 1])
      @output = @registers[@registers[arg-1] - 1]
    end

    def jzero(arg)
      if @reg1 == 0 then
        @programCounter = arg - 2
      end
    end

    def jgtz(arg)
      if @reg1 > 0 then
        @programCounter = arg - 2
      end
    end

    def jump(arg)
      @programCounter = arg - 2
    end
  end

  class Instruction
    attr_reader :opcode, :args
    def initialize(opcode, args)
      @opcode = opcode
      @args = args
    end

    def self.validateParse(instructionString)
      opcode, *args = instructionString.split(" ")
      opcode.downcase!
      errorString = "Error in instruction `#{instructionString}`: "
      begin
        case opcode
        when /^(noop|read|end|add|sub|mult|div|add1|sub1|write1)$/
          unless args.length == 0 then
            raise errorString + "Must not take any arguments."
          end
          if opcode == "end" then
            opcode = "terminate" # gotta rename this cuz 'end' is a ruby keyword
          end
        when /^(jzero|jgtz|jump)$/
          unless args.length == 1 then
            raise errorString + "Requires 1 argument."
          end
          unless args[0] =~ /-?[0-9]+/ then
            raise errorString + "Can't understand argument `#{args[0]}`."
          end
          args[0] = args[0].to_i
        when /^(store|load|load1|load2|write|jzero|jgtz|jump)$/
          unless args.length == 1 then
            raise errorString + "Requires 1 argument."
          end
          unless args[0] =~ /[=*]?-?[0-9]+/ then
            raise errorString + "Can't understand argument `#{args[0]}`."
          end

          if args[0].start_with?("=") then
            #cut off =-sign from args and add it to opcode instead
            opcode += "_direct"
            args[0] = args[0][1..]
          elsif args[0].start_with?("*") then
            #cut off *-sign from args and use other opcode (e.g. 'store_addr' or 'load_addr')
            opcode += "_addr"
            args[0] = args[0][1..]
          end
          args[0] = args[0].to_i
        else
          raise errorString + "Unknown opcode `#{opcode}`."
        end
      rescue => exception
        raise "Error while executing `#{instructionString}`: #{exception.message}"
      end

      return Instruction.new(opcode, args)
    end

    def to_s
      return "#{@opcode}(#{@args.join(", ")})"
    end
  end
end
