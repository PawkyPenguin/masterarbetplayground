module Util
  def transposeContent(array)
    return array.reduce(array.map{|e| []}) {|acc, arr| acc.zip(arr).map{|tuple| tuple[0].append(tuple[1])}}
  end

  def printStates(states)
    transpose = states.transpose #transposeContent(states)
    puts "Steps: #{transpose[0]}"
    puts "Queue: #{transpose[1].map{|x| x.map{|y| y.to_f}}}"
    cpuREGS = transpose[2]
    puts "REG(0): #{cpuREGS.map{|y| y[0].to_f}}"
    puts "REG(1): #{cpuREGS.map{|y| y[1].to_f}}"
    states = transpose[3]

    registersInSteps = states.map {|state| state.length}
    maxRegisters = registersInSteps.max
    (0...states.length).each do |i|
      if states[i].length < maxRegisters then
        State.ensureLength(states[i], maxRegisters)
      end
    end

    states = states.transpose
    states.each_with_index do |state, i|
      puts "Reg #{i}: #{state.map {|e| e.to_f}}" unless state.all? {|e| e == 0}
    end
  end
end
