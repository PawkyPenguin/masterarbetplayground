#!/usr/bin/env ruby
require 'websocket'
require 'websocket-eventmachine-server'
require 'json'
require './lib/state.rb'
include Machine

def parseInstructions(instructions, input)
  parsedInstructions = []
  begin
    inputs = input.chomp.split(" ").map{|x| x.to_r}.reverse
  rescue => exception
    raise "Couldn't parse input #{inputs}: #{exception.message}"
  end
  instructions.each do |instruction|
    instruction.chomp!
    if instruction == "" or instruction.start_with? "#" then
      parsedInstructions.append(Instruction.validateParse("noop"))
    else
      parsedInstructions.append(Instruction.validateParse(instruction))
    end
  end
  return State.new(parsedInstructions, inputs)
end

class Server
  def initialize()
    @machines = []

    EM.run do
      WebSocket::EventMachine::Server.start(:host => "0.0.0.0", :port => 8088) do |ws|
        ws.onopen do
          puts "Client connected, sending codes"
        end

        ws.onmessage do |msg, type|
          puts "Received: #{msg}"
          msg = JSON.parse(msg)
          case msg["type"]
          when "newMachine"
            id = msg['content']['machineId']
            begin
              @machines[id] = parseInstructions(msg["content"]['instructions'], msg['content']['input'])
              ws.send({type: 'createdMachine', content: id}.to_json)
            rescue => exception
              ws.send({type: 'error', content: {machineId: id, errorMessage: exception.message}}.to_json)
            end
          when "step"
            machineId = msg['content'].to_i
            begin
              machine = @machines[machineId]
              output = machine.step()
              ws.send({type: 'output', content: {machineId: machineId, output: output, state: machine.stateTable}}.to_json)
              puts "sending: #{{type: 'output', content: {machineId: machineId, output: output, state: machine.stateTable}}}"
              if machine.stopped then
                ws.send({type: 'finished', content: machineId}.to_json)
              end
            rescue => exception
              ws.send({type: 'error', content: {machineId: machineId, errorMessage: exception.message}}.to_json)
              puts "sending: #{{type: 'error', content: {machineId: machineId, errorMessage: exception.message}}}"
            end
          end
        end

        ws.onclose do
          puts "Client disconnected"
        end

      end
    end

  end
end

Server.new()
