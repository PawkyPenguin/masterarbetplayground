Experiments to create dynamic documents for learning and teaching.

# Introduction

This repository uses the mill build system. The `mill` executable in `dynamic_book_website/` provides the entry point. It is tracked by git and can be used as-is. To see available build tasks, execute `./mill resolve _`. Right now, only the `scalaJs` module actually does something relevant. `./mill resolve scalaJs._` will list these tasks.  

The most important tasks are:

* `./mill scalaJs.buildBook` to build the html file of the book. The resulting HTML is `out/scalaJs/buildBook.dest/out.html`. You can open this file from Firefox or any other browser.
* `./mill scalaJs.runPandoc` will use the pandoc template in `markdown_to_html/template.html` to build the `out.html` file that is used by the buildBook task. This does not build the associated ScalaJS files, so only functionality provided within the HTML template itself will work. Note that you typically need not execute the `runPandoc` task by hand, because `buildBook` does it automatically.

(You can always find output files of a mill task in the `out/` folder. If you execute `./mill module.submodule.subsubmodule.my_task`, the output files will be found in `out/module/submodule/subsubmodule/my_task.dest`. 

## Build instructions

While in the repository's root folder:

* Start the ruby server in `assembler_server/assembler_interpreter_websocket.rb` to interpret ASSEMBLER instructions that are sent from the website to the ruby server via a Websocket connection (if you forgot to perform this step, just reload the website).
* Execute `dynamic_book_website/mill scalaJs.buildBook` and open `out/scalaJs/buildBook.dest/out.html` in your browser.


# Similar Ideas/Feature Inspiration

- [Geogebra](https://www.geogebra.org/)
- [D3.js](https://d3js.org/)
- [Jupyter Notebook](https://docs.jupyter.org/en/latest/)
- [Analysis Skript Manfred Einsiedler & Andreas Wieser](https://wp-prd.let.ethz.ch/WP0-CIPRF9693/)
- [MathBox](https://github.com/unconed/mathbox)
- [Bret Victor -- Up and Down the Ladder of Abstraction](http://worrydream.com/LadderOfAbstraction/)
- [Bret Victor -- Learnable Programming](http://worrydream.com/#!/LearnableProgramming)
- [Bret Victor -- Media for Thinking the Unthinkable](http://worrydream.com/#!/MediaForThinkingTheUnthinkable)
- [Vi Hart & Nicky Case -- Parable of the Polygons](https://ncase.me/polygons/)
- [Mnemonic Medium/Quantum Country](https://quantum.country/qcvc)
- [Structure and Interpretation of Classical Mechanics, second edition](https://mitp-content-server.mit.edu/books/content/sectbyfn/books_pres_0/9579/sicm_edition_2.zip/book.html), resp. https://mitpress.mit.edu/9780262028967/structure-and-interpretation-of-classical-mechanics/
- [Functional Differential Geometry](https://mitpress.mit.edu/9780262019347/functional-differential-geometry/)
- [Nextjournal](https://nextjournal.com/home)
- [Pandoc Tables](https://pandoc.org/MANUAL.html#extension-pipe_tables)

## Tangentially Related

- [XLogoOnline und einfachInformatik](https://xlogo.inf.ethz.ch/release/latest/)
- [Processing (programming language)](https://p5js.org/learn/)
- [Maria for Clojure](https://www.maria.cloud/intro)
- [Scratch](https://scratch.mit.edu/)
- [Remnote](https://www.remnote.com/)
- [Tiddlywiki](https://tiddlywiki.com/)
- [RoamReserach](https://roamresearch.com/)
- Obsidian? Evernote? Try them out maybe.

# Document Format Implementation Candidates

- [AsciiDoc](https://docs.asciidoctor.org/)
  - seems most sensible. Build for extension and is plaintext.
  - Has Java implementation
  - Major problem: Syntax is complex. Implicit defaults and lots of features.
- Markdown, specifically [CommonMark](https://spec.commonmark.org/0.30/)
  - Problems: Confusing whitespace semantics.
    - especially lists combined with pre-blocks create extremely confusing issues.
  - Not built for extension, though can easily pass through HTML
  - Nesting elements is not possible (e.g. lists in tables)
    - Pandoc supports this via markdown extensions, but the syntax gets confusing once again.
- [YAML](https://yaml.org/)
  - confusing whitespace semantics. Relatively large and feature rich. More than what I need.
  - advantages: Widely adopted.
- Emacs orgmode/Tiddlywiki/other quine-like systems
  - Tiddlywiki advantage: It meshes well with Javascript.
  - Disadvantages: Both orgmode and tiddlywiki are already very feature rich. Hard to understand fully and use in a software project.
  - made for customization, but difficult to get into.
  - intuitive syntax, but niche -> almost no support online

# Libraries To Facilitate Implementation

- [Tangle](https://github.com/worrydream/Tangle)
- [MathBox](https://github.com/unconed/mathbox)
- [Asciidoctor Java Bindings](https://github.com/asciidoctor/asciidoctorj)

# Documents That Could Be Made Executable

- Vektorgeometrie W. Gander
- Biology: Profits from spaced repetition (SRS) and mouse-over wikipedia-style for concepts
  - check Kanti Geneology self-study exercises
  - Campbell Biologie -> has problems with images on different pages than references, has "Merke !"-Kästchen, ...
- Chemie SII -> periodic tables? Instant molecule lookup. Short descriptions for chemical elements with pictures?
- Language learning textbooks -> Can use SRS or mouse-over/link to grammar reference. 
  - maybe built-in vocabulary trainer that distributes load (read one page, answer ~5 flashcards or so).
  - Genki, check Kanti french books

# Papers

- [Improving Students’ Learning With Effective Learning Techniques (Dunlosky 2013)](https://www.researchgate.net/publication/258180568_Improving_Students%27_Learning_With_Effective_Learning_Techniques)
- Das merk ich mir! (make it stick)
