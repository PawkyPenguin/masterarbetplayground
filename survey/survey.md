I need a survey that gives me technical expertise level of kanti teachers.
Sött mir erlaube z entscheide eb AsciiDoc, Markdown, WYSIWYG, Latex, eigets Format, etc. die geigneti Wahl isch.

-> online nahluege oder chatgpt frage: gits schu assessment surveys für das? Wenn ja chani die nii (+eigeti frage), will wenns Teil vu grössee Studie sind, hends sicher nuch Korrelatione und so für hüüfigi Fäll drii.

# Mit welchen Applikationen erstellen Sie normalerweise Ihr Unterrichtsmaterial (mehrere Antworten möglich)? 

- Microsoft Word
- Apple Word[]
- Latex
- Google Docs
- Texteditor (z.B. Editor, Notepad++, Sublime(?))
- Andere: <textfield>

# Wünschen Sie sich, dass die oben ausgewählten Applikationen mehr Features hätten? Wenn ja, welche Features?

<freetext>

# Bereiten Ihnen gewisse Aspekte der oben ausgewählten Applikationen Schwierigkeiten (bzw. erwünschen Sie sich mehr davon)? Wenn ja, was für Schwierigkeiten?

<freetext>

# Mit welchen Applikationen haben Sie bereits mindestens einmal Dokumente verfasst? (Die in Frage #[] genannten Applikationen müssen Sie nicht erneut ankreuzen)

- Microsoft Word
- Apple Word[]
- Latex
- Google Docs
- Texteditor (z.B. Editor, Notepad++, Sublime(?))
- Andere: <textfield>

# Können Sie programmieren? Falls ja, mit welchen Programmiersprache(n)?

- Java
- Python
- Javascript
- HTML/CSS
- PHP
- C/C++
- Andere: <textfield>

# Welche der folgenden Sonderzeichen können Sie instinktiv tippen?

-

# Was ist Ihr Tastaturlayout?

- Unverändert/Weiss nicht
- de_CH (Schweizer Layout, Deutsch)
- fr_CH (Schweizer Layout, Französisch)
- en_US
- en_GB
